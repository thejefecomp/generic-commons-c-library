/*
*****************************************************************************************
*
*Copyright (c) 2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: pqueue_ts.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include <stdarg.h>

#include "queue/pqueue_ts.h"

static gccl_list_type_t foundation_list_type = GCCL_LINKED_LIST;

/*
 * TODO: The list of mutexes must be replaced by a dictionary when that data structure gets
 * implemented in the Generic Commons C library (GCCL).
 */
static int pqueue_m_index_LL = -1;

static pqueue_mutex_t* get_pqueue_mutex(int16_t index_pqueue);

static int8_t pqueue_mutex_comparator(const void* const arg1, const void* const arg2);

int16_t create_pqueue_ts(size_t data_alloc_size,
						 int8_t (*const comparator) (const void * const arg1,
													 const void * const arg2),
						 void (*const free_data) (const void * const data),
						 const char * const config_mask, ...)
{
	va_list arg_list = NULL;

	va_start(arg_list,config_mask);

	int16_t pqueue_index = vcreate_list(foundation_list_type, data_alloc_size, comparator, free_data, config_mask, arg_list);

	if(pqueue_m_index_LL == -1)
		pqueue_m_index_LL = create_list(GCCL_LINKED_LIST,sizeof(pqueue_mutex_t), pqueue_mutex_comparator,NULL,NULL);

	pqueue_mutex_t pqueue_mutex = {.index_pqueue = pqueue_index};

	pthread_cond_init(&(pqueue_mutex.access_cond), NULL);
	pthread_mutex_init(&(pqueue_mutex.access_mutex),NULL);

	insert_element_list(GCCL_LINKED_LIST,&pqueue_mutex, INS_LAST, pqueue_m_index_LL);

	return pqueue_index;

}

bool exists_element_in_pqueue_ts(const void * const data,
								 int16_t index_pqueue)
{
	pqueue_mutex_t *pqueue_mutex = get_pqueue_mutex(index_pqueue);

	bool isElement = false;

	pthread_mutex_lock(&(pqueue_mutex->access_mutex));

	isElement = exists_element_in_list(foundation_list_type, data, index_pqueue);

	pthread_mutex_unlock(&(pqueue_mutex->access_mutex));

	return isElement;
}

static pqueue_mutex_t* get_pqueue_mutex(int16_t index_pqueue)
{
	pqueue_mutex_t pqueue_mutex = {.index_pqueue = index_pqueue};

	return (pqueue_mutex_t*) get_element_list(GCCL_LINKED_LIST,&pqueue_mutex,pqueue_m_index_LL);
}

int get_size_pqueue_ts(int16_t index_pqueue)
{
	pqueue_mutex_t *pqueue_mutex = get_pqueue_mutex(index_pqueue);

	int pqueue_size = -1;

	pthread_mutex_lock(&(pqueue_mutex->access_mutex));

	pqueue_size = get_size_list(foundation_list_type, index_pqueue);

	pthread_mutex_unlock(&(pqueue_mutex->access_mutex));

	return pqueue_size;
}

long initialise_service_pqueue_ts(gccl_alloc_t rule, gccl_list_type_t base_list_type,
		 	 	 	 	 	 	  int16_t initial_number_of_pqueues)
{
	/*
	 * Check if the argument is a valid type of list.
	 * If not, return false.
	 */

	foundation_list_type = base_list_type;

	switch(foundation_list_type)
	{

		case GCCL_ARRAY_LIST:
		case GCCL_DOUBLY_LINKED_LIST:
		case GCCL_LINKED_LIST:

			break;

		default:

			return -1L;
	}

	return initialise_service_list(foundation_list_type, rule, initial_number_of_pqueues);
}

bool isEmpty_pqueue_ts(int16_t index_pqueue)
{
	pqueue_mutex_t *pqueue_mutex = get_pqueue_mutex(index_pqueue);

	bool isEmpty = false;

	pthread_mutex_lock(&(pqueue_mutex->access_mutex));

	isEmpty = isEmpty_list(foundation_list_type, index_pqueue);

	pthread_mutex_unlock(&(pqueue_mutex->access_mutex));

	return isEmpty;
}


bool isFull_pqueue_ts(int16_t index_pqueue)
{
	//TODO
	return false;

}

void* pdequeue_ts_element(int16_t index_pqueue)
{
	//TODO
	return NULL;
}

int penqueue_ts_element(const void * const data, int16_t index_pqueue)
{
	//TODO
	return -1;
}

static int8_t pqueue_mutex_comparator(const void * const arg1, const void * const arg2)
{
	pqueue_mutex_t *pqueue_mutex1 = (pqueue_mutex_t*)arg1;

	pqueue_mutex_t *pqueue_mutex2 = (pqueue_mutex_t*)arg2;

	return pqueue_mutex1->index_pqueue == pqueue_mutex2->index_pqueue ? 0
		   : pqueue_mutex1->index_pqueue < pqueue_mutex2->index_pqueue ? -1
		   : 1;
}

bool stop_service_pqueue_ts(long reference_id)
{
	return stop_service_list(reference_id);
}
