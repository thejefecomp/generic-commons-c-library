/*
*****************************************************************************************
*
*Copyright (c) 2022-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: doubly_linked_list.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include "list/linked_list_mgmt.h"
#include "list/doubly_linked_list.h"


/* ********** BEGIN - DOUBLY LINKED LIST INTERFACE IMPLEMENTATION ********** */

static gccl_uds_type_t element_str_type = {.ll_type = GCCL_DOUBLY_LINKED_LIST};


/*!
 *
 * Manages all the deletes and searches operations performed by the
 * Generic_Commons_C_Library, which target doubly linked lists. This is an internal
 * function not available to be utilised by clients of the library.
 */

static void * get_and_delete_element_internal_doubly_LL(const void * const data,
													    int8_t delete_mode,
													    bool perform_delete,
													    int16_t index_LL);

int16_t create_doubly_LL(size_t data_alloc_size,
                         int8_t (*const comparator) (const void * const arg1,
                                                     const void * const arg2),
						 void (*const free_data) (const void * const data),
						 const char * const config_mask, ...)
{
    va_list arg_list = NULL;

    va_start(arg_list, config_mask);

    return gccl_vmgmt_create_LL(data_alloc_size, comparator, free_data, element_str_type,
    					   config_mask, arg_list);
}

int delete_element_doubly_LL(const void * const data, int8_t delete_mode,
                             int16_t index_LL)
{
	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL || list_header->element_str_type.ll_type != GCCL_DOUBLY_LINKED_LIST)
		return -1;

	get_and_delete_element_internal_doubly_LL(data, delete_mode, true, index_LL);

    return list_header->size;
}


bool destroy_doubly_LL(int16_t index_LL)
{
    return gccl_mgmt_free_LL(index_LL);
}



void destroy_array_copy__doubly_LL(const void * array_list_ptr)
{
    gccl_mgmt_free_array_copy_LL(array_list_ptr);
}


bool destroy_ALL_doubly_LL()
{
	return gccl_mgmt_free_all_typed_LL(element_str_type, GCCL_LIST);
}


bool exists_element_in_doubly_LL(const void * const data, int16_t index_LL)
{
    //TODO
    return false;
}


void * get_array_copy_doubly_LL(int16_t index_LL, void (*const element_copy)
                                						(const void * const restrict to,
                                						 const void * const restrict from, int position))
{
    //TODO
    return NULL;
}



void * get_and_delete_element_doubly_LL(const void * const data, int8_t delete_mode,
                                        int16_t index_LL)
{
    return get_and_delete_element_internal_doubly_LL(data, delete_mode, true,
    												 index_LL);
}


static inline void * get_and_delete_element_internal_doubly_LL(const void * const data,
                                                        	   int8_t delete_mode,
															   bool perform_delete,
															   int16_t index_LL)
{
	//TODO
    return NULL;
}


void * get_and_delete_head_doubly_LL(int16_t index_LL)
{
    //TODO
    return NULL;
}


void * get_and_delete_tail_doubly_LL(int16_t index_LL)
{
    //TODO
    return NULL;
}


void * get_element_doubly_LL(const void * const data, int16_t index_LL)
{
    //TODO
    return NULL;
}


void * get_element_at_position_doubly_LL(int position, int16_t index_LL)
{
    //TODO
    return NULL;
}


void * get_head__doubly_LL(int16_t index_LL)
{
    //TODO
    return NULL;
}


int get_size_doubly_LL(int16_t index_LL)
{
    //TODO
    return -1;
}


void * get_tail_doubly_LL(int16_t index_LL)
{
    //TODO
    return NULL;
}


long initialise_doubly_LL_service(gccl_alloc_t rule, int16_t initial_number_of_doubly_LLs)
{
    return gccl_mgmt_initialise_LL_manager(rule,element_str_type,initial_number_of_doubly_LLs);
}


int insert_element_doubly_LL(const void* const data, int8_t insert_mode,
                             int16_t index_LL)
{
    //TODO
    return -1;
}


bool isEmpty_doubly_LL(int16_t index_LL)
{
	if(!gccl_mgmt_check_LL_type(index_LL,element_str_type,GCCL_LIST))
			return false;

	gccl_list_header_LL_t *list_headers = gccl_mgmt_get_LL(index_LL);

    return list_headers->size == 0;
}

bool isFull_doubly_LL(int16_t index_LL)
{
	if(!gccl_mgmt_check_LL_type(index_LL,element_str_type,GCCL_LIST))
		return true;

	gccl_list_header_LL_t *list_headers = gccl_mgmt_get_LL(index_LL);

	return list_headers->lock_max_size && list_headers->size == list_headers->max_size;
}


bool reset_doubly_LL(int16_t index_LL)
{
    //TODO
    return false;
}

bool stop_doubly_LL_service(void)
{
	return stop_LL_service_of_type(element_str_type.ll_type);
}

bool stop_doubly_LL_service_of_reference_id(long reference_id)
{
	return stop_LL_service_of_reference_id(reference_id);
}

/* ********** END - DOUBLY LINKED LIST INTERFACE IMPLEMENTATION ********** */
