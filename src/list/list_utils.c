/*
*****************************************************************************************
*
*Copyright (c) 2013-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: list_utils.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include "list/list_utils.h"

/* ********** BEGIN -  LIST_UTILS FUNCTIONS IMPLEMENTATION ********** */

int8_t double_comparator(const void * const arg1, const void * const arg2)
{
	return *((const double *) arg1) == *((const double *) arg2) ? 0
	                                                : *((const double *) arg1) > *((const double *) arg2) ? 1
	                                                                                          : -1;
}

void double_element_array_copy (const void * const restrict to,
                                const void * const restrict from,
                                int position)
{
	((double *)to)[position] = *((const double *)from);
}

int8_t float_comparator(const void * const arg1, const void * const arg2)
{
	return *((const float *) arg1) == *((const float *) arg2) ? 0
	                                              : *((const float *) arg1) > *((const float *) arg2) ? 1
	                                                                                      : -1;
}

void float_element_array_copy (const void * const restrict to,
                               const void * const restrict from,
                               int position)
{
	((float *)to)[position] = *((const float *)from);
}

int8_t integer_comparator(const void * const arg1, const void * const arg2)
{
	return *((const int *) arg1) == *((const int *) arg2) ? 0
	                                          : *((const int *) arg1) > *((const int *) arg2) ? 1
	                                                                              : -1;
}

void integer_element_array_copy (const void * const restrict to,
                                 const void * const restrict from,
                                 int position)
{
	((int *)to)[position] = *((const int *)from);
}

int8_t long_comparator(const void * const arg1, const void * const arg2)
{
	return *((const long *) arg1) == *((const long *) arg2) ? 0
	                                            : *((const long *) arg1) > *((const long *) arg2) ? 1
	                                                                                  : -1;
}

void long_element_array_copy (const void * const restrict to,
                              const void * const restrict from,
                              int position)
{
	((long *)to)[position] = *((const long *)from);
}

void print_integer(FILE *output, const void * const data)
{
	fprintf(output,"%i", *(const int* const)data);
}

/* ********** END -  LIST_UTILS FUNCTIONS IMPLEMENTATION ********** */

