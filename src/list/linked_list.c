/*
*****************************************************************************************
*
*Copyright (c) 2013-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: linked_list.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "list/linked_list_mgmt.h"
#include "list/linked_list.h"


/* ********** BEGIN - LINKED LIST PUBLIC INTERFACE IMPLEMENTATION ********** */

static gccl_uds_type_t element_str_type = {GCCL_LINKED_LIST};

/*!
 *
 * @function get_and_delete_element_internal_LL
 *
 * @description Manages all the deletes and searches operations performed by the
 * Generic Commons C Library (GCCL), which the target is a linked list. This is an
 * internal function not available to be utilised by clients (i.e, users) of the
 * library.
 *
 * @param data represents the data a user (i.e., developer) wants to find and
 *  	  delete from a linked list represented by a given index [index_LL].
 *
 * @param delete_mode represents the delete mode utilised to perform the delete
 *        operation. It can assume one of the following values: (DEL_DATA_INDEX,
 *        DEL_FIRST,DEL_LAST,DEL_AT_POS).
 *
 * @param perform_delete indicates whether the element must be deleted.
 *
 * @param copy_data indicates whether the data must be copied no matter if the
 * 		  element gets deleted or not.
 *
 * @param index_LL represents an index for the list that gets manipulated by
 *        the function.
 *
 * @result searches the existence of an element on a linked list represented by
 * 		   a given index (index_LL), deleting the element (if requested). Upon
 * 		   request indicated through the parameter copy_data, the function returns
 * 		   a copy of the element that has been found. The function returns NULL,
 * 		   otherwise.
 */

static void * get_and_delete_element_internal_LL(const void * const data,
												 delete_mode_t delete_mode,
												 bool perform_delete,
												 bool copy_data,
												 int16_t index_LL);

int16_t create_LL(size_t data_alloc_size,
                  int8_t (*const comparator) (const void * const arg1,
                                               const void * const arg2),
				  void (*const free_data) (const void * const data),
				  const char *const config_mask, ...)
{
	va_list arg_list = NULL;

	va_start(arg_list, config_mask);


	return gccl_vmgmt_create_LL(data_alloc_size, comparator, free_data, element_str_type,
						   config_mask, arg_list);
}

int16_t vcreate_LL(size_t data_alloc_size,
                   int8_t (* const comparator) (const void * const arg1,
                                                const void * const arg2),
				   void (*const free_data) (const void * const data),
				   const char * const config_mask, va_list arg_list)
{
    return gccl_vmgmt_create_LL(data_alloc_size, comparator, free_data, element_str_type,
    					   config_mask, arg_list);
}

int delete_element_LL(const void * const data, delete_mode_t delete_mode, int16_t index_LL)
{

	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL)
		return -1;

	get_and_delete_element_internal_LL(data, delete_mode, true, false, index_LL);

	return list_header->size;
}

bool destroy_LL(int16_t index_LL)
{
	return gccl_mgmt_free_LL(index_LL);
}

void destroy_array_copy_LL(const void * array_list_ptr)
{
	gccl_mgmt_free_array_copy_LL(array_list_ptr);
}

bool destroy_ALL_LL(void)
{
	return gccl_mgmt_free_all_typed_LL(element_str_type, GCCL_LIST);
}

bool destroy_ALL_typed_LL(gccl_list_type_t list_type)
{
	gccl_uds_type_t element_str_type_rem = {list_type};
	return gccl_mgmt_free_all_typed_LL(element_str_type_rem, GCCL_LIST);
}

bool exists_element_in_LL(const void* const data, int16_t index_LL)
{
	return get_and_delete_element_internal_LL(data, DEL_FIRST, false,
									   	      true, index_LL) != NULL;
}

void * get_and_delete_element_LL(const void * const data, delete_mode_t delete_mode,
                                 int16_t index_LL)
{
	return get_and_delete_element_internal_LL(data, delete_mode, true, true,
											  index_LL);
}

static void * get_and_delete_element_internal_LL(const void * const data,
                                                 delete_mode_t delete_mode,
										         bool perform_delete,
												 bool copy_data,
										         int16_t index_LL)
{
	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL)
		return NULL;

	gccl_element_LL_t *element_del = NULL, *previous_del = NULL;

	const void * return_data = NULL;

	switch (delete_mode)
	{
		case DEL_LAST:

			element_del = list_header->head;
			previous_del = NULL;

			for (int i = list_header->size; i > 0 ; --i)
			{
				if(list_header->comparator_func (data, element_del->data) == 0)
					break;

				previous_del = element_del;
				element_del = element_del->next;
			}

			gccl_mgmt_copy_and_delete_element_data_LL(&return_data, element_del,
												 previous_del, perform_delete,
												 copy_data,
												 index_LL);
			break;

		case DEL_FIRST:
		case DEL_DATA_INDEX:

			previous_del = element_del = list_header->head;

			for(int i = list_header->size; i > 0 ; --i)
			{
				if(list_header->comparator_func (data, element_del->data) == 0)
				{
					gccl_mgmt_copy_and_delete_element_data_LL(&return_data,
														 element_del,
														 previous_del,
														 perform_delete,
														 copy_data,
														 index_LL);
					break;
				}

				previous_del = element_del;
				element_del = element_del->next;
			}

			break;

		case DEL_AT_POS:

			if(*((int*)data) + 1 > list_header->size)
				return NULL;

			previous_del = element_del = list_header->head;

			for(int i = *((int*)data); i > 0 ; --i)
			{
				previous_del = element_del;
				element_del = element_del->next;
			}

			gccl_mgmt_copy_and_delete_element_data_LL(&return_data, element_del,
												 previous_del, perform_delete,
												 copy_data,
												 index_LL);

			break;

		default: return NULL;
	}

	return (void *)return_data;
}

void *get_array_copy_LL(int16_t index_LL,
                        void (*const element_copy)(const void * const restrict to,
                                                   const void * const restrict data,
                                                   int position))
{
	return gccl_mgmt_get_array_copy_LL(index_LL,element_copy);
}

void * get_and_delete_head_LL (int16_t index_LL)
{
	int head_pos = 0;
	return get_and_delete_element_internal_LL(&head_pos, DEL_AT_POS, true, true,
	                                          index_LL);
}

void * get_and_delete_tail_LL(int16_t index_LL)
{
	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	int tail_pos = list_header->size-1;

	return get_and_delete_element_internal_LL(&tail_pos, DEL_AT_POS, true, true,
											  index_LL);
}

void * get_element_LL(const void * const data, int16_t index_LL)
{
	return get_and_delete_element_internal_LL(data, DEL_DATA_INDEX, false, true,
											  index_LL);
}

void * get_element_at_position_LL(int position, int16_t index_LL)
{
	return get_and_delete_element_internal_LL(&position, DEL_AT_POS, false, true,
											  index_LL);
}

void * get_head_LL(int16_t index_LL)
{
	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL)
		return NULL;

	return list_header->head == NULL ? NULL : list_header->head->data;
}

int get_max_size_LL(int16_t index_LL){

	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

		if(list_header == NULL)
			return -2;

		return list_header->max_size;
}

int get_size_LL(int16_t index_LL)
{
	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL)
		return -1;

	return list_header->size;
}

void * get_tail_LL(int16_t index_LL)
{
	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL)
		return NULL;

	return list_header->tail == NULL ? NULL : list_header->tail->data;
}

long initialise_LL_service(gccl_alloc_t rule, int16_t initial_number_of_LLs)
{
	gccl_uds_type_t element_str_type = {GCCL_LINKED_LIST};

	if (!gccl_mgmt_initialise_LL_manager(rule,element_str_type,initial_number_of_LLs))
		return -1;

	return gccl_mgmt_generate_reference_id();
}

int insert_element_LL(const void * const data, insert_mode_t insert_mode, int16_t index_LL)
{

	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL || isFull_LL(index_LL))
		return -1;

	gccl_element_LL_t *element = (gccl_element_LL_t*) gccl_mgmt_malloc(GCCL_MEM_DATA,sizeof(gccl_element_LL_t));

	gccl_element_LL_t *next = NULL, *previous = NULL;

	assert(element != NULL);

	element->free_data_func = NULL;

	element->next = NULL;

	element->previous = NULL;

	element->data = gccl_mgmt_malloc(GCCL_MEM_DATA,list_header->data_alloc_size);

	assert(element->data != NULL);

	memcpy(element->data, data, list_header->data_alloc_size);

	element->is_keeping_data = false;
	element->delete_time = 0;

	if(list_header->size == 0)
	{
		element->next = NULL;
		list_header->head = list_header->tail = element;
	}
	else
	{
		switch (insert_mode)
		{
			case INS_LAST:

				list_header->tail->next = element;
				list_header->tail = element;
				element->next = NULL;
				break;

			case INS_FIRST:

				element->next = list_header->head;
				list_header->head = element;
				break;

			case INS_ASC_ORDER:

				if(list_header->comparator_func(element->data, list_header->tail->data) >= 0)
				{
					list_header->tail->next = element;
					element->next = NULL;
					list_header->tail = element;
				}
				else if(list_header->comparator_func(element->data, list_header->head->data) < 0)
				{
					element->next = list_header->head;
					list_header->head = element;
				}
				else
				{
					previous = list_header->head;
					next = list_header->head->next;

					do
					{
						if(list_header->comparator_func(element->data, next->data) < 0)
							break;

						previous = next;
						next = next->next;
					}
					while(next != NULL);

					previous->next = element;
					element->next = next;
				}

				break;

			case INS_DESC_ORDER:

				if(list_header->comparator_func(element->data, list_header->tail->data) <= 0)
				{
					list_header->tail->next = element;
					element->next = NULL;
					list_header->tail = element;
				}
				else if(list_header->comparator_func(element->data, list_header->head->data) > 0)
				{
					element->next = list_header->head;
					list_header->head = element;
				}
				else
				{
					previous = list_header->head;
					next = list_header->head->next;

					do
					{
						if(list_header->comparator_func(element->data, next->data) > 0)
							break;

						previous = next;
						next = next->next;
					}
					while(next != NULL);

					previous->next = element;
					element->next = next;
				}

				break;

			default:

				return -1;
		}
	}

	list_header->size++;

	return list_header->size;
}

bool isEmpty_LL(int16_t index_LL)
{

	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	return list_header != NULL ? list_header->size == 0 : false;
}

bool isFull_LL(int16_t index_LL)
{

	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	return list_header != NULL ? list_header->size == list_header->max_size :
		   false;
}

bool iterate_LL(int16_t index_LL, bool (* const applied_function)
												(const void *const param,
												 const void *const status,
												 const void *const data),
												 int start_position,
												 int end_position,
												 bool stop_at_first_error,
												 void *const param,
												 void *const status)
{

	int size = get_size_LL(index_LL);

	if(size == -1 || start_position >=size || start_position < 0
	   || end_position >= size || end_position < 0 || start_position > end_position)
		return false;

	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);


	gccl_element_LL_t *current_element = list_header->head;

	for(int i = 0; i < start_position; i++)
		current_element = current_element->next;

	for(int i = start_position; i <= end_position; i++, current_element = current_element->next)
		if(!applied_function(param,status,current_element->data) &&
		    stop_at_first_error)
			return false;

	return true;

}

int set_max_size_LL(int16_t index_LL, int new_max_size)
{

	gccl_list_header_LL_t *list_header = gccl_mgmt_get_LL(index_LL);

	if(list_header == NULL)
		return -2;

	int nr_elements_to_delete = -1;

	int tail_pos = -1;

	if(!list_header->lock_max_size)
	{

		if (list_header->size > new_max_size)
		{
			nr_elements_to_delete = list_header->size - new_max_size;

			for(int i = 1; i <= nr_elements_to_delete ; ++i)
			{
				tail_pos = list_header->size - i;

				get_and_delete_element_internal_LL(&tail_pos, DEL_LAST, true,
												   false, index_LL);
			}
		}

		return list_header->max_size = new_max_size;
	}

	return -1;
}

bool reset_LL(int16_t index_LL)
{
	return gccl_mgmt_reset_LL(index_LL);
}

bool stop_LL_service(void)
{
	gccl_uds_type_t element_str_type_rem = {.str_type = GCCL_NO_STRUCTURE};

	bool is_list = false;

	for(int i = GCCL_LINKED_LIST; i >=0; --i)
	{
		switch(i)
		{
			case GCCL_ARRAY_LIST:
				element_str_type_rem.ll_type = GCCL_ARRAY_LIST;
				break;

			case GCCL_DOUBLY_LINKED_LIST:
				element_str_type_rem.ll_type = GCCL_DOUBLY_LINKED_LIST;
				break;

			case GCCL_LINKED_LIST:
				element_str_type_rem.ll_type = GCCL_LINKED_LIST;
				break;

			default:
				element_str_type_rem.str_type = GCCL_NO_STRUCTURE;
				break;
		}

		gccl_mgmt_free_all_typed_LL(element_str_type_rem, is_list ? GCCL_LIST : GCCL_NO_DATA_STRUCTURE);
	}

	return true;
}

bool stop_LL_service_of_type(gccl_list_type_t gccl_list_type)
{
	gccl_uds_type_t element_str_type_rem = {.ll_type = gccl_list_type};

	return gccl_mgmt_free_all_typed_LL(element_str_type_rem,GCCL_LIST);
}

bool stop_LL_service_of_reference_id(long reference_id)
{
	return gccl_mgmt_free_all_referenced_LL(reference_id);
}

/* ********** END - LINKED LIST PUBLIC INTERFACE IMPLEMENTATION ********** */
