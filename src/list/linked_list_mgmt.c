/*
*****************************************************************************************
*
*Copyright (c) 2013-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: linked_list_mgmt.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/
#include <malloc/malloc.h>
#include <math.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "list/linked_list_mgmt.h"
#include "list/linked_list.h"

/* ********** BEGIN - LINKED LIST INTERNAL DATA TYPE INITIALISATION ********** */

static const int GCCL_ALLOC_INIT_ZONE_SIZE = 128 * 1024; // 128Kib => 1Kib = 1024 bits.

static const int GCCL_ALLOC_DATA_INIT_ZONE_SIZE = 1024 * 1024; // 1Mib => 1Kib = 1024 bits.

static const char *GCCL_ALLOC_ZONE_NAME = "GCCL_MANAGEMENT_MEMORY_ZONE\0";

static const char *GCCL_ALLOC_DATA_ZONE_NAME = "GCCL_DATA_MEMORY_ZONE\0";

static const int GCCL_FREE_GARBAGE_CYCLE = 1000;

static malloc_zone_t *gccl_alloc_zone = NULL;

static malloc_zone_t *gccl_alloc_data_zone = NULL;

static time_t gccl_erasing_up_to_time = -1;

static pthread_t gccl_garbage_thread = NULL;

static bool is_garbage_cleaner_active = false;

/* The first element in the list_headers is responsible to manage the indexes of
 * removed data structures. The indexes of such data structures can be reutilised
 * to allocate new managed data structures.
 * */
static gccl_list_header_LL_t *gccl_list_headers = NULL;

/* indicates the metadata related to the number and types of managed linked
 * lists, which stores distinguished data structures on top of the established
 * foundation. When the DYNAMIC length_rule is used, the value of the
 * corresponding data structure is automatically incremented by the library
 * when needed (depends on available memory). As a consequence, the total
 * number of all data structures is incremented accordingly.
 * */
static gccl_ds_metadata_lib_t gccl_ds_metadata_lib = {{{GCCL_ALLOC_DYNAMIC,0,0,{.ll_type = GCCL_ARRAY_LIST}},\
									   	   	  	  	 {GCCL_ALLOC_DYNAMIC,0,0,{.ll_type = GCCL_DOUBLY_LINKED_LIST}},\
													 {GCCL_ALLOC_DYNAMIC,0,0,{.ll_type = GCCL_LINKED_LIST}},\
													 {GCCL_ALLOC_DYNAMIC,0,0,{.tree_type = GCCL_AVL_TREE}},\
													 {GCCL_ALLOC_DYNAMIC,0,0,{.tree_type = GCCL_RED_BLACK_TREE}},\
													 {GCCL_ALLOC_DYNAMIC,0,0,{.str_type = GCCL_DICTIONARY}}},0,0};

static const int gccl_last_ds_valid_type = GCCL_DICTIONARY;

/* indicates the next available index in the list_headers to allocate a new
 * managed data structure.
 * */
static int16_t gccl_ptr_ins_LL = 0;

/*
 * Represents the condition variable utilised to either signal or wait for the
 * existence of elements to be freed from memory.
 */
static pthread_cond_t gccl_signal_ready_erase_cond_LL = {-1L};

/* Represents the mutex variable utilised to guarantee mutual exclusion while
 * freeing elements from either reseted or destroyed linked lists.
 * */
static pthread_mutex_t gccl_size_mutex_LL = {-1L};

/* ********** END - LINKED LIST INTERNAL DATA TYPE INITIALISATION ********** */


/* ********** BEGIN - LINKED LIST INTERNAL MANAGEMENT INTERFACE IMPLEMENTATION ********** */

/*
 * @function gccl_mgmt_free_garbage_internal_LL
 *
 * @description Removes all elements from either reseted or destroyed linked lists,
 * 				together with all elements removed from active linked lists.
 * 				This is a function executed by a separated thread we have named
 * 				garbage cleaner thread.
 *
 * @param param represents a parameter that can be passed to the garbage cleaner
 * 				thread, according to the mandatory signature utilised by
 * 				the POSIX pthread_create() function. At the moment, no value is
 * 				provided in this parameter.
 *
 * @result at the moment, the function returns NULL when finishing its execution.
 */
static void* gccl_mgmt_free_garbage_internal_LL(const void * const param);

/*
 * @function gccl_mgmt_get_malloc_zone
 *
 * @description Returns the memory zone utilised for storing data within
 * 				Generic Commons C Library (GCCL). There are only two memory zones
 * 				(at the moment): DATA and MANAGEMENT. The DATA type is utilised
 * 				to store all memory blocks related to elements created to hold
 * 				user data; while the MANAGEMENT type holds special memory blocks
 * 				related to data accessible only by GCCL internal functions.
 *
 * @param memory_type represents the type of memory zone that gets returned.
 * 					  At the moment, there are only two types: DATA and MANAGEMENT.
 *
 * 	@result returns a pointer to the memory zone at choice; NULL, otherwise.
 */
static malloc_zone_t * gccl_mgmt_get_malloc_zone(gccl_memory_type_t memory_type);

/*!
 *
 * @function gccl_mgmt_initialise_default_header
 *
 * @description initialises the default headers of the Generic Commons C Library
 * 				(GCCL) manager. Index 0 stores deleted lists for reutilisation;
 * 				while Index 1 stores deleted elements to be removed from memory
 * 				during the program execution.
 *
 * @param header_index represents the initial index of the list_header, which
 * 		  at the moment of the first invocation is zero (0).
 *
 * @param size represents the initial number of elements held by the index
 * 		  associated with the header_index, which is usually zero (0).
 *
 * @param max_size represents the max number of elements held by the index
 * 		  associated with the header_index. The value of -1 represents INT32_MAX.
 *
 * @param is_max_size_locked indicates if the max_size is whether locked, which
 * 		  implies the ability to either increase the current size or prevent it
 * 		  to happen.
 *
 * @param element_str_type represents the data structure type carried on by managed
 * 		  linked lists. Although this parameter indicates the data structure
 * 		  being managed by the Generic Commons C Library (GCCL), the library
 * 		  uses linked list elements on its foundation for enhanced flexibility
 * 		  related to dynamic memory allocation requirements. Depending on the data
 * 		  structure type that has been carried on, only one linked list element
 * 		  will be created, which is the starting point to access specific elements
 * 		  of the aforementioned data structure type.
 *
 * @param reference_id represents a unique reference utilised to identify data
 * 		  structures created by a given service, which the management cycle can
 * 		  be performed apart from other data structures of the same type.
 *
 * @param comparator represents the function utilised to compare the elements
 * 		  stored by a given data structure.
 *
 * @param free_data represents the function utilised to execute special purpose
 * 		  instructions to free a given element from memory.
 *
 * @param data_alloc_size represents the size of the data stored by a given
 * 		  data structure supported by the Generic Commons C Library (GCCL).
 *
 */
static void gccl_mgmt_initialise_default_header(int8_t header_index, int size,
                                           	   	int max_size,
												bool is_max_size_locked,
												gccl_uds_type_t element_str_type,
												long reference_id,
												int8_t (*const comparator)
                                                  	  	(const void * const arg1,
                                                  	  	 const void * const arg2),
												void (*const free_data) (const void * const data),
												size_t data_alloc_size);

/*!
 *
 * @function gccl_mgmt_set_free_func_for_all_LL
 *
 * @description when a given linked list gets reseted, which means its elements
 * 				must be removed because either a provided data structure service
 * 				or the Generic Commons C Library (GCCL) manager is stopped, and
 * 				that given linked list requires a special procedure to free its
 * 				elements from memory, this function prepare the elements of to be
 * 				properly freed from memory. It means there is no more requirement
 * 				of removing all elements of this type of list before either
 * 				stopping a given data structure service or the GCCL manager
 * 				itself.
 *
 * @param index_LL represents the index for the linked list carrying a data
 * 		  structure with a given type, which the function checks the existence.
 *
 *
 */
static void gccl_mgmt_set_free_func_for_all_LL(int16_t index_LL);


inline void gccl_bin_off_indication(void)
{
	gccl_mgmt_bin_off_indication();
}

void gccl_mgmt_bin_off_indication(void)
{
	pthread_mutex_lock(&gccl_size_mutex_LL);

		gccl_erasing_up_to_time = time(NULL);

	pthread_mutex_unlock(&gccl_size_mutex_LL);
}


bool gccl_mgmt_check_index_LL(int16_t index_LL)
{

	if (index_LL < 0 || index_LL >= gccl_ptr_ins_LL)
		return false;

	return true;
}

bool gccl_mgmt_check_LL_type(int16_t index_LL, gccl_uds_type_t element_str_type,
							 gccl_abs_ds_type_t abs_ds_type)
{
	if((index_LL < 2 && index_LL >= gccl_ptr_ins_LL))
		return false;

	switch(abs_ds_type)
	{

		case GCCL_LIST: return element_str_type.ll_type == gccl_list_headers[index_LL].element_str_type.ll_type;

		case GCCL_TREE: return element_str_type.tree_type == gccl_list_headers[index_LL].element_str_type.tree_type;

		case GCCL_DS_OTHER: return element_str_type.str_type == gccl_list_headers[index_LL].element_str_type.str_type;

		case GCCL_NO_DATA_STRUCTURE:

		default: return false;
	}
}

bool gccl_mgmt_check_reference_id(long reference_id)
{
	if(reference_id < 0)
		return false;

	for(int index = gccl_ptr_ins_LL-1; index >= 2; --index)
		if(gccl_list_headers[index].reference_id == reference_id)
			return true;

	return false;
}

void gccl_mgmt_copy_and_delete_element_data_LL(const void ** data_ptr,
										  	   gccl_element_LL_t * const restrict element_del,
											   gccl_element_LL_t * const restrict previous_del,
											   bool perform_delete,
											   bool copy_data,
											   int16_t index_LL)
{
	if(element_del != NULL && copy_data)
		*data_ptr = element_del->data;

	if (element_del != NULL && perform_delete)
		gccl_mgmt_free_element_LL(previous_del, element_del, copy_data, index_LL);
}

int16_t gccl_mgmt_create_LL(size_t data_alloc_size,
                       	   	int8_t (*const comparator) (const void * const arg1,
                                                   	    const void * const arg2),
							void (*free_data) (const void * const data),
							gccl_uds_type_t str_type, const char * const config_mask, ...)
{
    va_list arg_list = NULL;

    va_start(arg_list,config_mask);

    return gccl_vmgmt_create_LL(data_alloc_size, comparator, free_data, str_type, config_mask, arg_list);

}

int16_t gccl_vmgmt_create_LL(size_t data_alloc_size,
                        	 int8_t (*const comparator) (const void * const arg1,
                                                    	 const void * const arg2),
							 void (*free_data) (const void * const data),
							 gccl_uds_type_t element_str_type, const char * const config_mask,
							 va_list arg_list)
{
		int config_mask_length = -1;

	    int16_t *data = NULL;

		int16_t index_LL = -1;

		bool is_max_size_locked = false;

		int max_size = -1;

		long reference_id = -1L;

        if(gccl_list_headers == NULL)
            return -1;

        if(gccl_list_headers[0].size > 0)
        {
            data = (int16_t *) get_and_delete_head_LL(0);
            index_LL = *data;
        }
        else
        {
            if(gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].nr_structures > gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].max_nr_structures
               && gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].alloc_length_rule == GCCL_ALLOC_STATIC)
                return -2;

            else if(gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].nr_structures == gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].max_nr_structures
            		&& gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].alloc_length_rule == GCCL_ALLOC_DYNAMIC)
            {
            	gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].max_nr_structures+=6;
            	gccl_ds_metadata_lib.total_max_nr_structures+=6;

            	pthread_mutex_lock(&gccl_size_mutex_LL);

                gccl_list_headers = gccl_mgmt_realloc(GCCL_MEM_MANAGEMENT, gccl_list_headers,gccl_ds_metadata_lib.total_max_nr_structures*sizeof(gccl_list_header_LL_t));

                pthread_mutex_unlock(&gccl_size_mutex_LL);

                if(gccl_list_headers == NULL)
                    return -3;
            }

            index_LL = gccl_ptr_ins_LL++;
        }

        gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].nr_structures++;
        gccl_ds_metadata_lib.total_nr_structures++;

        if(config_mask != NULL)
        {
        	config_mask_length = strlen(config_mask);

        	config_mask_length = config_mask_length > 3 ? 3 : config_mask_length;

        	for(int i = 0; i < config_mask_length; i++)
        	{
        		switch(config_mask[i])
        		{
        			case 'l':

        				is_max_size_locked = va_arg(arg_list,int);
        				break;

        			case 's':

						max_size = va_arg(arg_list,int);
						break;

        			case 'u':

        				is_max_size_locked = !va_arg(arg_list,int);
        				break;

        			case 'i':

        				reference_id = va_arg(arg_list,long);
        				break;

        			default:

        				break;
        		}
        	}
        }

        va_end(arg_list);

        gccl_mgmt_initialise_default_header(index_LL,0,max_size,is_max_size_locked,
        							   element_str_type,reference_id,comparator,
									   free_data,data_alloc_size);

        return index_LL;
}

inline void gccl_mgmt_free(gccl_memory_type_t memory_type, const void * memblock_ptr)
{
	malloc_zone_free(gccl_mgmt_get_malloc_zone(memory_type),(void *)memblock_ptr);
}

bool gccl_mgmt_free_LL(int16_t index_LL)
{
	int16_t * rem_LL = NULL;

	if(!gccl_mgmt_check_index_LL(index_LL))
		return false;

	gccl_ds_metadata_lib.ds_metadata[gccl_list_headers[index_LL].element_str_type.ll_type].nr_structures--;
	gccl_ds_metadata_lib.total_nr_structures--;

	rem_LL = gccl_mgmt_malloc(GCCL_MEM_DATA,sizeof(int16_t));

	gccl_mgmt_reset_LL(index_LL);

	*rem_LL = index_LL;

	insert_element_LL(rem_LL, INS_LAST, 0);

	return true;
}

bool gccl_mgmt_free_all_LL()
{
	if(gccl_list_headers == NULL)
		return true;

	void **return_join_exit_status = NULL;

	for(int i = 0; i < gccl_ptr_ins_LL; i++)
		if(!gccl_mgmt_reset_LL(i))
			return false;

	pthread_mutex_lock(&gccl_size_mutex_LL);

	gccl_erasing_up_to_time = time(NULL);
	is_garbage_cleaner_active = false;

	pthread_cond_signal(&gccl_signal_ready_erase_cond_LL);

	pthread_mutex_unlock(&gccl_size_mutex_LL);

	pthread_join(gccl_garbage_thread,return_join_exit_status);

	pthread_cond_destroy(&gccl_signal_ready_erase_cond_LL);
	pthread_mutex_destroy(&gccl_size_mutex_LL);

	gccl_mgmt_free(GCCL_MEM_MANAGEMENT, gccl_list_headers);

	gccl_list_headers = NULL;
	gccl_ptr_ins_LL = 0;

	for(int i = gccl_last_ds_valid_type; i >=0; --i)
	{

		gccl_ds_metadata_lib.ds_metadata[i].alloc_length_rule = GCCL_ALLOC_DYNAMIC;
		gccl_ds_metadata_lib.ds_metadata[i].max_nr_structures = 0;
		gccl_ds_metadata_lib.ds_metadata[i].nr_structures = 0;
	}

	gccl_ds_metadata_lib.total_max_nr_structures = 0;
	gccl_ds_metadata_lib.total_nr_structures = 0;

	malloc_destroy_zone(gccl_alloc_zone);
	malloc_destroy_zone(gccl_alloc_data_zone);

	return true;
}

bool gccl_mgmt_free_all_referenced_LL(long reference_id)
{
	if(reference_id < 0)
		return false;

	for(int index = gccl_ptr_ins_LL-1; index >=2; index--)
		if(gccl_list_headers[index].reference_id == reference_id)
			if(!gccl_mgmt_free_LL(index))
				return false;

	return true;
}

bool gccl_mgmt_free_all_typed_LL(gccl_uds_type_t element_str_type, gccl_abs_ds_type_t abs_ds_type)
{
	for(int index = gccl_ptr_ins_LL-1; index >=2; --index)
		switch(abs_ds_type)
		{
			case GCCL_LIST:

					  if(gccl_list_headers[index].element_str_type.ll_type == element_str_type.ll_type)
						  if(!gccl_mgmt_free_LL(index))
							  return false;
					  break;

			case GCCL_TREE:

			case GCCL_NO_DATA_STRUCTURE:

			default: return false;
		}

	return true;
}

inline void gccl_mgmt_free_array_copy_LL(const void * array_list_ptr)
{
	gccl_mgmt_free(GCCL_MEM_DATA,array_list_ptr);
}

bool gccl_mgmt_free_element_LL(gccl_element_LL_t * const previous, gccl_element_LL_t * const element,
                          bool is_keeping_data, int16_t index_LL)
{
	if(!gccl_mgmt_check_index_LL(index_LL) || element == NULL)
		return false;

	if(gccl_list_headers[index_LL].size == 1)
		gccl_list_headers[index_LL].head = gccl_list_headers[index_LL].tail = NULL;

	else if(element == gccl_list_headers[index_LL].head){
		gccl_list_headers[index_LL].head = element->next;

		//Only effective for Doubly linked lists
		gccl_list_headers[index_LL].head->previous = NULL;
	}
	else if(element == gccl_list_headers[index_LL].tail)
	{
		previous->next = NULL;
		gccl_list_headers[index_LL].tail = previous;
	}
	else{

		previous->next = element->next;

		if(gccl_list_headers[index_LL].element_str_type.ll_type == GCCL_DOUBLY_LINKED_LIST)
			element->next->previous = previous;
	}

	//Assigning special purpose function to free element from memory accordingly.
	element->free_data_func = gccl_list_headers[index_LL].free_data_func != NULL ? gccl_list_headers[index_LL].free_data_func
																				 : NULL;

	pthread_mutex_lock(&gccl_size_mutex_LL);

	element->is_keeping_data = is_keeping_data;
	element->delete_time = time(NULL);

	if(gccl_list_headers[1].head != NULL)
	{
		gccl_list_headers[1].tail->next = element;
	}
	else
	{
		gccl_list_headers[1].head = element;
	}

	element->next = NULL;
	gccl_list_headers[1].tail = element;
	++gccl_list_headers[1].size;

	if(gccl_list_headers[1].size >= GCCL_FREE_GARBAGE_CYCLE)
		pthread_cond_signal(&gccl_signal_ready_erase_cond_LL);

	pthread_mutex_unlock(&gccl_size_mutex_LL);

	--gccl_list_headers[index_LL].size;

	return true;
}

/*
 * @function gccl_mgmt_free_garbage_internal_LL
 *
 * @description Removes all elements from either reseted or destroyed linked lists,
 * 				together with all elements removed from active linked lists.
 * 				This is a function executed by a separated thread we have named
 * 				garbage cleaner thread.
 *
 * @param param represents a parameter that can be passed to the garbage cleaner
 * 				thread, according to the mandatory signature utilised by
 * 				the POSIX pthread_create() function. At the moment, no value is
 * 				provided in this parameter.
 *
 * @result at the moment, the function returns NULL when finishing its execution.
 */
static void* gccl_mgmt_free_garbage_internal_LL(const void * const param)
{
	pthread_mutex_lock(&gccl_size_mutex_LL);

	while (is_garbage_cleaner_active || gccl_list_headers[1].size)
	{
		for(gccl_element_LL_t *element_to_del = gccl_list_headers[1].head,*previous = gccl_list_headers[1].head; element_to_del != NULL;)
		{
			if(!element_to_del->is_keeping_data ||
			   (element_to_del->is_keeping_data &&
			    element_to_del->delete_time <= gccl_erasing_up_to_time))
			{
				if(element_to_del->data != NULL)
				{
					if(element_to_del->free_data_func != NULL)
						element_to_del->free_data_func((const void * const) element_to_del->data);

					gccl_mgmt_free(GCCL_MEM_DATA,element_to_del->data);
				}
				gccl_element_LL_t *next_to_erase = element_to_del->next;

				if(gccl_list_headers[1].size > 1)
				{
					if(element_to_del == gccl_list_headers[1].head)
						previous = gccl_list_headers[1].head = next_to_erase;

					else if(element_to_del == gccl_list_headers[1].tail)
					{
						previous->next = next_to_erase;
						gccl_list_headers[1].tail = previous;
					}

					if(next_to_erase == gccl_list_headers[1].tail)
						previous = gccl_list_headers[1].tail;
				}
				else
					gccl_list_headers[1].head = gccl_list_headers[1].tail = NULL;

				gccl_mgmt_free(GCCL_MEM_DATA,element_to_del);
				--gccl_list_headers[1].size;
				element_to_del = next_to_erase;
			}
			else
			{
				previous = element_to_del;
				element_to_del = element_to_del->next;
			}
		}

		if(is_garbage_cleaner_active)
			pthread_cond_wait(&gccl_signal_ready_erase_cond_LL,&gccl_size_mutex_LL);
	}

	pthread_mutex_unlock(&gccl_size_mutex_LL);

	pthread_exit(NULL);

	return NULL;
}

long gccl_mgmt_generate_reference_id(void)
{
	long generated_reference_id = -1;

	for(int i = 10; i > 0; --i)
		if(!gccl_mgmt_check_reference_id((generated_reference_id = random())))
			break;

	return generated_reference_id;
}

gccl_list_header_LL_t * gccl_mgmt_get_LL(int16_t index_LL)
{
	if(index_LL < 0 || index_LL >= gccl_ptr_ins_LL)
		return NULL;
	else
		return gccl_list_headers+index_LL;
}

void * gccl_mgmt_get_array_copy_LL(int16_t index_LL,
                              	   void (*const element_array_copy) (const void * const to,
                                                                	 const void * const data,
																	 int position))
{
	if(index_LL < 0 || index_LL >= gccl_ptr_ins_LL)
		return NULL;

	void * array_list_ptr = gccl_mgmt_malloc(GCCL_MEM_DATA,gccl_list_headers[index_LL].size * gccl_list_headers[index_LL].data_alloc_size);

	gccl_element_LL_t *element = gccl_list_headers[index_LL].head;

	for(int i = 0; i < gccl_list_headers[index_LL].size; ++i,element = element->next)
	{
		element_array_copy(array_list_ptr,element->data,i);
	}

	return array_list_ptr;
}

static malloc_zone_t * gccl_mgmt_get_malloc_zone(gccl_memory_type_t memory_type)
{
	malloc_zone_t *malloc_zone = NULL;

	switch(memory_type)
	{
		case GCCL_MEM_DATA: malloc_zone = gccl_alloc_zone;
			  	   break;

		case GCCL_MEM_MANAGEMENT: malloc_zone = gccl_alloc_data_zone;
			  	  	  	 break;
		default: break;
	}

	return malloc_zone;
}

int gccl_mgmt_get_total_typed_LL(gccl_uds_type_t element_str_type, gccl_abs_ds_type_t abs_ds_type)
{

	int index = -1;

	switch(abs_ds_type)
	{
		case GCCL_LIST:
				  index = element_str_type.ll_type;
				  break;

		case GCCL_TREE:
				  index = element_str_type.tree_type;
				  break;

		case GCCL_DS_OTHER: index = element_str_type.str_type;
					break;

		case GCCL_NO_DATA_STRUCTURE:

		default: return index;
	}

	return gccl_ds_metadata_lib.ds_metadata[index].nr_structures;
}

static void gccl_mgmt_initialise_default_header(int8_t header_index, int size,
                                           	    int max_size,
												bool is_max_size_locked,
												gccl_uds_type_t element_str_type,
												long reference_id,
												int8_t (*const comparator)
                                                  	    (const void * const arg1,
                                                  	     const void * const arg2),
												void (*const free_data) (const void * const data),
												size_t data_alloc_size)
{
    gccl_list_headers[header_index].size = size;
    gccl_list_headers[header_index].max_size = max_size < 0 ? INT32_MAX : max_size;
    gccl_list_headers[header_index].lock_max_size = is_max_size_locked;
    gccl_list_headers[header_index].element_str_type = element_str_type;
    gccl_list_headers[header_index].reference_id = reference_id;
    gccl_list_headers[header_index].comparator_func = comparator;
    gccl_list_headers[header_index].free_data_func = free_data;
    gccl_list_headers[header_index].data_alloc_size = data_alloc_size;
    gccl_list_headers[header_index].head = gccl_list_headers[header_index].tail = NULL;

    if(header_index == 0)
    	gccl_ptr_ins_LL+=2;
}

bool gccl_mgmt_initialise_LL_manager(gccl_alloc_t rule, gccl_uds_type_t element_str_type,
									 int16_t initial_number_of_DSs)
{

	if(gccl_list_headers == NULL)
	{
		gccl_ds_metadata_lib.ds_metadata[GCCL_LINKED_LIST].max_nr_structures = 2;
		gccl_ds_metadata_lib.ds_metadata[GCCL_LINKED_LIST].nr_structures = 2;
		gccl_ds_metadata_lib.total_nr_structures+= 2;
		gccl_ds_metadata_lib.total_max_nr_structures = 2;

		gccl_alloc_zone = malloc_create_zone(GCCL_ALLOC_INIT_ZONE_SIZE,0);
		gccl_alloc_data_zone = malloc_create_zone(GCCL_ALLOC_DATA_INIT_ZONE_SIZE,0);

		malloc_set_zone_name(gccl_alloc_zone,GCCL_ALLOC_ZONE_NAME);
		malloc_set_zone_name(gccl_alloc_data_zone,GCCL_ALLOC_DATA_ZONE_NAME);

		gccl_list_headers = gccl_mgmt_malloc(GCCL_MEM_MANAGEMENT,(gccl_ds_metadata_lib.total_max_nr_structures+initial_number_of_DSs) * sizeof(gccl_list_header_LL_t));

		if(gccl_list_headers != NULL)
		{
			/*
			 * The first element of list_headers stores deleted lists
			 * for reutilisation.
			 */
			gccl_uds_type_t default_element_str_type = {GCCL_LINKED_LIST};
		    gccl_mgmt_initialise_default_header(0,0,-1,false,default_element_str_type,
		    							   -1,integer_comparator,NULL,sizeof(int16_t));

		    /*
		     * Creating and initialising the Garbage cleaner thread
		     */
		    	is_garbage_cleaner_active = true;

		    	pthread_cond_init(&gccl_signal_ready_erase_cond_LL,NULL);
		    	pthread_mutex_init(&gccl_size_mutex_LL,NULL);
		    	pthread_create(&gccl_garbage_thread,NULL,
		    				   (void *(*)(void *))gccl_mgmt_free_garbage_internal_LL,
							   NULL);
		}
	}

	if(gccl_list_headers != NULL)
	{

		gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].max_nr_structures+=initial_number_of_DSs;
		gccl_ds_metadata_lib.total_max_nr_structures+=initial_number_of_DSs;
		gccl_ds_metadata_lib.ds_metadata[element_str_type.ll_type].alloc_length_rule = rule;
	}

	return gccl_list_headers == NULL ? false : true;
}

inline bool gccl_mgmt_is_LL_manager_active(void)
{
	return gccl_list_headers != NULL;
}

inline void * gccl_mgmt_malloc(gccl_memory_type_t memory_type, size_t size)
{
	return malloc_zone_malloc(gccl_mgmt_get_malloc_zone(memory_type),size);
}

inline void * gccl_mgmt_realloc(gccl_memory_type_t memory_type, const void * memblock_ptr,
						   size_t new_size)
{
	return malloc_zone_realloc(gccl_mgmt_get_malloc_zone(memory_type),(void *)memblock_ptr,
							   new_size);
}

bool gccl_mgmt_reset_LL(int16_t index_LL)
{
	if(index_LL == 1)
		return true;
	else if(!gccl_mgmt_check_index_LL(index_LL))
		return false;

	gccl_mgmt_set_free_func_for_all_LL(index_LL);

	pthread_mutex_lock(&gccl_size_mutex_LL);

	if(gccl_list_headers[index_LL].head != NULL)
	{
		if(gccl_list_headers[1].head != NULL)
		{
			gccl_list_headers[1].tail->next = gccl_list_headers[index_LL].head;
		}
		else
		{
			gccl_list_headers[1].head = gccl_list_headers[index_LL].head;
		}

		gccl_list_headers[1].tail = gccl_list_headers[index_LL].tail;
		gccl_list_headers[1].size+=gccl_list_headers[index_LL].size;

		gccl_list_headers[index_LL].head = gccl_list_headers[index_LL].tail = NULL;
		gccl_list_headers[index_LL].size = 0;

		if(gccl_list_headers[1].size >= GCCL_FREE_GARBAGE_CYCLE)
			pthread_cond_signal(&gccl_signal_ready_erase_cond_LL);
	}

	pthread_mutex_unlock(&gccl_size_mutex_LL);

	return true;
}

static void gccl_mgmt_set_free_func_for_all_LL(int16_t index_LL)
{

	gccl_list_header_LL_t list_header = gccl_list_headers[index_LL];

	if((list_header.free_data_func == NULL) ||
		(list_header.size == 0))
		return;

	gccl_element_LL_t *element = list_header.head;

	do
	{
		element->free_data_func = list_header.free_data_func;
	}
	while((element = element->next) != NULL);
}

inline bool gccl_mgmt_start_LL_manager(void)
{
	gccl_uds_type_t element_str_type = {GCCL_LINKED_LIST};
	return gccl_mgmt_initialise_LL_manager(GCCL_ALLOC_DYNAMIC, element_str_type, 0);
}

inline bool gccl_mgmt_stop_LL_manager(void)
{
	return gccl_mgmt_free_all_LL();
}

inline bool gccl_start_manager(void)
{
	return gccl_mgmt_start_LL_manager();
}

inline bool gccl_stop_manager(void)
{
	return gccl_mgmt_stop_LL_manager();
}
/* ********** END - LINKED LIST INTERNAL MANAGEMENT INTERFACE IMPLEMENTATION ********** */
