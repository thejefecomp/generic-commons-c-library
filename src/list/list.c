/*
*****************************************************************************************
*
*Copyright (c) 2021-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: list.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include<stdarg.h>

#include "list/linked_list.h"
#include "list/list.h"

/* ********** BEGIN - GENERIC LIST PUBLIC INTERFACE IMPLEMENTATION ********** */

int16_t create_list(gccl_list_type_t type,size_t data_alloc_size,
						   int8_t (*const comparator) (const void * const arg1,
								                       const void * const arg2),
						   void (*const free_data) (const void * const data),
						   const char * const config_mask, ...)
{
	va_list arg_list = NULL;

	va_start(arg_list, config_mask);

	return vcreate_list(type, data_alloc_size,comparator, free_data, config_mask,
						arg_list);
}

int16_t vcreate_list(gccl_list_type_t type, size_t data_alloc_size,
                         int8_t (*const comparator) (const void * const arg1,
                                                     const void * const arg2),
						 void (*const free_data) (const void * const data),
						 const char * const config_mask, va_list arg_list)
{
    switch(type){

            case GCCL_ARRAY_LIST:


            case GCCL_DOUBLY_LINKED_LIST:

                return -1;

            case GCCL_LINKED_LIST:

                return vcreate_LL(data_alloc_size, comparator, free_data, config_mask,
                				  arg_list);

            default:

                return -1;
        }
}


int delete_element_list(gccl_list_type_t type, const void * const data,
					    delete_mode_t delete_mode, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return -1;

		case GCCL_LINKED_LIST:

			return delete_element_LL(data, delete_mode, list_index);

		default:

			return -1;
	}
}


bool destroy_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return false;

		case GCCL_LINKED_LIST:

			return destroy_LL(list_index);

		default:

			return false;
	}

}


void destroy_array_copy_list(const void * array_list_ptr)
{

	destroy_array_copy_LL(array_list_ptr);
}

bool destroy_ALL_lists(gccl_list_type_t type)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return false;

		case GCCL_LINKED_LIST:

			return destroy_ALL_LL();

		default:

			return false;
	}
}


bool exists_element_in_list(gccl_list_type_t type, const void * const data,
						    int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return false;

		case GCCL_LINKED_LIST:

			return exists_element_in_LL(data, list_index);

		default:

			return false;
	}

}

void * get_array_copy_list(gccl_list_type_t type, int16_t list_index,
						   void (*const element_copy)(const void * const restrict to,
								                      const void * const restrict data,
								                      int position))
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_array_copy_LL(list_index, element_copy);

		default:

			return NULL;
	}
}

void * get_and_delete_element_list(gccl_list_type_t type, const void * const data,
								   delete_mode_t delete_mode, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_and_delete_element_LL(data, delete_mode, list_index);

		default:

			return NULL;
	}

}

void * get_and_delete_head_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_and_delete_head_LL(list_index);

		default:

			return NULL;
	}
}


void * get_and_delete_tail_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_and_delete_tail_LL(list_index);

		default:

			return NULL;
	}
}


void * get_element_list(gccl_list_type_t type, const void * const data, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_element_LL(data, list_index);

		default:

			return NULL;
	}

}

void * get_element_at_position_list(gccl_list_type_t type, int position,
                                    int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_element_at_position_LL(position, list_index);

		default:

			return NULL;
	}

}

void * get_list_head(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_head_LL(list_index);

		default:

			return NULL;
	}

}


int get_size_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return -1;

		case GCCL_LINKED_LIST:

			return get_size_LL(list_index);

		default:

			return -1;
	}
}

void * get_tail_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return NULL;

		case GCCL_LINKED_LIST:

			return get_tail_LL(list_index);

		default:

			return NULL;
	}

}

long initialise_service_list(gccl_list_type_t type, gccl_alloc_t rule,
							 int16_t initial_number_of_lists)
{
	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return -1L;

		case GCCL_LINKED_LIST:

		default:

			return initialise_LL_service(rule, initial_number_of_lists);
	}
}

int insert_element_list(gccl_list_type_t type, const void * const data, insert_mode_t insert_mode,
                        int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return -1;

		case GCCL_LINKED_LIST:

			return insert_element_LL(data, insert_mode, list_index);

		default:

			return -1;
	}
}

bool isEmpty_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return false;

		case GCCL_LINKED_LIST:

			return isEmpty_LL(list_index);

		default:

			return false;
	}

}

bool isFull_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return false;

		case GCCL_LINKED_LIST:

			return isFull_LL(list_index);

		default:

			return false;
	}

}

bool reset_list(gccl_list_type_t type, int16_t list_index)
{

	switch(type){

		case GCCL_ARRAY_LIST:


		case GCCL_DOUBLY_LINKED_LIST:

			return false;

		case GCCL_LINKED_LIST:

			return reset_LL(list_index);

		default:

			return false;
	}
}

bool stop_all_services_list(void)
{
	return stop_LL_service();
}

extern bool stop_service_list_of_type(gccl_list_type_t type)
{
	return stop_LL_service_of_type(type);
}

bool stop_service_list(long reference_id)
{
	return stop_LL_service_of_reference_id(reference_id);
}


/* ********** END - GENERIC LIST PUBLIC INTERFACE IMPLEMENTATION ********** */

