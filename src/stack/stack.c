/*
*****************************************************************************************
*
*Copyright (c) 2013-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: stack.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include <stdarg.h>

#include "stack/stack.h"

static gccl_list_type_t foundation_list_type = GCCL_LINKED_LIST;

int16_t create_stack(size_t data_alloc_size,
                     int8_t (*const comparator) (const void * const arg1,
                                                 const void * const arg2),
					 void (*const free_data) (const void * const data),
                     const char * const config_mask, ...)
{

  va_list arg_list = NULL;

  va_start(arg_list, config_mask);

  return vcreate_list(foundation_list_type, data_alloc_size, comparator, free_data,
		  	  	  	  config_mask, arg_list);

}

bool destroy_ALL_stacks()
{

	return destroy_ALL_lists(foundation_list_type);
}

bool destroy_stack(int16_t index_stack)
{
	return destroy_list(foundation_list_type,index_stack);
}

bool exists_element_in_stack(void * const data,
						     int16_t index_stack)
{
	return exists_element_in_list(foundation_list_type, data, index_stack);
}

int get_size_stack(int16_t index_stack)
{
	return get_size_list(foundation_list_type, index_stack);
}

bool isEmpty_stack(int16_t index_stack)
{
  return isEmpty_list(foundation_list_type,index_stack);
}

bool isFull_stack(int16_t index_stack)
{
  return isFull_list(foundation_list_type,index_stack);
}

long initialise_service_stack(gccl_alloc_t rule, gccl_list_type_t base_list_type,
							  int16_t initial_number_of_stacks)
{
	/*
	 * Check if the argument is a valid type of list.
	 * If not, return false.
	 */

	foundation_list_type = base_list_type;

	switch(foundation_list_type){

		case GCCL_ARRAY_LIST:
		case GCCL_DOUBLY_LINKED_LIST:
		case GCCL_LINKED_LIST:

			break;

		default:

			return -1L;
	}

	return initialise_service_list(foundation_list_type, rule, initial_number_of_stacks);
}

int push_element_stack(void* const data, int16_t index_stack)
{
  return insert_element_list(foundation_list_type,data,INS_FIRST,index_stack);
}

void* pop_element_stack(int16_t index_stack)
{
  return get_and_delete_head_list(foundation_list_type, index_stack);
}

bool stop_service_stack(long reference_id)
{
	return stop_service_list(reference_id);
}
