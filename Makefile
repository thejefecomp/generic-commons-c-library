#
#########################################################################################
#
#Copyright (c) 2013-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
#All rights reserved.
#Project: Generic_Commons_C_Library
#
#Filename: Makefile
#
#This file is part of Generic_Commons_C_Library project.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#* Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.
#
#* Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.
#
#* Neither the name of my-courses nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#########################################################################################
#

#Useful command's definitions
ARCH_CMD = /usr/bin/arch

ECHO = /bin/echo

GATHERHEADERDOC = /usr/bin/gatherheaderdoc

GIT = /usr/bin/git

GREP = /usr/bin/grep

HEADERDOC2HTML = /usr/bin/headerdoc2html

MACHINE_CMD = /usr/bin/machine

MKDIR = /bin/mkdir

LS = /bin/ls

PWD = /bin/pwd

RM = /bin/rm -rf

SHELL = /bin/sh

UNAME = /usr/bin/uname

#Paths for source, include, and work directories
#/gen_commons_c_library
DEFAULT_RELATIVE_LIBRARY_PATH = lib

DEFAULT_LIBRARY_INSTALL_PATH = /usr/$(DEFAULT_RELATIVE_LIBRARY_PATH)

PATH_BIN_DIR = $(PATH_WORK_DIR)/bin

PATH_DOC_DIR = $(PATH_WORK_DIR)/docs

PATH_HEADER_DOC_CONFIG_FILE = $(PATH_WORK_DIR)/Generic_Commons_C_Library.config

PATH_HEADER_DOC_HDOC_FILE = $(PATH_WORK_DIR)/Generic_Commons_C_Library.hdoc

PATH_INCLUDE_DIR = $(PATH_WORK_DIR)/inc

PATH_LIB_DIR = $(PATH_WORK_DIR)/lib

PATH_RESOURCES_DIR = $(PATH_WORK_DIR)/resources 

PATH_SRC_DIR = $(PATH_WORK_DIR)/src

PATH_TESTS_DIR = $(PATH_WORK_DIR)/tests

PATH_WORK_DIR = $(shell $(PWD))

#Operating Systems Shared Library Conventions

TARGET_OS = $(shell $(UNAME))

#LANGUAGE OF THE LIBRARY
LANGUAGE = 'c'

ifndef ARCH

	ARCH = $(shell $(ARCH_CMD))
	
	MACHINE = $(shell $(MACHINE_CMD))
	
	ifneq ($(ARCH),$(MACHINE))
	
		WARNING_MSG = "*****WARNING: arch (${ARCH}) and machine (${MACHINE})" 
		WARNING_MSG += "commands are returning different architectures for the" 
		WARNING_MSG += "same hardware. Check official information from your" 
		WARNING_MSG += "hardware manufacturer regarding the machine processor's" 
		WARNING_MSG += "model, and allowed compilation options for the given" 
		WARNING_MSG += "specialised hardware architecture (${MACHINE})."
		WARNING_MSG += "For testing purposes of the Generic Commons C Library"
		WARNING_MSG += "(GCCL), we are using the value returned by arch (${ARCH})."
		
	endif
	
	ifneq ($(ARCH),avr)
	
		ARCH_TARGET = no-avr
		
	else
		
		ARCH_TARGET = avr
	
	endif
	
endif

#Definitions valid for arm64, arm64e, x86, and x86_64 architectures
ifeq ($(ARCH_TARGET),no-avr)

	#Use CLANG compiler by default.	
	ifndef CC
	
		CC = /usr/bin/clang
		
		ifeq ($(SANITISE),undefined)
		
			CFLAGS += -fsanitize=undefined
			
		endif
		
	endif

	CFLAGS += -Wall -O3 -faccess-control -faddrsig 
	CFLAGS += -fcommon -fkeep-static-consts 
	CFLAGS += -flto -fPIC -fPIE -ftls-model=local-exec -x ${LANGUAGE}
	
	ifdef DEBUG

		CFLAGS += -g -v -fexceptions -fshow-source-location 
		CFLAGS += -fdiagnostics-fixit-info -faddrsig -integrated-as 
		CFLAGS += -fcolor-diagnostics -fprofile-instr-generate 
		CFLAGS += -fcoverage-mapping -fsanitize-address-use-after-scope
		
	endif
	
	ifdef CPU
	
		CFLAGS += -mcpu=${CPU}
		
	endif

	#Static library is the default library mode
	ifeq ($(LIB_MODE),shared)

		CC_LIB = $(CC)

		LD_FLAGS += -shared
		
		LD_FLAGS += -Wl,-arch,${ARCH},-ld_new,-random_uuid,-rpath,'$$ORIGIN'/$(DEFAULT_RELATIVE_LIBRARY_PATH)
		
		ifeq ($(TARGET_OS),Darwin)
		
			LIB_SUFFIX = dylib
			
			LINKER_OBJ_OPT= -dylib
			LINKER_OBJ_OPT+= -compatibility_version ${MINIMUM_VERSION}
			LINKER_OBJ_OPT+= -current_version ${CURRENT_VERSION}
		else

			LIB_SUFFIX = so
			
		endif
		
		LINKER_OBJ_OPT+= -o
		
		TEST_EXEC_SUFFIX = shared

	else

		CC_LIB = /usr/bin/ar

		LD_FLAGS += -rcs

		LIB_SUFFIX = a
		
		TEST_EXEC_SUFFIX = static

	endif

endif

#Definition of variable's values for avr architectures
ifeq ($(ARCH_TARGET),avr)

	PROJECT = GEN_COMMONS_C_LIBRARY

	CC = /usr/bin/avr-gcc

	CC_LIB = /usr/bin/avr-ar

	CFLAGS = -Wall -Werror -g -Wundef -std=c99 -Os

	LD_FLAGS += -rcs

	LIB_SUFFIX = a

	#LDFLAGS := -Wl,-Map=$(PATH_BIN_DIR)/$(PROJECT).map

	ifndef MCU

		$(error The target mcu is not defined. You have to define it!)

	else

		CFLAGS += -mmcu=$(MCU)

	endif

endif

#Names of generated executables and libraries

MINIMUM_VERSION=0.0.1

CURRENT_VERSION=${MINIMUM_VERSION}

#Libraries linker names
GEN_COMMONS_C_LIBRARY=gen_commons_c-${CURRENT_VERSION}
GEN_COMMONS_C_LIST=gen_commons_c_list-${CURRENT_VERSION}
GEN_COMMONS_C_QUEUE=gen_commons_c_queue-${CURRENT_VERSION}
GEN_COMMONS_C_STACK=gen_commons_c_stack-${CURRENT_VERSION}
GEN_COMMONS_C_TREE=gen_commons_c_tree-${CURRENT_VERSION}

#Libraries names
LIBRARY_NAME_COMPLETE = lib${GEN_COMMONS_C_LIBRARY}.$(LIB_SUFFIX)

LIBRARY_NAME_LIST = lib${GEN_COMMONS_C_LIST}.$(LIB_SUFFIX)

LIBRARY_NAME_QUEUE = lib${GEN_COMMONS_C_QUEUE}.$(LIB_SUFFIX)

LIBRARY_NAME_STACK = lib${GEN_COMMONS_C_STACK}.$(LIB_SUFFIX)

LIBRARY_NAME_TREE = lib${GEN_COMMONS_C_TREE}.$(LIB_SUFFIX)

#List of objects resulting from source compilation
OBJECTS = $(OBJECTS_LIST)\
${OBJECTS_QUEUE}\
$(OBJECTS_STACK)

OBJECTS_LIST = $(PATH_BIN_DIR)/list/doubly_linked_list.o\
$(PATH_BIN_DIR)/list/linked_list_mgmt.o\
$(PATH_BIN_DIR)/list/linked_list.o\
$(PATH_BIN_DIR)/list/list.o\
$(PATH_BIN_DIR)/list/list_utils.o

OBJECTS_QUEUE = $(PATH_BIN_DIR)/queue/queue.o\
$(PATH_BIN_DIR)/queue/pqueue.o\
$(PATH_BIN_DIR)/queue/pqueue_ts.o

OBJECTS_STACK = $(PATH_BIN_DIR)/stack/stack.o


#Compilation targets

.PHONY: all
.SILENT: compile

all: build-sources build-libs build-test_LL build-test_stack

$(PATH_BIN_DIR):

	$(MKDIR) -p $@/list $@/queue $@/stack $@/test/statistics $@/test/list/test-reports $@/test/queue/test-reports $@/test/stack/test-reports

$(PATH_LIB_DIR):

	$(MKDIR) -p $@/list $@/queue $@/stack 
	
$(PATH_DOC_DIR):

	$(MKDIR) -p $@/api
	
${PATH_RESOURCES_DIR}:

	${MKDIR} $@

build-sources:	$(PATH_BIN_DIR) $(PATH_LIB_DIR) $(OBJECTS)

build-libs: build-sources $(PATH_LIB_DIR)/$(LIBRARY_NAME_COMPLETE) $(PATH_LIB_DIR)/list/$(LIBRARY_NAME_LIST) $(PATH_LIB_DIR)/queue/$(LIBRARY_NAME_QUEUE) $(PATH_LIB_DIR)/stack/$(LIBRARY_NAME_STACK)

clean: clean-docs

	$(RM) $(PATH_BIN_DIR) $(PATH_LIB_DIR)

clean-docs:

	$(RM) $(PATH_DOC_DIR)

compile: build-sources build-libs

	$(ECHO) "BUILD SUCCESS"
	$(ECHO) ${WARNING_MSG}

docs: $(PATH_DOC_DIR)

	$(HEADERDOC2HTML) -H -S -g --tocformat iframes -o $(PATH_DOC_DIR)/api -c \
	$(PATH_HEADER_DOC_CONFIG_FILE) $(PATH_HEADER_DOC_HDOC_FILE) $(PATH_INCLUDE_DIR)

	$(GATHERHEADERDOC) $(PATH_DOC_DIR)/api index.html

#Compiling object files
$(PATH_BIN_DIR)/%.o : $(PATH_SRC_DIR)/%.c
	$(CC) $(LDFLAGS) -c $(CFLAGS) -I $(PATH_INCLUDE_DIR) -o $@ $<

#Compiling GEN_COMMONS_C_LIBRARY 
$(PATH_LIB_DIR)/$(LIBRARY_NAME_COMPLETE): $(OBJECTS)
	$(CC_LIB) $(LD_FLAGS) $(LINKER_OBJ_OPT) ./lib/$(LIBRARY_NAME_COMPLETE) $(OBJECTS)

#Compiling GEN_COMMONS_C_LIST_LIBRARY 
$(PATH_LIB_DIR)/list/$(LIBRARY_NAME_LIST): $(OBJECTS_LIST)
	$(CC_LIB) $(LD_FLAGS) $(LINKER_OBJ_OPT) ./lib/list/$(LIBRARY_NAME_LIST) $(OBJECTS_LIST)
	
#Compiling GEN_COMMONS_C_QUEUE_LIBRARY 
$(PATH_LIB_DIR)/queue/$(LIBRARY_NAME_QUEUE): $(OBJECTS_LIST) $(OBJECTS_QUEUE)
	$(CC_LIB) $(LD_FLAGS) $(LINKER_OBJ_OPT) ./lib/queue/$(LIBRARY_NAME_QUEUE) $(OBJECTS_LIST) $(OBJECTS_QUEUE)
	
#Compiling GEN_COMMONS_C_STACK_LIBRARY 
$(PATH_LIB_DIR)/stack/$(LIBRARY_NAME_STACK): $(OBJECTS_LIST) $(OBJECTS_STACK)
	$(CC_LIB) $(LD_FLAGS) $(LINKER_OBJ_OPT) ./lib/stack/$(LIBRARY_NAME_STACK) $(OBJECTS_LIST) $(OBJECTS_STACK)
	

#Including tests' Makefile
include $(PATH_TESTS_DIR)/Makefile