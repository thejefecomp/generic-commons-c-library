/*
*****************************************************************************************
*
*Copyright (c) 2013-2024, NeartWord, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: stack.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

/*!
 * @header stack.h
 *
 * This header file defines the functions present in the implementation of the Stack
 * Data Structure, which is provided by the Generic Commons C Library (GCCL).
 *
 * @author Jeferson Souza (thejefecomp) - thejefecomp\@neartword.com.
 *
 * @copyright 2013-2022 NeartWord, Jeferson Souza (thejefecomp) - thejefecomp\@neartword.com. All Rights Reserved.
 *
 */

#ifndef STACK_H_
#define STACK_H_

/* Include of Generic_Commons_C_Library headers */

#include "list/list.h"

#ifdef __cplusplus
extern "C" {
#endif


/* ********** BEGIN - STACK PUBLIC INTERFACE PROTOTYPES ********** */

/*!
 *
 * @function create_stack
 *
 * @description Creates a new stack using a given data allocation size, and
 * 				function for element comparison. Upon successful creation, it returns
 * 				a stack index, which is utilised for performing stack manipulation. The
 * 				comparator pointer represents the callback function, which is utilised
 * 				internally for comparison purposes. The signature of this callback
 * 				function receives two arguments, arg1 and arg2, which are the data
 * 				elements to be compared. The comparator callback function returns
 * 				a value of zero (0) if arg1 == arg2, i.e., they hold the same value;
 * 				a value greater than zero (0) if arg1 > arg2, i.e., the first
 *              argument is greater than the second one; or a value lesser than
 *              zero (0) if arg1 < arg2, i.e., the first argument is lesser than
 *              the second one. The function returns a value greater than zero (1)
 *              representing the index of the stack if the function executes successfully;
 *              -1 if there is a problem in the initialisation of the stack service;
 *              -2 if the number of lists holding stacks has exceeded its maximum
 *              value in a STATIC configuration; or -3 if there is a problem
 *              allocating memory for internal storage.
 *
 * @param data_alloc_size represents the maximum size of the memory block
 * 		  required to store the data in each element of a stack.
 *
 * @param comparator represents the comparator function provided by the
 * 		  user (i.e., developer) to compare elements of the created stack.
 *
 * @param free_data represents the function utilised to execute special purpose
 * 		  instructions to free a given element from memory.
 *
 * @param config_mask represents the order and meaning of each additional
 * 		  parameter passed to this function. Actually, there are three different
 * 		  types of parameter we can pass to the create_list() function when
 * 		  creating a new stack:
 *
 * 		  s -> represents the maximum size the created stack can hold;
 *
 * 		  l -> represents the locked status associated with the maximum size
 * 		  	   of the created stack.
 *
 * 		  u -> represents the unlocked status associated with the maximum size
 * 		  	   of the created stack.
 *
 * @result returns a value greater than zero (1) representing the index of the
 *         stack if the function executes successfully; -1 if there is a problem
 *         in the initialisation of the stack service; -2 if the number of lists
 *         holding stacks has exceeded its maximum value in a STATIC configuration;
 *         or -3 if there is a problem allocating memory for internal storage.
 */
extern  int16_t create_stack(size_t data_alloc_size,
                             int8_t (*const comparator) (const void * const arg1,
                                                         const void * const arg2),
							 void (*const free_data) (const void * const data),
                             const char * const config_mask, ...);

/*!
 *
 * Destroy all stacks managed by the service.
 */
extern bool destroy_ALL_stacks(void);

/*!
 *
 * Destroy the stack represented by a given index.
 */
extern bool destroy_stack(int16_t index_stack);


extern bool exists_element_in_stack(void * const data,
								   int16_t index_stack);

/*!
 *
 * Return the size of the stack
 */
extern int get_size_stack(int16_t index_stack);

/*!
 *
 * Initialise the stack service
 */
extern long initialise_service_stack(gccl_alloc_t rule, gccl_list_type_t base_list_type,
									 int16_t initial_number_of_stacks);

/*!
 *
 * Test whether the stack is empty.
 */
extern bool isEmpty_stack(int16_t index_stack);

/*!
 *
 * Test whether the stack is full.
 */
extern bool isFull_stack(int16_t index_stack);

/*!
 *
 * Retrieve (and remove) the data element on the top of the stack.
 */
extern void * pop_element_stack(int16_t index_stack);

/*!
 *
 * Insert a new data element on the top of he stack.
 */
extern int push_element_stack(void* const data, int16_t index_stack);

extern bool stop_service_stack(long reference_id);

/* ********** BEGIN - STACK PUBLIC INTERFACE PROTOTYPES ********** */

#ifdef __cplusplus
}
#endif

#endif /* STACK_H_ */
