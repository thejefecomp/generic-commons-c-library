/*
*****************************************************************************************
*
*Copyright (c) 2013-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: linked_list_mgmt.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

/*!
 * @header linked_list_mgmt.h
 *
 * This header file defines the internal data types and functions utilised
 * to manage the Generic Common C Library (GCCL) data structure services. A GCCL
 * user must not include this header file directly into his/her own source code;
 * the specific data-structure's header file must be included instead, as the
 * linked_list_mgmt.h header will be neither installed nor included by binary
 * versions of the library.
 *
 */

#ifndef LINKED_LIST_MGMT_H_
#define LINKED_LIST_MGMT_H_

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include "commons/commons_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ********** BEGIN - LINKED LIST INTERNAL DATA TYPE DECLARATIONS ********** */

typedef struct gccl_ds_metadata
{
	gccl_alloc_t alloc_length_rule;
    int max_nr_structures;
    int nr_structures;
    gccl_uds_type_t str_type;
}gccl_ds_metadata_t;

typedef struct gccl_ds_metadata_lib
{
	gccl_ds_metadata_t ds_metadata[GCCL_DICTIONARY+1];

	int total_max_nr_structures;

	int total_nr_structures;

}gccl_ds_metadata_lib_t;

typedef struct gccl_element_LL
{
	void *data;
	time_t delete_time; //Only handled during delete operations
	struct gccl_element_LL *next;
	struct gccl_element_LL *previous; //Only handled by Doubly Linked Lists
	unsigned int is_keeping_data : 1; //Only handled during delete operations
	/*
	 * It's only assigned if, and only if the following conditions hold:
	 *
	 * (1) a special procedure is required to free the element from memory;
	 *
	 * (2) when the element gets in the garbage.
	 *
	 */
	void (*free_data_func) (const void * const data);
}gccl_element_LL_t;

typedef struct gccl_list_header_LL
{
	size_t data_alloc_size;

	long reference_id;

	/*
	 * 0 implies unlock, i.e., read-write; and 1 implies lock, i.e.,
	 *          read-only linked list.
	 */
	unsigned int lock_LL : 1;

	/*
	 * 0 implies unlock, and 1 implies lock max_size.
	 */
	unsigned int lock_max_size : 1;

	int max_size;

	int size;

	gccl_uds_type_t element_str_type;

	gccl_element_LL_t *head;

	gccl_element_LL_t *tail;

	int8_t (*comparator_func) (const void *const arg1, const void *const arg2);
	/*
	 * Enables defining a function to erase stored data from memory, which require
	 * special procedures to do so.
	 */
	void (*free_data_func) (const void * const data);

}gccl_list_header_LL_t;


typedef enum gccl_memory_type
{
	GCCL_MEM_DATA = false,
	GCCL_MEM_MANAGEMENT = true
}gccl_memory_type_t;

/* ********** END - LINKED LIST INTERNAL DATA TYPE DECLARATIONS ********** */


/* ********** BEGIN - LINKED LIST INTERNAL MANAGEMENT INTERFACE ********** */


/*
 * @function gccl_mgmt_bin_off_indication
 *
 * @description Indicates that any data retrieved by the user's application and
 * 				deleted from a given data structure managed by the Generic Common
 * 				C Library (GCCL), at a point of time previous to the call of this
 * 				function, will be erased from memory. As a consequence, any reference
 * 				to those data will be in an inconsistent state (eventually) and
 * 				could not be further utilised to perform any additional computation.
 */
extern void gccl_mgmt_bin_off_indication(void);

/*!
 *
 * @function gccl_mgmt_check_index_LL
 *
 * @description Checks if a given index is whether valid. The function returns
 * 				true if the given index is valid; false, otherwise.
 *
 * @param index_LL represents the index for the the linked list carrying a data
 * 		  structure, which the function checks the existence.
 *
 * @result returns true if the given index is valid; false, otherwise.
*/

extern bool gccl_mgmt_check_index_LL(int16_t index_LL);

/*!
 *
 * @function gccl_mgmt_check_LL_type
 *
 * @description Checks if a given index is whether valid, and for valid indexes,
 * 				if the stored type matches the type given as a parameter.
 * 				This function returns true if the index is valid and the type
 * 				matches; false, otherwise.
 *
 * @param index_LL represents the index for the linked list carrying a data structure
 * 		  with a given type, which the function checks the existence.
 *
 * @param element_str_type represents the type the linked list is carrying on, which
 * 		  will be checked against the type stored by the Generic Commons C Library
 * 		  (GCCL).
 *
 * @param abs_ds_type represents the abstract type the linked list is carrying on,
 * 		  which will be checked by this function.
 *
 * @result returns true if the index is valid and the type matches; false, otherwise.
 *
 */
extern bool gccl_mgmt_check_LL_type(int16_t index_LL, gccl_uds_type_t element_str_type,
							   	    gccl_abs_ds_type_t abs_ds_type);

/*!
 *
 * @function gccl_mgmt_check_reference_id
 *
 * @description Checks if a given reference_id exists in the Generic Commons C
 * 				Library (GCCL) data structure index. The function returns true if
 * 				a given reference_id exists; false, otherwise.
 *
 * @param reference_id represents a given reference_id, which the existence is
 * 		  checked.
 *
 * @result returns true if a given reference_id exists; false, otherwise.
 *
 */
extern bool gccl_mgmt_check_reference_id(long reference_id);

/*!
 *
 * @function gccl_mgmt_copy_and_delete_element_data_LL
 *
 * @description Copy and delete element data from a given linked list.
 *
 * @param data_ptr represents the pointer to the pointer holding the reference
 * 		  to the data the function's caller is interested in.
 *
 * @param element_del represents the element that the data will be extracted from.
 * 		  if requested, this element is also deleted.
 *
 * @param previous_del represents the previous element on the linked list, which
 * 		  is utilised by the delete process to keep the linked list consistent.
 *
 * @param perform_delete indicates whether element_del will be deleted. If true,
 * 		  element_del is deleted; if false, element_del is not removed.
 *
 * @param copy_data indicates whether the data associated with the element_del needs
 * 		  to be kept, and therefore, returned through the pointer to the pointer
 * 		  data_ptr. If true, the data is kept; if false, data is also removed
 * 		  from memory.
 *
 * @param index_LL represents the index of the linked list, in which the
 * 		  operation will be performed.
 */

extern void gccl_mgmt_copy_and_delete_element_data_LL(const void ** data_ptr,
											     	  gccl_element_LL_t * const restrict element_del,
													  gccl_element_LL_t * const restrict previous_del,
													  bool perform_delete,
													  bool copy_data,
													  int16_t index_LL);

/*!
 *
 * @function gccl_mgmt_create_LL
 *
 * @description Creates a new managed linked list. The function returns the
 * 				index of a new managed linked list if it has been created
 * 				successfully; -1, otherwise.
 *
 * @param data_alloc_size represents the maximum size of the memory block
 * 		  required to store the data in each element of a linked list.
 *
 * @param comparator represents the comparator function provided by the
 * 		  user (i.e., developer) to compare elements of the created linked list.
 *
 * @param free_data represents the function utilised to execute special purpose
 * 		  instructions to free a given element from memory.
 *
 * @param str_type represents the type of the data structure the created linked list
 * 		  will be carrying on. This parameter must be specified internally by
 * 		  the Generic Commons C Library (GCCL) when creating linked lists with
 * 		  different purposes, which includes the specific support for more advanced
 * 		  data structures.
 *
 * @param config_mask represents the order and meaning of each additional
 * 		  parameter passed to this function. Actually, there are three different
 * 		  types of parameter we can pass to the mgmt_create_LL() function when
 * 		  creating a new linked list:
 *
 * 		  s -> representing the maximum size the created linked list can hold;
 *
 * 		  l -> representing the locked status associated with the maximum size
 * 		  	   of the created linked list.
 *
 * 		  u -> representing the unlocked status associated with the maximum size
 * 		  	   of the created linked list.
 *
 *  @result returns a value greater than 1 (i.e. > 1), representing the index of
 *  		the linked list, if the function executes successfully; -1 if there is
 *  		a problem in the initialisation of the Generic Commons C Library (GCCL)
 *  		manager; -2 if the number of linked lists carrying a given data structure
 *  		type has exceeded its maximum value in a STATIC configuration; or -3 if
 *          there is a problem allocating memory for internal storage.
 *
 * */

extern int16_t gccl_mgmt_create_LL(size_t data_alloc_size,
                              	   int8_t (*const comparator) (const void * const arg1,
                                                          	   const void * const arg2),
								   void (*const free_data) (const void * const data),
								   gccl_uds_type_t str_type, const char * const config_mask, ...);

/*!
*
* @function gccl_vmgmt_create_LL
*
* @description Variation of the mgmt_create_LL() function, replacing the variable
* 			   argument declaration by the va_list data type parameter, which
* 			   holds the list of arguments initialised by the va_start() function.
*
* @param data_alloc_size represents the maximum size of the memory block
* 		  required to store the data in each element of a linked list.
*
* @param comparator represents the comparator function provided by the
* 		  user (i.e., developer) to compare elements of the created linked list.
*
* @param free_data represents the function utilised to execute special purpose
* 		 instructions to free a given element from memory.
*
* @param element_str_type represents the type of the data structure the created
* 		 linked list will be carrying on. This parameter must be specified internally
* 		 by the Generic Commons C Library (GCCL) when creating linked lists with
* 		 different purposes, which includes the specific support for more advanced
* 		 data structures.
*
* @param config_mask represents the order and meaning of each additional
* 		  parameter passed to this function. Actually, there are three different
* 		  types of parameter we can pass to the mgmt_create_LL() function when
* 		  creating a new linked list:
*
* 		  s -> representing the maximum size the created linked list can hold;
*
* 		  l -> representing the locked status associated with the maximum size
* 		  	   of the created linked list.
*
* 		  u -> representing the unlocked status associated with the maximum size
* 		  	   of the created linked list.
*
* 		  i -> representing the identifier given header is associated to, which
* 		  	   allows enhancing the distributed-nature operation of the Generic
* 		  	   Commons C Library (GCCL).
*
* @param arg_list allows accessing the list of variable arguments indicated by
* 	     the config_mask.
*
* @result returns a value greater than 1 (i.e. > 1), representing the index of
*  		  the linked list, if the function executes successfully; -1 if there is
*  		  a problem in the initialisation of the Generic Commons C Library (GCCL)
*  	  	  manager; -2 if the number of linked lists carrying a given data structure
*  	  	  type has exceeded its maximum value in a STATIC configuration; or -3 if
*         there is a problem allocating memory for internal storage.
*
* */

extern int16_t gccl_vmgmt_create_LL(size_t data_alloc_size,
                               	    int8_t (*const comparator) (const void * const arg1,
                                                           	    const void * const arg2),
									void (*const free_data) (const void * const data),
									gccl_uds_type_t element_str_type,
									const char * const config_mask, va_list arg_list);

/*
 * @function gccl_mgmt_free
 *
 * @description Helps the task of freeing memory utilised by services
 * 				and tasks related to the Generic Commons C Library (GCCL).
 *
 * @param memory_type represents the type of memory zone that gets returned.
 * 					  At the moment, there are only two types: DATA and MANAGEMENT.
 *
 * @param memblock_ptr represents the pointer to the memory block that needs to
 * 					   be freed.
 */
extern void gccl_mgmt_free(gccl_memory_type_t memory_type, const void * memblock_ptr);

/*!
 *
 * @function gccl_mgmt_free_LL
 *
 * @description Removes the linked list represented by a given index. This function
 * 				returns true if the linked list, represented by a given index,
 * 				is removed successfully; false, otherwise.
 *
 * @param index_LL represents the index of the linked list, in which the
 * 		  operation will be performed.
 *
 * @result returns true if the linked list, represented by a given index, is
 * 		   removed successfully; false, otherwise.
 * */
extern bool gccl_mgmt_free_LL(int16_t index_LL);


/*!
 *
 * @function gccl_mgmt_free_all_LL
 *
 * @description Removes all managed linked lists carrying any data structure type,
 * 				stopping the Generic Commons C Library (GCCL) manager. This function
 * 				returns true if all managed data structures are removed successfully;
 * 				false, otherwise.
 *
 * 	@result returns true if all managed data structures are removed successfully;
 * 			false, otherwise.
 * */
extern bool gccl_mgmt_free_all_LL();


extern bool gccl_mgmt_free_all_referenced_LL(long reference_id);


/*!
 *
 * @function gccl_mgmt_free_all_typed_LL
 *
 * @description Removes all managed linked lists carrying a given data structure type.
 * 				This function returns true if all managed linked lists of a given
 * 				data structure type are removed successfully; false, otherwise.
 *
 * 	@result returns true if all managed linked lists of a given data structure type
 * 			are removed successfully; false, otherwise.
 * */
extern bool gccl_mgmt_free_all_typed_LL(gccl_uds_type_t element_str_type,
								   	    gccl_abs_ds_type_t abs_ds_type);

/*!
 *
 * @function gccl_mgmt_free_array_copy_LL
 *
 * @description Removes the allocated memory of an array copied from one of the
 * 				linked lists managed by the Generic Commons C Library (GCCL).
 *
 * @param array_list_ptr represents the pointer to the array that needs to be
 * 		  freed.
 */
extern void gccl_mgmt_free_array_copy_LL(const void * array_list_ptr);


/*!
 *
 * @function gccl_mgmt_free_element_LL
 *
 * @description Removes a given element from the linked list represented by a
 * 				given index. This function returns true if the given element is
 * 				removed successfully; false, otherwise.
 *
 * @param previous represents the element of the linked list previous to the
 * 		  element being removed.
 *
 * @param element represents the element to be removed from the linked list.
 *
 * @param is_keeping_data represents a boolean state indicating whether the Generic
 * 		  Commons C Library (GCCL) needs to prevent data from being deleted
 * 		  until explicitly requested to.
 *
 * @param index_LL represents the index of the linked list, in which the
 * 		  operation will be performed.
 *
 * @result returns true if the given element is removed successfully;
 * 		   false, otherwise.
 *
 */
extern bool gccl_mgmt_free_element_LL(gccl_element_LL_t * const previous,
                                 	  gccl_element_LL_t * const element,
									  bool is_keeping_data,
									  int16_t index_LL);

/*!
 *
 * @function gccl_mgmt_generate_reference_id
 *
 * @description Generates a unique identifier, which can be utilised in other
 * 				functions that need a reference_id. This function returns a
 * 				unique identifier greater than or equal to zero (0); minus one (-1),
 * 				otherwise.
 *
 * @result returns a unique identifier greater than or equal to zero (0); minus
 * 		   one (-1), otherwise.
 *
 */
extern long gccl_mgmt_generate_reference_id(void);


/*!
 *
 * @function gccl_mgmt_get_LL
 *
 * @description Returns the linked list represented by a given index. This
 * 				function returns a pointer to the header of a linked list if the
 * 				given index is valid. NULL, otherwise.
 *
 *  @param index_LL represents the index of the linked list, in which the
 * 		   operation will be performed.
 *
 *  @result returns a pointer to the header of a linked list if the given index
 *  		is valid. NULL, otherwise.
 */
extern gccl_list_header_LL_t * gccl_mgmt_get_LL(int16_t index_LL);


/*!
 *
 * @function gccl_mgmt_get_array_copy_LL
 *
 * @description Returns a pointer to an array of elements, which represents a copy
 * 				from the linked list represented by a given index (index_LL).
 * 				A user function is required to perform the copy from the linked list
 * 				to the created array, in order to use the right allocated type to
 * 				perform the operation. The moment such an array is no more needed,
 * 				it must be freed by the user (i.e., developer) using either free()
 * 				or the provided mgmt_free_array_copy_LL() function.
 *
 * 	@param index_LL represents the index of the linked list, in which the
 * 		   operation will be performed.
 *
 * 	@param element_array_copy represents the user provided function dictating
 * 		   the correct data type that needs to be utilised to create the array
 * 		   copy. This function ensures that the Generic Commons C Library (GCCL)
 * 		   does not need to know the data type being stored, keeping the data
 * 		   privacy at its highest level.
 *
 * @result returns an array copy of the data stored on the linked list represented
 * 		   by a given index (index_LL); NULL, otherwise.
 *
 */
extern void * gccl_mgmt_get_array_copy_LL(int16_t index_LL,
                                     	  void (*const element_array_copy)(const void * const to,
                                     			  	  	  	  	  	  	   const void * const data,
																		   int position));

/*!
 *
 * @function gccl_mgmt_get_total_typed_LL
 *
 * @description Provides the total number of linked lists, which are storing a
 * 				data structure of a given type. This function returns the total
 * 				number of linked lists holding a given data structure type; -1,
 * 				otherwise.
 *
 * @param element_str_type represents the type of the data structure elements
 * 		  are related to. Although this parameter indicates the data structure
 * 		  being managed by the Generic Commons C Library (GCCL), the library
 * 		  uses linked list elements on its foundation for enhanced flexibility
 * 		  related to dynamic memory allocation requirements.
 *
 * @param abs_ds_type represents the abstract type of the linked list that
 * 		  will be checked by this function.
 *
 * @result returns the total number of linked lists holding a given data
 * 		   structure type; -1, otherwise.
 *
 */
extern int gccl_mgmt_get_total_typed_LL(gccl_uds_type_t element_str_type, gccl_abs_ds_type_t abs_ds_type);

/*!
 *
 * @function gccl_mgmt_initialise_LL_manager
 *
 * @description Initialises the Generic Commons C Library (GCCL) manager, which is
 * 				the general manager of all data structures supported by the GCCL.
 * 				This manager provides the foundation to store data using distinguished
 * 				types of data structures, using a simple linked list to manage both
 * 				memory reutilisation and element removal. This function returns true
 * 				if the manager is initialised successfully; false, otherwise.
 *
 * @param rule represents the allocation rule utilised to initialise the GCCL manager.
 * 		  It can assume two possible values: DYNAMIC, for dynamic memory
 * 		  allocation; or STATIC, for a fixed size of a given type of data
 * 		  structure to be managed by the service provided by the GCCL.
 *
 * @param element_str_type represents the data structure type carried on by managed
 * 		  linked lists. Although this parameter indicates the data structure
 * 		  being managed by the Generic Commons C Library (GCCL), the library
 * 		  uses linked list elements on its foundation for enhanced flexibility
 * 		  related to dynamic memory allocation requirements. Depending on the data
 * 		  structure type that has been carried on, only one linked list element
 * 		  will be created, which is the starting point to access specific elements
 * 		  of the aforementioned data structure type.
 *
 *  @param initial_number_of_DSs the initial number of data structures the GCCL
 *  	   manager is ready to manage when the corresponding specific data structure
 *  	   service is initialised.
 *
 *  @result returns true if the manager is initialised successfully; false, otherwise.
 */
extern bool gccl_mgmt_initialise_LL_manager(gccl_alloc_t rule,
											gccl_uds_type_t element_str_type,
											int16_t initial_number_of_DSs);


/*!
 *
 * @function gccl_mgmt_is_LL_manager_active
 *
 * @description Checks is the Generic Commons C Library (GCCL) manger is whether active.
 * 				This function returns true if the manager is active; false, otherwise.
 *
 *
 * @result returns true if the manager is active; false, otherwise.
 *
 */
extern bool gccl_mgmt_is_LL_manager_active(void);


/*
 * @function gccl_mgmt_malloc
 *
 * @description Helps the task of allocating memory utilised by services
 * 				and tasks related to the Generic Commons C Library (GCCL).
 *
 * @param memory_type represents the type of memory zone that gets returned.
 * 					  At the moment, there are only two types: DATA and MANAGEMENT.
 *
 * @param size represents the size of the memory block to be allocated.
 *
 * @result returns a pointer to the new memory block of size (size); NULL, otherwise.
 */
extern void * gccl_mgmt_malloc(gccl_memory_type_t memory_type, size_t size);


/*
 * @function gccl_mgmt_malloc
 *
 * @description Helps the task of reallocating memory utilised by services
 * 				and tasks related to the Generic Commons C Library (GCCL).
 *
 * @param memory_type represents the type of memory zone that gets returned.
 * 					  At the moment, there are only two types: DATA and MANAGEMENT.
 *
 *	@param memblock_ptr represents the pointer to the memory block that needs to
 * 					   be reallocated.
 *
 * @param new_size represents the new size of the memory block that needs to be
 * 				   reallocated.
 *
 * @result returns a pointer to the new memory block of size (new_size); NULL,
 * 		   otherwise.
 */
extern void * gccl_mgmt_realloc(gccl_memory_type_t memory_type, const void * memblock_ptr,
						   	   	size_t new_size);


/*!
 *
 * @function gccl_mgmt_reset_LL
 *
 * @description Removes all elements of the linked list represented by a given
 * 				index. This function returns true if all elements of the linked
 * 				list, represented by the given index, are removed successfully;
 * 				false, otherwise.
 *
 * @param index_LL represents the index of the linked list, in which the
 * 		  operation will be performed.
 *
 * @result returns true if all elements of the linked list, represented by the
 * 		   given index, are removed successfully; false, otherwise.
 */
extern bool gccl_mgmt_reset_LL(int16_t index_LL);

/*
 * @function gccl_mgmt_start_LL_manager
 *
 * @description Starts the Generic Commons C Library (GCCL) manager, which implies
 * 				in the set-up of the internal variables and data structures of the
 * 				GCCL. This function returns true if the manager is
 * 				started successfully; false, otherwise.
 *
 * @result returns true if the manager is started successfully; false, otherwise.
 */

extern bool gccl_mgmt_start_LL_manager(void);

/*
 * @function gccl_mgmt_stop_LL_manager
 *
 * @description Stops the Generic Commons C Library (GCCL) manager, which implies
 * 				in the removal of all linked lists. This function returns true if
 * 				the manager is stopped successfully; false, otherwise.
 *
 * @result returns true if the manager is stopped successfully; false, otherwise.
 */

extern bool gccl_mgmt_stop_LL_manager(void);


/* ********** END - LINKED LIST INTERNAL MANAGEMENT INTERFACE ********** */

#ifdef __cplusplus
}
#endif

#endif /* LINKED_LIST_MGMT_H_ */
