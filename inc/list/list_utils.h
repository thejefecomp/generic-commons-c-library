/*
*****************************************************************************************
*
*Copyright (c) 2013-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: list_utils.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

/*!
 * @header list_utils.h
 *
 * This header file defines the auxiliary functions and enums used by the implementation of
 * List Data Structures, all being provided by the Generic Commons C Library (GCCL).
 *
 */

#ifndef LIST_UTILS_H_
#define LIST_UTILS_H_

#include<stdio.h>
#include<stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ********** BEGIN - LINKED LIST PUBLIC DATA TYPE DECLARATIONS ********** */

/*!
 * @enum insert_mode_t
 *
 * This enum includes the distinguished options for inserting data at different
 * positions of a given list, including: ascending order; descending order;
 * first position; last position.
 */

typedef enum {INS_ASC_ORDER=-1, INS_LAST, INS_DESC_ORDER, INS_FIRST} insert_mode_t;

/*!
 * @enum delete_mode_t
 *
 * This enum includes the distinguished options for removing data at different
 * positions of a given list, including: the index of a given data content;
 * last position; first position; a valid position.
 */

typedef enum {DEL_DATA_INDEX, DEL_LAST, DEL_FIRST, DEL_AT_POS} delete_mode_t;


/* ********** END - LINKED LIST PUBLIC DATA TYPE DECLARATIONS ********** */


/* ********** BEGIN - LIST_UTILS FUNCTIONS PROTOTYPES ********** */

/*!
 * @function double_comparator
 *
 * @description Compares two double elements, using the following rule:
 *
 *              - Returns 0 if the elements [arg1 and arg2] being compared are
 *                equals;
 *
 *              - Returns 1 if the element arg1 is greater than element arg2;
 *
 *              - Returns -1 if the element arg2 is greater than element arg1.
 *
 * @param arg1 a pointer to the first double element of the comparison.
 *
 * @param arg2 a pointer to the second double element of the comparison.
 *
 * @result returns 0 if the elements [arg1 and arg2] being compared are equals;
 *         returns 1 if the element arg1 is greater than element arg2; returns
 *         -1 if the element arg2 is greater than element arg1.
 */
extern int8_t double_comparator(const void * const arg1, const void * const arg2);


/*!
 * @function double_element_array_copy
 *
 * @description Copies a double element from a pointer to an array, into a given
 *              position.
 *
 * @param to a pointer to the memory region (array) where the double element
 * 		  being copied has to be stored.
 *
 * @param from a pointer to the double element to be copied from a given list.
 *
 * @param position represents the position of the array associated with the
 *        memory region where the double element being copied will be stored.
 *
 */
extern void double_element_array_copy (const void * const to,
                                       const void * const from,
                                       int position);

/*!
 * @function float_comparator
 *
 * @description Compares two float elements, using the following rule:
 *
 *              - Returns 0 if the elements [arg1 and arg2] being compared are
 *                equals;
 *
 *              - Returns 1 if the element arg1 is greater than element arg2;
 *
 *              - Returns -1 if the element arg2 is greater than element arg1.
 *
 * @param arg1 a pointer to the first float element of the comparison.
 *
 * @param arg2 a pointer to the second float element of the comparison.
 *
 * @result returns 0 if the elements [arg1 and arg2] being compared are equals;
 *         returns 1 if the element arg1 is greater than element arg2; returns
 *         -1 if the element arg2 is greater than element arg1.
 */
extern int8_t float_comparator(const void * const arg1, const void * const arg2);


/*!
 * @function float_element_array_copy
 *
 * @description Copies a float element from a pointer to an array, into a given
 *              position.
 *
 * @param to a pointer to the memory region (array) where the float element
 * 		  being copied has to be stored.
 *
 * @param from a pointer to the float element to be copied from a given list.
 *
 * @param position represents the position of the array associated with the
 *        memory region where the float element being copied will be stored.
 *
 */
extern void float_element_array_copy (const void * const to,
                                      const void * const from,
                                      int position);

/*!
 * @function integer_comparator
 *
 * @description Compares two integer elements, using the following rule:
 *
 *              - Returns 0 if the elements [arg1 and arg2] being compared are
 *                equals;
 *
 *              - Returns 1 if the element arg1 is greater than element arg2;
 *
 *              - Returns -1 if the element arg2 is greater than element arg1.
 *
 * @param arg1 a pointer to the first integer element of the comparison.
 *
 * @param arg2 a pointer to the second integer element of the comparison.
 *
 * @result returns 0 if the elements [arg1 and arg2] being compared are equals;
 *         returns 1 if the element arg1 is greater than element arg2; returns
 *         -1 if the element arg2 is greater than element arg1.
 */
extern int8_t integer_comparator(const void * const arg1, const void * const arg2);


/*!
 * @function integer_element_array_copy
 *
 * @description Copies an integer element from a pointer to an array, into a
 *              given position.
 *
 * @param to a pointer to the memory region (array) where the integer element
 * 		  being copied has to be stored.
 *
 * @param from a pointer to the integer element to be copied from a given list.
 *
 * @param position represents the position of the array associated with the
 *        memory region where the integer element being copied will be stored.
 *
 */
extern void integer_element_array_copy (const void * const to,
                                        const void * const from,
                                        int position);


/*!
 * @function long_comparator
 *
 * @description Compares two long elements, using the following rule:
 *
 *              - Returns 0 if the elements [arg1 and arg2] being compared are
 *                equals;
 *
 *              - Returns 1 if the element arg1 is greater than element arg2;
 *
 *              - Returns -1 if the element arg2 is greater than element arg1.
 *
 * @param arg1 a pointer to the first long element of the comparison.
 *
 * @param arg2 a pointer to the second long element of the comparison.
 *
 * @result returns 0 if the elements [arg1 and arg2] being compared are equals;
 *         returns 1 if the element arg1 is greater than element arg2; returns
 *         -1 if the element arg2 is greater than element arg1.
 *
 */
extern int8_t long_comparator(const void * const arg1, const void * const arg2);


/*!
 * @function long_element_array_copy
 *
 * @description Copies an long element from a pointer to an array, into a
 *              given position.
 *
 * @param to a pointer to the memory region (array) where the long element
 * 		  being copied has to be stored.
 *
 * @param from a pointer to the long element to be copied from a given list.
 *
 * @param position represents the position of the array associated with the
 *        memory region where the long element being copied will be stored.
 *
 */
extern void long_element_array_copy (const void * const to,
                                     const void * const from,
                                     int position);


/*!
 * @function print_integer
 *
 * @description Prints a given data as integer to a given output.
 *
 * @param output a pointer to a file where the given data will be printed as
 *        integer.
 *
 * @param data a pointer to a given memory region, which the content will be
 *        printed as integer.
 */
extern void print_integer(FILE *output, const void * const data);

/* ********** END - LIST_UTILS FUNCTIONS PROTOTYPES ********** */

#ifdef __cplusplus
}
#endif

#endif /* LIST_UTILS_H_ */
