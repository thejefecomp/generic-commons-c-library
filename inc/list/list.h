/*
*****************************************************************************************
*
*Copyright (c) 2021-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: list.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

/*!
 * @header list.h
 *
 * This header file defines the functions present in the public interface of the
 * List Data Structure Facade selector. According to the user's definition, This
 * facade implementation establishes a common way to call a given operation on
 * different data structures implementations of the List Data Structures. The
 * supported implementations are:
 *
 * 1. ArrayList - To be implemented;
 *
 * 2. Doubly Linked List - To be implemented;
 *
 * 3. Linked List.
 *
 * All the aforementioned implementations are provided by the Generic Commons C
 * Library (GCCL).
 *
 */

#ifndef LIST_H_
#define LIST_H_

#include "commons/commons_utils.h"
#include "list_utils.h"

#ifdef __cplusplus
extern "C" {
#endif


/* ********** BEGIN - GENERIC LIST PUBLIC INTERFACE PROTOTYPES ********** */

/*!
 *
 * @function create_list
 *
 * @description Creates a new list of a given type, using a given data
 *              allocation size, and function for element comparison. Upon
 *              successful creation, it returns a list index, which is utilised
 *              for list manipulation. The comparator pointer represents the
 *              callback function, which is utilised internally for comparison
 *              purposes. The signature of this callback function receives two
 *              arguments, arg1 and arg2, which are the data elements to be
 *              compared. The comparator callback function returns a value of
 *              zero (0) if arg1 == arg2, i.e., they hold the same value; a
 *              value greater than zero (0) if arg1 > arg2, i.e., the first
 *              argument is greater than the second one; or a value lesser than
 *              zero (0) if arg1 < arg2, i.e., the first argument is lesser than
 *              the second one. The function returns a value greater than zero (1)
 *              representing the index of the list if the function executes successfully;
 *              -1 if there is a problem in the initialisation of the list service;
 *              -2 if the number of lists has exceeded its maximum value in a STATIC
 *              configuration; or -3 if there is a problem allocating memory for
 *              internal storage.
 *
 * @param type represents the type of the list in use by the user.
 *
 *  @param data_alloc_size represents the maximum size of the memory block
 * 		   required to store the data in each element of a list.
 *
 *  @param comparator represents the comparator function provided by the
 * 		   user (i.e., developer) to compare elements of the created list.
 *
 * @param free_data represents the function utilised to execute special purpose
 * 		  instructions to free a given element from memory.
 *
 * @param config_mask represents the order and meaning of each additional
 * 		  parameter passed to this function. Actually, there are three different
 * 		  types of parameter we can pass to the create_list() function when
 * 		  creating a new list:
 *
 * 		  s -> represents the maximum size the created list can hold;
 *
 * 		  l -> represents the locked status associated with the maximum size
 * 		  	   of the created list.
 *
 * 		  u -> represents the unlocked status associated with the maximum size
 * 		  	   of the created list.
 *
 * @result returns a value greater than zero (1) representing the index of the
 *         list if the function executes successfully; -1 if there is a problem
 *         in the initialisation of the list service; -2 if the number of lists
 *         has exceeded its maximum value in a STATIC configuration; or -3 if
 *         there is a problem allocating memory for internal storage.
 */
extern int16_t create_list(gccl_list_type_t type, size_t data_alloc_size,
						   int8_t (*const comparator) (const void * const arg1,
								                       const void * const arg2),
						   void (*const free_data) (const void * const data),
						   const char * const config_mask, ...);

/*!
 *
 * @function vcreate_list
 *
 * @description Variation of the create_list() function, replacing the variable
 *              argument declaration by the va_list data type, which holds the
 *              list of arguments initialised by the va_start() function.
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param data_alloc_size represents the size of each data element being
 *        allocated within the new list.
 *
 * @param comparator represents the comparator function provided by the
 * 		  user (i.e., developer) to compare elements of the created list.
 *
 * @param free_data represents the function utilised to execute special purpose
 * 		  instructions to free a given element from memory.
 *
 * @param arg_list represents a list of variable arguments, which may be passed
 *        to the function in order to enable the creation of a specific type of
 *        list.
 *
 * @result returns a value greater than zero (0) representing the index of the
 *         list if the function executes successfully; a value of -1, otherwise.
 */
extern int16_t vcreate_list(gccl_list_type_t type, size_t data_alloc_size,
                            int8_t (*const comparator) (const void * const arg1,
                                                        const void * const arg2),
							void (*const free_data) (const void * const data),
							const char * const config_mask, va_list arg_list);

/*!
 * @function delete_element_list
 *
 * @description Deletes an element from the target list, which is represented by
 *              an index [list_index], according to a given delete mode
 *              (DEL_DATA_INDEX,DEL_FIRST,DEL_LAST,DEL_AT_POS). The removal
 *              policy is the following:
 *
 *              DEL_DATA_INDEX: delete the element pointed by data, in an index
 *              that has been found within the target list, which is represented
 *              by an index [list_index];
 *
 *              DEL_FIRST: delete the first element of the target list, which is
 *              represented by an index [list_index], where such element matches
 *              the given element pointed by data;
 *
 *              DEL_LAST: delete the last element of the target list, which is
 *              represented by an index [list_index], where such element matches
 *              the given element pointed by data;
 *
 *              DEL_AT_POS: delete the element of the target list, which is
 *              represented by an index [list_index], where such a element is in
 *              the given position pointed by data.
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param data represents a pointer to the data content, or a list position,
 *        that want to be removed from the list represented by a given index
 *        [list_index].
 *
 * @param delete_mode represents the delete mode utilised to perform the delete
 *        operation. It can assume one of the following values: (DEL_DATA_INDEX,
 *        DEL_FIRST,DEL_LAST,DEL_AT_POS).
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @result returns a value representing the current size of the list being
 *         manipulated if the internal delete operation is executed successfully;
 * 	 	   a value of -1, otherwise.
 */
extern int delete_element_list(gccl_list_type_t type, const void * const data,
							   delete_mode_t delete_mode, int16_t list_index);

/*!
 * @function destroy_ALL_lists
 *
 * @description Destroys all lists of a given type, which are managed by the
 *              Generic Commons C Library (GCCL).
 *
 * @param type represents the type of the list the user wants to destroy.
 *
 * @return returns true if success; false, otherwise.
 */
extern bool destroy_ALL_lists(gccl_list_type_t type);

/*!
 * @function destroy_array_copy_list
 *
 * @description Destroys the memory block pointed by array_list_ptr.
 *
 * @param array_list_ptr represents a pointer to the array that wants to be
 *        destroyed.
 */
extern void destroy_array_copy_list(const void * array_list_ptr);


/*!
 * @function destroy_list
 *
 * @description Destroys the target list represented by an index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns true if the list represented by a given index[list_index] is
 * 		   destroyed successfully; false, otherwise.
 */
extern bool destroy_list(gccl_list_type_t type, int16_t list_index);

/*!
 * @function exists_element_in_list
 *
 * @description Searches for a given element pointed by data in a target list,
 *              which is represented by an index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param data represents the data a user (i.e., developer) wants to find on a
 * list indicated by a given index [list_index].
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns true if the given data is found; false, otherwise.
 */
extern bool exists_element_in_list(gccl_list_type_t type, const void * const data,
								   int16_t list_index);

/*!
 * @function get_and_delete_element_list
 *
 * @description Returns and deletes an element from the target list, which is
 *              represented by an index [list_index], according to a given
 *              delete mode (DEL_DATA_INDEX, DEL_FIRST, DEL_LAST, DEL_AT_POS).
 *              The removal policy is the following:
 *
 *              DEL_DATA_INDEX: delete the element pointed by data, in a
 *              position that has been found within the target list, which is
 *              represented by an index [list_index];
 *
 *              DEL_FIRST: delete the first element of the target list, which is
 *              represented by an index [list_index], where such element matches
 *              the given element pointed by data;
 *
 *              DEL_LAST: delete the last element of the target list, which is
 *              represented by an index [list_index], where such element matches
 *              the given element pointed by data;
 *
 *              DEL_AT_POS: delete the element of the target list, which is
 *              represented by an index [list_index], where such a element is in
 *              the given position pointed by data.
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param data represents the data a user wants to find and delete from a list
 * indicated by a given index [list_index].
 *
 * @param delete_mode represents the delete mode utilised to perform the delete
 *        operation. It can assume one of the following values: (DEL_DATA_INDEX,
 *        DEL_FIRST,DEL_LAST,DEL_AT_POS).
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the deleted element if success; NULL, otherwise.
 */
extern void * get_and_delete_element_list(gccl_list_type_t type, const void * const data,
                                          delete_mode_t delete_mode,
                                          int16_t list_index);

/*!
 * @function get_and_delete_head_list
 *
 * @description Returns and deletes the head of the target list, which is
 *              represented by an index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the head of the target list if success; NULL, otherwise.
 */
extern void * get_and_delete_head_list(gccl_list_type_t type, int16_t list_index);


/*!
 * @function get_and_delete_tail_list
 *
 * @description Returns and deletes the tail of the target list, which is
 *              represented by an index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the tail of the target list if success; NULL, otherwise.
 */
extern void * get_and_delete_tail_list(gccl_list_type_t type, int16_t list_index);


/*!
 * @function get_array_copy_list
 *
 * @description Returns an array of elements as a copy of the target list, which
 *              is represented by an index [list_index]. The callback function
 *              element_copy is responsible to create a copy of each element,
 *              storing such a copy into a given position of the new array. The
 *              subsequent calls to the function are performed (internally) by
 *              get_array_copy_list, which is responsible to provide all
 *              arguments at the moment of calling the element_copy callback
 *              function: (1) the address of the new array ("to" argument); (2)
 *              the element to be copied ("data" argument); and (3) the position
 *              that such element will be stored on the new array.
 *
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @param element_copy represents the user-provided callback function responsible
 * 		  to perform the copy of an element ("data" argument) from a list represented
 *	 	  by a given index (list_index), to a given position of a given new array
 *	 	  ("to" argument).
 *
 * @return returns the new allocated array with all elements of a list represented
 * 		   by a given index [list_index].
 */
extern void * get_array_copy_list(gccl_list_type_t type, int16_t list_index,
								  void (*const element_copy)
								  (const void * const restrict to,
								   const void * const restrict data,
								   int position));


/*!
 * @function get_element_list
 *
 * @description Returns a given element pointed by data, if and only if, it is a
 *              member of the target list, which is represented by an index
 *              [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param data represents the data a user wants to find within a list
 * indicated by a given index [list_index].
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the desired element if success; NULL, otherwise.
 */
extern void * get_element_list(gccl_list_type_t type, const void * const data,
							   int16_t list_index);

/*!
 * @function get_element_at_position_list
 *
 * @description Returns the element in a given position of the target list,
 *              which is represented by an index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param position represents the position a user wants to find a element within
 * a list indicated by a given index [list_index].
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return the element of a given position if success; NULL, otherwise.
 */
extern void * get_element_at_position_list(gccl_list_type_t type, int position,
										   int16_t list_index);


/*!
 * @function get_list_head
 *
 * @description Returns the head of the target list, which is represented by an
 *              index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the head of the target list if success; NULL, otherwise.
 */
extern void * get_list_head(gccl_list_type_t type, int16_t list_index);


/*!
 * @function get_size_list
 *
 * @description Returns the size of the target list, which is represented by an
 *              index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the size of the target list if success; -1, otherwise.
 */
extern int get_size_list(gccl_list_type_t type, int16_t list_index);

/*!
 * @function get_tail_list
 *
 * @description Returns the tail of the target list, which is represented by an
 *              index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the tail of the target list if success; NULL, otherwise.
 */
extern void * get_tail_list(gccl_list_type_t type, int16_t list_index);

/*!
 * @function initialise_service_list
 *
 * @description Initialises the list service of the given type, using a given
 *              allocation rule (DYNAMIC,STATIC), and the initial number of
 *              lists the service is ready to manage.
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param rule represents the allocation rule utilised to initialise the list
 *        service. It can assume two possible values: DYNAMIC, for dynamic memory
 *        allocation; or STATIC, for a fixed size of lists to be managed by the
 *        list service.
 *
 * @param initial_number_of_lists the initial number of lists the list service
 *        is ready to manage.
 *
 * @return returns a unique identifier if the service has been initialised successfully;
 * 		   minus one (-1), otherwise.
 */
extern long initialise_service_list(gccl_list_type_t type,
									gccl_alloc_t rule,
									int16_t initial_number_of_lists);

/*!
 * @function insert_element_list
 *
 * @discussion Inserts an element in the target list, which is represented by an
 *             index [list_index], according to a given insert mode
 *             (INS_ASC_ORDER,INS_LAST,INS_DESC_ORDER,INS_FIRST). The insertion
 *             policy is the following:
 *
 *             INS_ASC_ORDER: inserts the element pointed by data in ascending
 *             order into the target list, which is represented by an index
 *             [list_index];
 *
 *             INS_LAST: inserts the element pointed by data in the tail of the
 *             target list, which is represented by an index [list_index];
 *
 *             INS_DESC_ORDER: inserts the element pointed by data in descending
 *             order into the target list, which is represented by an index
 *             [list_index];
 *
 *             INS_FIRST: inserts the element pointed by data in the head of the
 *             target list, which is represented by an index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param data represents the data a user wants to insert to a list indicated by
 *        a given index [list_index].
 *
 * @param insert_mode represents the insert mode utilised to perform the insert
 *        operation. It can assume one of the following values: (INS_ASC_ORDER,
 *        INS_LAST,INS_DESC_ORDER,INS_FIRST).
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns the new size of the target list if success; -1, otherwise.
 */
extern int insert_element_list(gccl_list_type_t type, const void * const data,
							   insert_mode_t insert_mode, int16_t list_index);

/*!
 * @function isEmpty_list
 *
 * @discussion Checks if the target list is empty. Such a list is represented by
 *             a given index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns true if the target list is empty; false, otherwise.
 */
extern bool isEmpty_list(gccl_list_type_t type, int16_t list_index);


/*!
 * @function isFull_list
 *
 * @discussion Checks if the target list is full. Such a list is represented by
 *             a given index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns true if the target list is full; false, otherwise.
 */
extern bool isFull_list(gccl_list_type_t type, int16_t list_index);


/*!
 * @function reset_list
 *
 * @discussion Resets the target list, removing all elements. Such a list is
 *             represented by a given index [list_index].
 *
 * @param type represents the type of the list in use by the user.
 *
 * @param list_index represents an index for the list that gets manipulated by
 *        the function.
 *
 * @return returns true if the target list gets reseted; false, otherwise.
 */
extern bool reset_list(gccl_list_type_t type, int16_t list_index);

/*!
 *
 * @function stop_all_services_list
 *
 * @discussion Stop all list services initialised by this common public list
 * 			   interface. This function returns true if all list services are
 * 			   stopped successfully; false, otherwise.
 *
 * @result returns true if all list services are stopped successfully;
 * 		   false, otherwise.
 *
 */

extern bool stop_all_services_list(void);

extern bool stop_service_list_of_type(gccl_list_type_t gccl_list_type);

extern bool stop_service_list(long reference_id);

/* ********** END - GENERIC LIST PUBLIC INTERFACE PROTOTYPES ********** */

#ifdef __cplusplus
}
#endif


#endif /* LIST_H_ */
