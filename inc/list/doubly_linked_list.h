/*
*****************************************************************************************
*
*Copyright (c) 2022-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: doubly_linked_list.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

/*!
 * @header doubly_linked_list.h
 *
 * This header file defines the functions present in the implementation of the
 * Doubly Linked List data structure provided by the Generic Commons C Library
 * (GCCL).
 *
 */

#ifndef DOUBLY_LINKED_LIST_H_
#define DOUBLY_LINKED_LIST_H_

#include "linked_list.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ********** BEGIN - DOUBLY LINKED LIST PUBLIC INTERFACE PROTOTYPES ********** */

/*!
 *
 * Creates a new doubly linked list using a given data allocation size, and function for
 * element comparison. Upon successful creation, it returns a doubly linked list index,
 * which is utilised for list manipulation.
 */
extern int16_t create_doubly_LL(size_t data_alloc_size,
						 int8_t (*const comparator) (const void * const arg1,
								                     const void * const arg2),
						 void (*const free_data) (const void * const data),
						 const char * const config_mask, ...);

/*!
 *
 * Deletes an element from the target doubly linked list, which is represented by an
 * index [index_LL], according to a given delete mode (DEL_DATA_INDEX, DEL_FIRST,
 * DEL_LAST, DEL_AT_POS). The removal policy is the following:
 *
 * DEL_DATA_INDEX: delete the element pointed by data, in an index
 * that has been found within the target doubly linked list, which is represented by an
 * index [index_LL];
 *
 * DEL_FIRST: delete the first element of the target doubly linked list, which is
 * represented by an index [index_LL], where such element matches the given
 * element pointed by data;
 *
 * DEL_LAST: delete the last element of the target doubly linked list, which is
 * represented by an index [index_LL], where such element matches the given
 * element pointed by data;
 *
 * DEL_AT_POS: delete the element of the target doubly linked list, which is
 * represented by an index [index_LL], where such a element is in the given
 * position pointed by data.
 */
extern int delete_element_doubly_LL(const void * const data, int8_t delete_mode,
							        int16_t index_LL);


/*!
 *
 * Destroys the target doubly linked list represented by an index [index_LL].
 */
extern bool destroy_doubly_LL(int16_t index_LL);


/*!
 *
 * Destroys the memory block pointed by array_list_ptr.
 */
extern void destroy_array_copy__doubly_LL(const void * array_list_ptr);

/*!
 *
 * Destroys all doubly linked lists managed by the Generic_Commons_C_Library.
 */
extern bool destroy_ALL_doubly_LL();


/*!
 *
 * Searches for a given element pointed by data in a target doubly linked list, which is
 * represented by an index [index_LL].
 */
extern bool exists_element_in_doubly_LL(const void * const data, int16_t index_LL);


/*!
 *
 * Returns an array of elements as a copy of the target doubly linked list, which is
 * represented by an index [index_LL].
 */
extern void * get_array_copy_doubly_LL(int16_t index_LL, void (*const element_copy)
							    							   (const void * const restrict to,
							    							    const void * const restrict from, int position));


/*!
 *
 * Returns and deletes an element from the target doubly linked list, which is
 * represented by an index [index_LL], according to a given delete mode
 * (DEL_DATA_INDEX, DEL_FIRST, DEL_LAST, DEL_AT_POS). The removal policy
 * is the following:
 *
 * DEL_DATA_INDEX: delete the element pointed by data, in a position
 * that has been found within the target doubly linked list, which is represented by
 * an index [index_LL];
 *
 * DEL_FIRST: delete the first element of the target doubly linked list, which is
 * represented by an index [index_LL], where such element
 * matches the given element pointed by data;
 *
 * DEL_LAST: delete the last element of the target doubly linked list, which is
 * represented by an index [index_LL], where such element matches the given
 * element pointed by data;
 *
 * DEL_AT_POS: delete the element of the target doubly linked list, which is represented
 * by an index [index_LL], where such a element is in the given position
 * pointed by data.
 */
extern void * get_and_delete_element_doubly_LL(const void * const data,
											   int8_t delete_mode, int16_t index_LL);

/*!
 *
 * Returns and deletes the head of the target doubly linked list, which is represented
 * by an index [index_LL].
 */
extern void * get_and_delete_head_doubly_LL(int16_t index_LL);


/*!
 *
 * Returns and deletes the tail of the target doubly linked list, which is represented
 * by an index [index_LL].
 */
extern void * get_and_delete_tail_doubly_LL(int16_t index_LL);


/*!
 *
 * Returns a given element pointed by data, if and only if, it is a member of
 * the target doubly linked list, which is represented by an index [index_LL].
 */
extern void * get_element_doubly_LL(const void * const data, int16_t index_LL);

/*!
 *
 * Returns the element in a given position of the target doubly linked list,
 * which is represented by an index [index_LL].
 */
extern void * get_element_at_position_doubly_LL(int position, int16_t index_LL);


/*!
 *
 * Returns the head of the target doubly linked list, which is represented
 * by an index [index_LL].
 */
extern void * get_head__doubly_LL(int16_t index_LL);


/*!
 *
 * Returns the size of the target doubly linked list, which is represented
 * by an index [index_LL].
 */
extern int get_size_doubly_LL(int16_t index_LL);

/*!
 *
 * Returns the tail of the target doubly linked list, which is represented
 * by an index [index_LL].
 */
extern void * get_tail_doubly_LL(int16_t index_LL);


/*!
 *
 * Initialises the doubly linked list service, using a given allocation rule
 * [DYNAMIC/STATIC], and the initial number of doubly linked lists the service
 * is ready to manage.
 */
extern long initialise_doubly_LL_service(gccl_alloc_t rule,
                                         int16_t initial_number_of_doubly_LLs);


/*!
 *
 * Inserts an element in the target doubly linked list, which is represented by an
 * index [index_LL], according to a given insert mode (INS_ASC_ORDER, INS_LAST,
 * INS_DESC_ORDER, INS_FIRST). The insertion policy is the following:
 *
 * INS_ASC_ORDER: inserts the element pointed by data in ascending order into
 * the target doubly linked list, which is represented by an index [index_LL];
 *
 * INS_LAST: inserts the element pointed by data in the tail of the target
 * doubly linked list, which is represented by an index [index_LL];
 *
 * INS_DESC_ORDER: inserts the element pointed by data in descending order into
 * the target doubly linked list, which is represented by an index [index_LL];
 *
 * INS_FIRST: inserts the element pointed by data in the head of the target
 * doubly linked list, which is represented by an index [index_LL].
 */
extern int insert_element_doubly_LL(const void* const data, int8_t insert_mode,
							        int16_t index_LL);

/*!
 *
 * Checks if the target doubly linked list, which is represented
 * by a given index [index_LL], is empty.
 */
extern bool isEmpty_doubly_LL(int16_t index_LL);


/*!
 *
 * Checks if the target doubly linked list, which is represented
 * by a given index [index_LL], is full.
 */
extern bool isFull_doubly_LL(int16_t index_LL);


/*!
 *
 * Resets the target doubly linked list, which is represented
 * by a given index [index_LL], removing all elements.
 */
extern bool reset_doubly_LL(int16_t index_LL);

/*!
 *
 * @function stop_doubly_LL_service
 *
 * @description Stops the doubly linked list service, removing all managed
 * 				doubly linked lists from the Generic Commons C Library (GCCL)
 * 				manager. Stopping the doubly linked list service doesn't stop
 * 				the GCCL manager. The GCCL manager must be stopped by invoking
 * 				the function stop_gccl_manager(), instead.
 *
 */

extern bool stop_doubly_LL_service(void);

extern bool stop_doubly_LL_service_of_reference_id(long reference_id);

/* ********** END - DOUBLY LINKED LIST PUBLIC INTERFACE PROTOTYPES ********** */

#ifdef __cplusplus
}
#endif

#endif /* DOUBLY_LINKED_LIST_H_ */
