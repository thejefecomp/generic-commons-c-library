/*
*****************************************************************************************
*
*Copyright (c) 2013-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: linked_list.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

/*!
 * @header linked_list.h
 *
 * This header file defines the functions present in the implementation of the Linked List
 * Data Structure, which is provided by the Generic Commons C Library (GCCL).
 *
 */

#ifndef LINKED_LIST_H_
#define LINKED_LIST_H_

#include "commons/commons_utils.h"
#include "list_utils.h"

#ifdef __cplusplus
extern "C" {
#endif


/* ********** BEGIN - LINKED LIST INTERFACE ********** */

/*!
 * @function create_LL
 *
 * @description Creates a new linked list using a given data allocation size and
 * 				function for element comparison. Upon successful creation, it returns
 * 				a linked list index, which is utilised for list manipulation.
 *
 * @param data_alloc_size represents the maximum size of the memory block
 * 		  required to store the data in each element of a linked list.
 *
 * @param comparator represents the comparator function provided by the
 * 		  user (i.e., developer) to compare elements of the created linked list.
 *
 * @param free_data represents the function utilised to execute special purpose
 * 		  instructions to free a given element from memory.
 *
 * @param config_mask represents the order and meaning of each additional
 * 		  parameter passed to this function. Actually, there are three different
 * 		  types of parameter we can pass to the create_LL() function when
 * 		  creating a new linked list:
 *
 * 		  s -> representing the maximum size the created linked list can hold;
 *
 * 		  l -> representing the locked status associated with the maximum size
 * 		  	   of the created linked list.
 *
 * 		  u -> representing the unlocked status associated with the maximum size
 * 		  	   of the created linked list.
 *
 * @result returns a value greater than 1 (i.e. > 1), representing the index of
 *  	   the linked list, if the function executes successfully; -1 if there is
 *  	   a problem in the initialisation of the Generic Commons C Library (GCCL)
 *  	   manager; -2 if the number of linked lists carrying a given data structure
 *  	   type has exceeded its maximum value in a STATIC configuration; or -3 if
 *         there is a problem allocating memory for internal storage.
 */
extern int16_t create_LL(size_t data_alloc_size,
						 int8_t (*const comparator) (const void * const arg1,
								                     const void * const arg2),
						 void (*const free_data) (const void * const data),
						 const char * const config_mask, ...);

/*!
 * @function vcreate_LL
 *
 * @description Variation of the create_LL() function, replacing the variable argument
 * declaration by the va_list data type, which holds the list of arguments initialised
 * by the va_start() function.
 *
 * @param data_alloc_size represents the maximum size of the memory block
 * 		  required to store the data in each element of a linked list.
 *
 * @param comparator represents the comparator function provided by the
 * 		  user (i.e., developer) to compare elements of the created linked list.
 *
 * @param free_data represents the function utilised to execute special purpose
 * 		  instructions to free a given element from memory.
 *
 * @param config_mask represents the order and meaning of each additional
 * 		  parameter passed to this function. Actually, there are three different
 * 		  types of parameter we can pass to the create_LL() function when
 * 		  creating a new linked list:
 *
 * 		  s -> representing the maximum size the created linked list can hold;
 *
 * 		  l -> representing the locked status associated with the maximum size
 * 		  	   of the created linked list.
 *
 * 		  u -> representing the unlocked status associated with the maximum size
 * 		  	   of the created linked list.
 *
 * @param arg_list allows accessing the list of variable arguments indicated by
 * 	      the config_mask.
 *
 * @result returns a value greater than 1 (i.e. > 1), representing the index of
 *  	   the linked list, if the function executes successfully; -1 if there is
 *  	   a problem in the initialisation of the Generic Commons C Library (GCCL)
 *  	   manager; -2 if the number of linked lists carrying a given data structure
 *  	   type has exceeded its maximum value in a STATIC configuration; or -3 if
 *         there is a problem allocating memory for internal storage.
 */
extern int16_t vcreate_LL(size_t data_alloc_size,
                   	      int8_t (*const comparator) (const void * const arg1,
                                                	   const void * const arg2),
						  void (*const free_data) (const void * const data),
						  const char *const config_mask, va_list arg_list);
/*!
 *
 * @function delete_element_LL
 *
 * @description Deletes an element from the target linked list, which is represented
 * 				by an index [index_LL], according to a given delete mode (DEL_DATA_INDEX,
 * 				DEL_FIRST, DEL_LAST, DEL_AT_POS). The removal policy is the following:
 *
 * 				DEL_DATA_INDEX: delete the element pointed by data, in an index
 * 				that has been found within the target linked list, which is represented
 * 				by an index [index_LL];
 *
 * 				DEL_FIRST: delete the first element of the target linked list, which
 * 				is represented by an index [index_LL], where such element matches
 * 				the given element pointed by data;
 *
 * 				DEL_LAST: delete the last element of the target linked list, which
 * 				is represented by an index [index_LL], where such element matches
 * 				the given element pointed by data;
 *
 * 				DEL_AT_POS: delete the element of the target linked list, which is
 * 				represented by an index [index_LL], where such a element is in the
 * 				given position pointed by data.
 *
 * @param data represents a pointer to the data content, or a list position,
 *        that want to be removed from the list represented by a given index
 *        [index_LL].
 *
 *  @param delete_mode represents the delete mode utilised to perform the delete
 *         operation. It can assume one of the following values: (DEL_DATA_INDEX,
 *         DEL_FIRST,DEL_LAST,DEL_AT_POS).
 *
 *  @param index_LL represents the index of the linked list, in which the
 * 		   operation will be performed.
 *
 * 	 @result returns a value representing the current size of the linked list
 * 	 		 being manipulated if the internal delete operation is executed
 * 	 		 successfully; a value of -1, otherwise.
 */
extern int delete_element_LL(const void * const data, delete_mode_t delete_mode,
							 int16_t index_LL);


/*!
 *
 * @function destroy_LL
 *
 * @description Destroys the target linked list represented by an index [index_LL].
 *
 * @param index_LL represents the index of the linked list, in which the
 * 		  operation will be performed.
 *
 * @result returns true if the linked list represented by a given index [index_LL]
 * 		   is destroyed successfully; false, otherwise.
 */
extern bool destroy_LL(int16_t index_LL);


/*!
 *
 * @function destroy_array_copy_LL
 *
 * @description Destroys the memory block pointed by array_list_ptr.
 *
 * @param array_list_ptr represents a pointer to the array that wants to be
 *        destroyed.
 */
extern void destroy_array_copy_LL(const void * array_list_ptr);

/*!
 *
 * @function destroy_ALL_LL
 *
 * @description Destroys all linked lists managed by the Generic Commons C Library
 * 				(GCCL).
 *
 * @result returns true if all linked lists carrying any type of list are destroyed;
 * 		   false, otherwise.
 */
extern bool destroy_ALL_LL(void);

/*!
 *
 * @function destroy_ALL_typed_LL
 *
 * @description Destroys all lists of a given type managed by the Generic Commons
 * C Library (GCCL).
 *
 * @result returns true if all linked lists carrying a given type of list are destroyed;
 * 		   false, otherwise.
 */
extern bool destroy_ALL_typed_LL(gccl_list_type_t list_type);


/*!
 *
 * @function exists_element_in_LL
 *
 * @description Searches for a given element pointed by data in a target linked list,
 * 				which is represented by an index [index_LL].
 *
 * @param data represents the data a user (i.e., developer) wants to find on a
 *  	  linked list indicated by a given index [index_LL].
 *
 * @result returns true if the given data is found; false, otherwise.
 *
 */
extern bool exists_element_in_LL(const void * const data, int16_t index_LL);


/*!
 *
 * @function get_array_copy_LL
 *
 * @description Returns an array of elements as a copy of the target list, which
 *              is represented by an index [index_LL]. The callback function
 *              element_copy is responsible to create a copy of each element,
 *              storing such a copy into a given position of the new array. The
 *              subsequent calls to the function are performed (internally) by
 *              get_array_copy_list, which is responsible to provide all
 *              arguments at the moment of calling the element_copy callback
 *              function: (1) the address of the new array ("to" argument); (2)
 *              the element to be copied ("data" argument); and (3) the position
 *              that such element will be stored on the new array.
 *
 * @param index_LL represents an index for the list that gets manipulated by
 *        the function.
 *
 * @param element_copy represents the user-provided callback function responsible
 * 		  to perform the copy of an element ("data" argument) from a list represented
 *	 	  by a given index (list_index), to a given position of a given new array
 *	 	  ("to" argument).
 *
 * @return returns the new allocated array with all elements of a list represented
 * 		   by a given index [index_LL].
 */
extern void * get_array_copy_LL(int16_t index_LL, void (*const element_copy)
							    (const void * const restrict to,
							     const void * const restrict data,
								 int position));

/*!
 *
 * @function get_and_delete_element_LL
 *
 *
 * @description Returns and deletes an element from the target linked list,
 * 				which is represented by an index [index_LL], according to a given
 * 				delete mode (DEL_DATA_INDEX, DEL_FIRST, DEL_LAST, DEL_AT_POS).
 * 				The removal policy is the following:
 *
 * 				DEL_DATA_INDEX: delete the element pointed by data, in a position
 * 				that has been found within the target linked list, which is
 * 				represented by an index [index_LL];
 *
 * 				DEL_FIRST: delete the first element of the target linked list,
 * 				which is represented by an index [index_LL], where such element
 * 				matches the given element pointed by data;
 *
 * 				DEL_LAST: delete the last element of the target linked list,
 * 				which is represented by an index [index_LL], where such element
 * 				matches the given element pointed by data;
 *
 * 				DEL_AT_POS: delete the element of the target linked list, which is
 * 				represented by an index [index_LL], where such element is in the
 * 				given position pointed by data.
 *
 * @param data represents the data a user (i.e., developer) wants to find and
 *  	  delete from a list indicated by a given index [index_LL].
 *
 * @param delete_mode represents the delete mode utilised to perform the delete
 *        operation. It can assume one of the following values: (DEL_DATA_INDEX,
 *        DEL_FIRST,DEL_LAST,DEL_AT_POS).
 *
 * @param index_LL represents an index for the list that gets manipulated by
 *        the function.
 *
 * @result returns the deleted element if successful; NULL, otherwise.
 */
extern void * get_and_delete_element_LL(const void * const data,
										delete_mode_t delete_mode,
										int16_t index_LL);

/*!
 * @function get_and_delete_head_LL
 *
 * @description Returns and deletes the head of the target linked list,
 * 				which is represented by an index [index_LL].
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 		  function.
 *
 * @result returns the value of the head, deleting the element from the linked list.
 * 		   The function returns NULL, otherwise.
 *
 */
extern void * get_and_delete_head_LL(int16_t index_LL);


/*!
 *
 * @function get_and_delete_tail_LL
 *
 * @description Returns and deletes the tail of the target linked list, which is
 * 				represented by a given index [index_LL].
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 		  function.
 *
 * @result returns the value of the tail, deleting the element from the linked list.
 * 		   The function returns NULL, otherwise.
 */
extern void * get_and_delete_tail_LL(int16_t index_LL);


/*!
 *
 * @function get_element_LL
 *
 * @description Returns a given element pointed by data, if and only if, it is a
 * 				member of the target linked list, which is represented by an index
 * 				[index_LL].
 *
 * @param data represents the data a user (i.e., developer) wants to find and
 *  	  delete from a linked list represented by a given index [index_LL].
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 		  function.
 *
 * @result if the function finds the element, it returns its value. The function
 * 		   returns NULL, otherwise.
 */
extern void * get_element_LL(const void * const data, int16_t index_LL);

/*!
 *
 * @function get_element_at_position_LL
 *
 * @description Returns the element on a given position of the target linked list,
 * which is represented by a given index [index_LL].
 *
 * @param position indicates the position a user (i.e., developer) requests the
 * 		  value of.
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @result returns the value of a given position of the target linked list; NULL,
 * 		   otherwise.
 */
extern void * get_element_at_position_LL(int position, int16_t index_LL);


/*!
 *
 * @function get_head_LL
 *
 * @description Returns the head of the target linked list, which is represented
 * by a given index [index_LL].
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @result returns the value of the head of the target linked list; NULL, otherwise.
 */
extern void * get_head_LL(int16_t index_LL);


/*
 *
 * @function get_max_size_LL
 *
 * @description returns the maximum size of a linked list represented by a given
 * 				index [index_LL].
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @result if a given index is valid, returns an integer representing the maximum
 * 		   size the target linked list can hold; -2, otherwise.
 */
extern int get_max_size_LL(int16_t index_LL);

/*!
 *
 * @function get_size_LL
 *
 * @description Returns the size of the target linked list, which is represented
 * by a given index [index_LL].
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
  * @result if a given index is valid, returns an integer representing the current
  * 		size the target linked list holds; -1, otherwise.
 */
extern int get_size_LL(int16_t index_LL);

/*!
 *
 * @function get_tail_LL
 *
 * @description Returns the tail of the target linked list, which is represented
 * 				by a given index [index_LL].
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @result returns the value of the tail of the target linked list; NULL, otherwise.
 */
extern void * get_tail_LL(int16_t index_LL);


/*!
 *
 * @function initialise_LL_service
 *
 * @description Initialises the linked list service, using a given allocation rule
 * 				[DYNAMIC/STATIC], and the initial number of linked lists the service
 * 				is ready to manage. The function returns a unique identifier if
 * 				the service is initialised successfully; -1, otherwise.
 *
 * @param rule indicates the allocation rule [DYNAMIC/STATIC] utilised by the service.
 * 		  DYNAMIC means the number of managed linked list can grows indefinitely,
 * 		  respecting restrictions of the software, operating system, and hardware
 * 		  the service is subjected to. On the other hand, STATIC means a maximum
 * 		  number of linked lists that can be managed by the service, which is
 * 		  indicated by the parameter initial_number_of_LLs.
 *
 * @param initial_number_of_LLs represents the initial number of linked lists that
 * 		  can be managed by the service.
 *
 * @result returns a unique identifier if the service is initialised successfully;
 * 		   minus one (-1), otherwise.
 */
extern long initialise_LL_service(gccl_alloc_t rule, int16_t initial_number_of_LLs);


/*!
 *
 * @function insert_element_LL
 *
 * @description Inserts an element on the target linked list, which is represented
 * 				by a given index [index_LL], according to a given insert mode
 * 				(INS_ASC_ORDER, INS_LAST, INS_DESC_ORDER, INS_FIRST).
 * 				The insertion policy is the following:
 *
 * 				INS_ASC_ORDER: inserts the element pointed by data in ascending
 * 				order into the target linked list, which is represented by a given
 * 				index [index_LL];
 *
 * 				INS_LAST: inserts the element pointed by data in the tail of the
 * 				target linked list, which is represented by a given index [index_LL];
 *
 * 				INS_DESC_ORDER: inserts the element pointed by data in descending
 * 				order into the target linked list, which is represented by an given
 * 				index [index_LL];
 *
 * 				INS_FIRST: inserts the element pointed by data in the head of the
 * 				target linked list, which is represented by a given index [index_LL].
 *
 * @result returns the current size of the target linked list after a successful
 * 		   insertion; -1, otherwise.
 */
extern int insert_element_LL(const void* const data, insert_mode_t insert_mode,
							 int16_t index_LL);

/*!
 *
 * @function isEmpty_LL
 *
 * @description Checks if the target linked list, which is represented
 * by a given index [index_LL], is empty. The function returns true if the target
 * linked list has no elements; false, otherwise.
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @result returns true if the target linked list has no elements; false, otherwise.
 */
extern bool isEmpty_LL(int16_t index_LL);


/*!
 *
 * @function isFull_LL
 *
 * @description Checks if the target linked list, which is represented
 * by a given index [index_LL], is full. The function returns true if the number
 * target of elements of the target linked list is equal to its maximum size;
 * false, otherwise.
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @result returns true if the number target of elements of the target linked list
 * 		   is equal to its maximum size; false, otherwise..
 */
extern bool isFull_LL(int16_t index_LL);

/*!
 *
 * @function iterate_LL
 *
 * @description Allows applying a function provided by the user (i.e., developer)
 * 				to iterate through data stored on the target linked list, which is
 * 				represented by a given index [index_LL]. The applied function has
 * 				one parameter (i.e., param) that can be given by the user at
 * 				the moment the provided function is applied. The given parameter
 * 				"param" is provided through a parameter of the iterate_LL() function,
 * 				which has the same name. The applied function also has a parameter
 * 				that enables the user to return a final status of the entire execution
 * 				("status" parameter), according to any rule/restriction defined by
 * 				the user. For each position that the function is applied
 * 				(from start_position to end_position), the user has to return an
 * 				indication (true/false), which is utilised by the iterate_LL()
 * 				function to stop (if required) the loop that is
 * 				iterating through the stored data. This function returns true if
 * 				the applied function iterates through all the data; false, otherwise.
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @param applied_function represents the function that will be applied on data.
 * 		  The signature of the applied_function has three parameters:
 *
 * 		  * param - representing a parameter provided by the user when the
 * 		  	applied_function gets executed.
 *
 * 		  * status - allowing the user to return a final status derived from all
 * 		    executions of the applied_function. It can hold any data type and any
 * 		    kind of information the user would like to return as a final status of
 * 		    their choice.
 *
 * 		  * data - represents the data that the applied_function will perform its
 * 		    operations on.
 *
 * @result returns true if the applied function iterates through all the data;
 * 		   false, otherwise.
 */

extern bool iterate_LL(int16_t index_LL, bool (* const applied_function)
												       (const void * const param,
												        const void * const status,
														const void * const data),
														int start_position,
														int end_position,
														bool stop_at_first_error,
														void * const param,
														void * const status);

/*!
 *
 * @function set_max_size_LL
 *
 *	@description Sets the new maximum size [new_max_size] of the target linked list,
 *				 which is represented by a given index [index_LL]. If the actual
 *				 size of the target list is greater than the new maximum size, i.e.,
 *				 size > new_max_size, the remaining elements above the new maximum
 *				 size are removed. However, if the max_size is locked, both max_size
 *				 and the linked list itself remain untouched.
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @param new_max_size represents the new maximum size to be set on the target
 * 		  linked list.
 *
 * @result returns the new maximum size if the function is executed successfully;
 * 		   -1 if the maximum size of the target linked list is locked; -2, otherwise.
 */
extern int set_max_size_LL(int16_t index_LL, int new_max_size);

/*!
 *
 * @function reset_LL
 *
 * @description Resets the target linked list, which is represented by a given
 * 				index [index_LL], removing all current elements. The function returns
 * 				true if executed successfully; false, otherwise.
 *
 * @param index_LL represents an index for the linked list manipulated by the
 * 	 	  function.
 *
 * @result returns true if executed successfully; false, otherwise.
 */
extern bool reset_LL(int16_t index_LL);

/*!
 * @function stop_LL_service
 *
 * @description Stops the linked list service, removing all managed lists (i.e,
 * 				Array Lists, Doubly Linked Lists, Linked Lists) from the Generic
 * 				Commons C Library (GCCL) manager. The function returns true if
 * 				executed successfully; false, otherwise. It is important to note
 * 				that stopping this service does not stop the GCCL manager.
 *
 * @result returns true if executed successfully; false, otherwise.
 */
extern bool stop_LL_service(void);

/*!
 * @function stop_LL_service_of_type
 *
 * @description Stops the management performed by the linked list service for
 * 				lists associated with a given list type (ARRAY_LIST,DOUBLY_LINKED_LIST,
 * 				LINKED_LIST), removing all managed data structures associated with
 * 				that type. The function returns true if executed successfully;
 * 				false, otherwise.
 *
 *
 * @param gccl_list_type represents the type of list, which the managed data structures
 * 		  get removed from the Generic Commons C Library (GCCL) manager.
 *
 * @result returns true if executed successfully; false, otherwise.
 */
extern bool stop_LL_service_of_type(gccl_list_type_t gccl_list_type);

/*!
 * @function stop_LL_service_of_reference_id
 *
 * @description Stops the management performed by the linked list service for
 * 				lists associated with a given identifier, removing all managed
 * 				data structures associated with a given reference identifier.
 * 				The function returns true if executed successfully; false, otherwise.
 *
 *
 * @param reference_id represents a unique identifier associated with the lists
 * 		  of a given type.
 *
 * @result returns true if executed successfully; false, otherwise.
 */
extern bool stop_LL_service_of_reference_id(long reference_id);

/* ********** BEGIN - LINKED LIST INTERFACE ********** */

#ifdef __cplusplus
}
#endif

#endif /* LINKED_LIST_H_ */
