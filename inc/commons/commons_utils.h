/*
*****************************************************************************************
*
*Copyright (c) 2022-2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: common_utils.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

/*!
 * @header commons_utils.h
 *
 * This header file defines the commons functions, enums, structs, and unions used
 * by the implementation of data structures provided by the Generic Commons C
 * Library (GCCL).
 *
 */

#ifndef COMMONS_UTILS_H_
#define COMMONS_UTILS_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


/* ********** BEGIN - COMMON DATA TYPE DECLARATIONS ********** */

/*!
 *
 * @enum gccl_abs_ds_type_t
 *
 * @description This enum provides a general and abstract representation of the
 * 				types of data structures supported by the Generic Commons C Library
 * 				(GCCL). The following general and abstract types are included:
 * 				list, tree, other, and no data structure.
 *
 * @seealso gccl_list_type_t gccl_list_type_t
 *
 */
typedef enum {GCCL_LIST,GCCL_TREE,GCCL_DS_OTHER,GCCL_NO_DATA_STRUCTURE=-1} gccl_abs_ds_type_t;

/*!
 * @enum gccl_alloc_t
 *
 * @description This enum establishes the length rule utilised to create managed
 * 				data structures. The DYNAMIC option means the managed data
 * 				structures of a given type may increase their initial number
 * 				(i.e. allocation length) dynamically; the STATIC option, otherwise,
 * 				implies the maximum number of managed data structures of a given type
 * 				cannot be modified.
*/
typedef enum {GCCL_ALLOC_DYNAMIC = true, GCCL_ALLOC_STATIC = false} gccl_alloc_t;

/*!
 * @enum gccl_list_type_t
 *
 * @description This enum indicates the types of list supported by the Generic
 * 				Commons C Library (GCCL). The following types are included:
 * 				array list; doubly linked list; linked list.
 *
 */

typedef enum {GCCL_ARRAY_LIST, GCCL_DOUBLY_LINKED_LIST, GCCL_LINKED_LIST} gccl_list_type_t;

/*!
 *
 * @enum gccl_tree_type_t
 *
 * @description This enum indicates the types of tree supported by the Generic
 * 				Commons C Library (GCCL). The following types are included:
 * 				avl tree, red black tree.
 *
 */

typedef enum {GCCL_AVL_TREE=3,GCCL_RED_BLACK_TREE} gccl_tree_type_t;

/*!
 *
 * @enum gccl_ds_type_t
 *
 * @description This enum indicates other types of data structure that have
 * 				no specific group but are supported by the Generic Commons C
 * 				Library (GCCL). The following types are included: dictionary.
 *
 */

typedef enum {GCCL_DICTIONARY=5,GCCL_NO_STRUCTURE=-1} gccl_ds_type_t;

/*
 * @union uds_type_t
 *
 * @description This union indicates what data structure will be managed
 * by the Generic Commons C Library (GCCL) upon its creation. It is important to
 * reach the understanding that (internally) the GCCL manages the different types
 * of supported data structures on top of a linked list abstraction, which implies
 * that---in the context of the GCCL manager---a given data structure type represents
 * the data structure an internal linked list is carrying on. We must not mistake
 * the data structure type with the type of user data. Those are related yet
 * orthogonal concepts we explain next.
 *
 *	* Data Structure Type - represents what data structure will be created;
 *
 *  * Type of User Data - the type of the data an element of a given data structure
 *  					  holds. That type is only known by whom has created the
 *  					  data structure.
 *
 * When managing data structures other than linked lists, the distinguished data
 * structure services provided by the GCCL become users of the GCCL manager
 * themselves. If the data structure being created has no direct correlation
 * with a linked list, no more than one linked list element will be used to support
 * accessing and storing the elements of that data structure.
 *
 * @field ll_type represents the types of list supported by the GCCL.
 *
 * @field tree_type represents the types of tree supported by the GCCL.
 *
 * @field str_type represents the types of data structures supported by the GCCL
 * 				   other than lists and trees.
 *
 */
typedef union
{
    gccl_list_type_t ll_type;
    gccl_tree_type_t tree_type;
    gccl_ds_type_t str_type;
}gccl_uds_type_t;

/* ********** END - COMMON DATA TYPE DECLARATIONS ********** */

/* ********** BEGIN - COMMON INTERFACE ********** */

/*
 * @function gccl_bin_off_indication
 *
 * @description Indicates that any data retrieved by the user's application and
 * 				deleted from a given data structure managed by the Generic Common
 * 				C Library (GCCL), at a point of time previous to the call of this
 * 				function, will be erased from memory. As a consequence, any reference
 * 				to those data will be in an inconsistent state (eventually) and
 * 				could not be further utilised to perform any additional computation.
 */
extern void gccl_bin_off_indication(void);

/*
 * @function gccl_start_manager
 *
 * @description Starts the Generic Commons C Library (GCCL) manager, which implies
 * 				the set-up of the internal variables and data structures of the
 * 				GCCL. This function returns true if the manager is started
 * 				successfully; false, otherwise.
 *
 * @result returns true if the manager is started successfully; false, otherwise.
 */
extern bool gccl_start_manager(void);

/*
 * @function gccl_stop_manager
 *
 * @description Stops the Generic Commons C Library (GCCL) manager, which implies
 * 				in the removal of all managed data structures. This function returns
 * 				true if the manager is stopped successfully; false, otherwise.
 *
 * @result returns true if the manager is stopped successfully; false, otherwise.
 */

extern bool gccl_stop_manager(void);

/* ********** END - COMMON INTERFACE ********** */

#ifdef __cplusplus
}
#endif

#endif /* COMMON_UTILS_H_ */
