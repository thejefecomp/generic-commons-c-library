/*
*****************************************************************************************
*
*Copyright (c) 2024, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: pqueue.h
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/
#ifndef INC_QUEUE_PQUEUE_H_
#define INC_QUEUE_PQUEUE_H_


#include "list/list.h"

#ifdef __cplusplus
extern "C"{
#endif


extern int16_t create_pqueue(size_t data_alloc_size,
                             int8_t (*const comparator) (const void * const arg1,
                                                  	  	 const void * const arg2),
							 void (*const free_data) (const void * const data),
							 const char * const config_mask, ...);

extern bool exists_element_in_pqueue(const void * const data, int16_t index_pqueue);

extern int get_size_pqueue(int16_t index_pqueue);

extern long initialise_service_pqueue(gccl_alloc_t rule, gccl_list_type_t base_list_type,
		 	 	 	 	 	 	 	  int16_t initial_number_of_pqueues);

extern bool isEmpty_pqueue(int16_t index_pqueue);

extern bool isFull_pqueue(int16_t index_pqueue);

extern void * pdequeue_element(int16_t index_pqueue);

extern int penqueue_element(const void * const data, int16_t index_pqueue);

extern bool stop_service_pqueue(long reference_id);

#ifdef __cplusplus
}
#endif

#endif /* INC_QUEUE_PQUEUE_H_ */
