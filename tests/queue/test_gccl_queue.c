/*
*****************************************************************************************
*
*Copyright (c) 2024-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: test_queue.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include<string.h>

#include"unity.h"

#include"queue/queue.h"

typedef struct
{
	int index;

	char name[20];

	long uid;

} user_data_struct;

static long reference_id = -1L;

static int validation_dataset[10];


int8_t user_data_struct_comparator(const void * const arg1, const void * const arg2)
{
	return ((const user_data_struct *) arg1)->uid == ((const user_data_struct *) arg2)->uid ? 0
	                                          : ((const user_data_struct *) arg1)->uid > ((const user_data_struct *) arg2)->uid ? 1
	                                          : -1;
}


void setUp(void)
{
	reference_id = initialise_service_queue(GCCL_ALLOC_DYNAMIC,GCCL_LINKED_LIST,10);
}

void tearDown(void)
{
	stop_service_queue(reference_id);

	reference_id = -1L;

	gccl_stop_manager();
}

void test_create_queue(void)
{
	int16_t index_queue = -1;

	for(int i = 2; i < 20; i++)
	{
		index_queue = create_queue(sizeof(int), integer_comparator, NULL, NULL);

		TEST_ASSERT_EQUAL_INT_MESSAGE(i,index_queue,"FAILURE: THE RETURNED INDEX OF THE CREATED QUEUE WAS NOT WHAT WAS EXPECTED!");
	}

	index_queue = create_queue(sizeof(int), integer_comparator,NULL, "i",reference_id);

	TEST_ASSERT_EQUAL_INT_MESSAGE(20,index_queue,"FAILURE: THE RETURNED INDEX OF THE CREATED QUEUE WAS NOT WHAT WAS EXPECTED!");

}

void test_exists_element_in_queue(void)
{
	int data = 0;

	int16_t index_queue  = create_queue(sizeof(int), integer_comparator,NULL, NULL);

	for(int i = 9; i >=0; --i)
	{
		validation_dataset[i] = i+1;
		enqueue_element(validation_dataset+i, index_queue);
	}

	for(int i = 9; i >=0; --i)
	{
		TEST_ASSERT_TRUE_MESSAGE(exists_element_in_queue(validation_dataset+i, index_queue),"FAILURE: THE VALUE IS NOT IN THE QUEUE!");
	}

	TEST_ASSERT_FALSE_MESSAGE(exists_element_in_queue(&data, index_queue),"FAILURE: THE VALUE IS IN THE QUEUE!");
}

void test_isEmpty_queue(void)
{
	int data = 2;

	int16_t index_queue = create_queue(sizeof(int), integer_comparator, NULL, "s",1);

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_queue(index_queue),"FAILURE: THE QUEUE WAS NOT CREATED PROPERLY: IT IS NOT EMPTY!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(1,enqueue_element(&data, index_queue),"FAILURE: THE DATA WAS NOT INSERTED PROPERLY ON THE QUEUE!");

	TEST_ASSERT_FALSE_MESSAGE(isEmpty_queue(index_queue),"FAILURE: THE QUEUE IS EMPTY!");

	int* element_ptr = (int *)dequeue_element(index_queue);

	TEST_ASSERT_NOT_NULL_MESSAGE(element_ptr,"FAILURE: THE RETURNED REFERENCE IS NULL!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(data,*element_ptr,"FAILURE: THE RETRIEVED DATA IS NOT EQUAL TO THE ENQUEUED ONE!");

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_queue(index_queue),"FAILURE: THE QUEUE IS NOT EMPTY!");
}

void test_isFull_queue(void)
{
	int data = 5;

	int* element_ptr = NULL;

	int16_t index_queue = create_queue(sizeof(int), integer_comparator, NULL, "s", 1);

	TEST_ASSERT_FALSE_MESSAGE(isFull_queue(index_queue), "FAILURE: THE QUEUE WAS NOT CREATED PROPERLY: IT IS FULL!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(1,enqueue_element(&data, index_queue),"FAILURE: THE DATA WAS NOT INSERTED PROPERLY ON THE QUEUE!");

	TEST_ASSERT_TRUE_MESSAGE(isFull_queue(index_queue),"FAILURE: THE QUEUE IS NOT FULL!");

	element_ptr = (int*) dequeue_element(index_queue);

	TEST_ASSERT_NOT_NULL_MESSAGE(element_ptr,"FAILURE: THE RETURNED REFERENCE IS NULL!");
}

void test_enqueue_and_dequeue(void)
{
	user_data_struct *element_ptr = NULL;

	int16_t index_queue = create_queue(sizeof(user_data_struct),user_data_struct_comparator,NULL, NULL);

	char name[20] = "Test\0";

	user_data_struct user_data[20];

	for(int i = 0; i < 20; ++i)
	{
		user_data[i].index = i;
		strcpy(user_data[i].name,name);
		user_data[i].uid = i+1;

		enqueue_element(user_data+i, index_queue);

		TEST_ASSERT_EQUAL_INT_MESSAGE(i+1,get_size_queue(index_queue),"FAILURE: THE RETURNED SIZE OF THE QUEUE WAS NOT WHAT WAS EXPECTED!");
	}

	for(int i = 0; i < 20; ++i)
	{
		element_ptr = dequeue_element(index_queue);

		TEST_ASSERT_EQUAL_INT_MESSAGE(19-i,get_size_queue(index_queue),"FAILURE: THE RETURNED SIZE OF THE QUEUE WAS NOT WHAT WAS EXPECTED!");

		TEST_ASSERT_EQUAL_INT_MESSAGE(user_data[i].index,element_ptr->index,"FAILURE: THE INDEX EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE QUEUE WAS NOT WHAT WAS EXPECTED!");

		TEST_ASSERT_EQUAL_CHAR_ARRAY_MESSAGE(user_data[i].name,element_ptr->name,20,"FAILURE: THE NAME EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE QUEUE WAS NOT WHAT WAS EXPECTED!");

		TEST_ASSERT_EQUAL_INT64_MESSAGE(user_data[i].uid,element_ptr->uid,"FAILURE: THE UID EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE QUEUE WAS NOT WHAT WAS EXPECTED!");
	}
}


int main(void)
{
	int nr_test_functions = 5;

		void* functionsPointer[] = {test_create_queue,\
									test_exists_element_in_queue,\
									test_isEmpty_queue,\
									test_isFull_queue,\
									test_enqueue_and_dequeue};

		char* functionsName[] = {"test_create_queue()",\
								 "test_exists_element_in_queue()",\
								 "test_isEmpty_queue()",\
								 "test_isFull_queue()",\
								 "test_enqueue_and_dequeue()"};

		for(int i = 0; i < nr_test_functions; ++i)
			UnityDefaultTestRun(functionsPointer[i],functionsName[i],i);

		return 0;
}
