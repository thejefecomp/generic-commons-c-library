/*
*****************************************************************************************
*
*Copyright (c) 2024-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: test_pqueue.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include<string.h>

#include "unity.h"

#include "queue/pqueue.h"


typedef struct
{
	int index;

	char name[20];

	long priority;

} user_data_struct;

static long reference_id = -1L;

static int validation_dataset[10];


int8_t user_data_struct_comparator(const void * const arg1, const void * const arg2)
{
	return ((user_data_struct *) arg1)->priority == ((user_data_struct *) arg2)->priority ? 0
								 : ((user_data_struct *) arg1)->priority > ((user_data_struct *) arg2)->priority ? 1
								 : -1;
}

void setUp(void)
{
	reference_id = initialise_service_pqueue(GCCL_ALLOC_DYNAMIC, GCCL_LINKED_LIST, 10);
}

void tearDown(void)
{
	stop_service_pqueue(reference_id);

	reference_id = -1L;

	gccl_stop_manager();
}

void test_create_pqueue(void)
{
	int16_t index_pqueue = -1;

	for(int i = 2; i < 20; i++)
	{
		index_pqueue = create_pqueue(sizeof(int), integer_comparator, NULL, NULL);

		TEST_ASSERT_EQUAL_INT_MESSAGE(i,index_pqueue,"FAILURE: THE RETURNED INDEX OF THE CREATED PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");
	}

	index_pqueue = create_pqueue(sizeof(int), integer_comparator, NULL, "i",reference_id);

	TEST_ASSERT_EQUAL_INT_MESSAGE(20,index_pqueue,"FAILURE: THE RETURNED INDEX OF THE CREATED PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");
}

void test_exists_element_in_pqueue(void)
{
	int data = 0;

	int16_t index_pqueue  = create_pqueue(sizeof(int), integer_comparator,NULL, NULL);

	for(int i = 9; i >=0; --i)
	{
		validation_dataset[i] = i+1;
		penqueue_element(validation_dataset+i, index_pqueue);
	}

	for(int i = 9; i >= 0; --i)
	{
		TEST_ASSERT_TRUE_MESSAGE(exists_element_in_pqueue(validation_dataset+i, index_pqueue),"FAILURE: THE VALUE IS NOT IN THE PRIORITY QUEUE!");
	}

	TEST_ASSERT_FALSE_MESSAGE(exists_element_in_pqueue(&data, index_pqueue),"FAILURE: THE VALUE IS IN THE PRIORITY QUEUE!");
}

void test_isEmpty_pqueue(void)
{
	int data = 2;

	int* element_ptr = NULL;

	int16_t index_pqueue = create_pqueue(sizeof(int), integer_comparator, NULL, "s",1);

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_pqueue(index_pqueue),"FAILURE: THE PRIORITY QUEUE WAS NOT CREATED PROPERLY: IT IS NOT EMPTY!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(1,penqueue_element(&data, index_pqueue),"FAILURE: THE DATA WAS NOT INSERTED PROPERLY IN THE PRIORITY QUEUE!");

	TEST_ASSERT_FALSE_MESSAGE(isEmpty_pqueue(index_pqueue),"FAILURE: THE PRIORITY QUEUE IS EMPTY!");

	element_ptr = (int *)pdequeue_element(index_pqueue);

	TEST_ASSERT_NOT_NULL_MESSAGE(element_ptr,"FAILURE: THE RETURNED REFERENCE IS NULL!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(data,*element_ptr,"FAILURE: THE RETRIEVED DATA IS NOT EQUAL TO THE ENQUEUED ONE!");

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_pqueue(index_pqueue),"FAILURE: THE PRIORITY QUEUE IS NOT EMPTY!");
}

void test_isFull_pqueue(void)
{
	int data = 5;

	int* element_ptr = NULL;

	int16_t index_pqueue = create_pqueue(sizeof(int), integer_comparator, NULL, "s", 1);

	TEST_ASSERT_FALSE_MESSAGE(isFull_pqueue(index_pqueue), "FAILURE: THE PRIORITY QUEUE WAS NOT CREATED PROPERLY: IT IS FULL!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(1,penqueue_element(&data, index_pqueue),"FAILURE: THE DATA WAS NOT INSERTED PROPERLY IN THE PRIORITY QUEUE!");

	TEST_ASSERT_TRUE_MESSAGE(isFull_pqueue(index_pqueue),"FAILURE: THE PRIORITY QUEUE IS NOT FULL!");

	element_ptr = (int*) pdequeue_element(index_pqueue);

	TEST_ASSERT_NOT_NULL_MESSAGE(element_ptr,"FAILURE: THE RETURNED REFERENCE IS NULL!");
}

void test_penqueue_and_pdequeue(void)
{
	user_data_struct *element_ptr = NULL;

	int16_t index_pqueue = create_pqueue(sizeof(user_data_struct),user_data_struct_comparator,NULL, NULL);

	char name[20] = "Test\0";

	user_data_struct user_data[20];

	for(int i = 1; i < 21; ++i)
	{
		user_data[i-1].index = i-1;
		strcpy(user_data[i-1].name,name);
		user_data[i-1].priority = i%2 == 0 ? 1 : 2;

		penqueue_element(user_data+i-1, index_pqueue);

		TEST_ASSERT_EQUAL_INT_MESSAGE(i,get_size_pqueue(index_pqueue),"FAILURE: THE RETURNED SIZE OF THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");
	}


	for(int i = 1; i < 20; i+=2)
	{
		element_ptr = pdequeue_element(index_pqueue);

		TEST_ASSERT_EQUAL_INT_MESSAGE(user_data[i].index,element_ptr->index,"FAILURE: THE INDEX EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");

		TEST_ASSERT_EQUAL_CHAR_ARRAY_MESSAGE(user_data[i].name,element_ptr->name,20,"FAILURE: THE NAME EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");

		TEST_ASSERT_EQUAL_INT64_MESSAGE(user_data[i].priority,element_ptr->priority,"FAILURE: THE PRIORITY EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");
	}

	TEST_ASSERT_EQUAL_INT_MESSAGE(10,get_size_pqueue(index_pqueue),"FAILURE: THE RETURNED SIZE OF THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");

	for(int i = 0; i < 20; i+=2)
	{
		element_ptr = pdequeue_element(index_pqueue);

		TEST_ASSERT_EQUAL_INT_MESSAGE(user_data[i].index,element_ptr->index,"FAILURE: THE INDEX EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");

		TEST_ASSERT_EQUAL_CHAR_ARRAY_MESSAGE(user_data[i].name,element_ptr->name,20,"FAILURE: THE NAME EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");

		TEST_ASSERT_EQUAL_INT64_MESSAGE(user_data[i].priority,element_ptr->priority,"FAILURE: THE PRIORITY EXTRACTED FROM THE ELEMENT RETRIEVED FROM THE PRIORITY QUEUE WAS NOT WHAT WAS EXPECTED!");
	}

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_pqueue(index_pqueue),"FAILURE: THE PRIORITY QUEUE WAS NOT CREATED PROPERLY: IT IS NOT EMPTY!");
}

int main(void)
{
	int nr_test_functions = 5;

		void* functionsPointer[] = {test_create_pqueue,\
								    test_exists_element_in_pqueue,\
									test_isEmpty_pqueue,\
									test_isFull_pqueue,\
									test_penqueue_and_pdequeue};

		char* functionsName[] = {"test_create_pqueue()",\
								 "test_exists_element_in_pqueue()",\
								 "test_isEmpty_pqueue()",\
								 "test_isFull_pqueue()",\
								 "test_penqueue_and_pdequeue()"};

		for(int i = 0; i < nr_test_functions; ++i)
			UnityDefaultTestRun(functionsPointer[i],functionsName[i],i);

		return 0;
}
