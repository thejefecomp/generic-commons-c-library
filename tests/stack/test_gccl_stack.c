/*
*****************************************************************************************
*
*Copyright (c) 2021-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: test_stack.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include <stdlib.h>

#include"unity.h"

#include"stack/stack.h"

static long reference_id = -1L;

static int validation_dataset[10];

typedef struct
{
	int id;

	int nr_rows;

	int nr_columns;

	int **matrix;

}user_free_data_t;

static int8_t user_data_comparator(const void* const arg1, const void* const arg2)
{
	const user_free_data_t param1 = *((const user_free_data_t*)arg1);

	const user_free_data_t param2 = *((const user_free_data_t*)arg2);

	return param1.id == param2.id ? 0 : param1.id < param2.id ? -1 : 1;
}

static void user_free_data(const void * const data)
{
	user_free_data_t *user_data = (user_free_data_t*)data;

	for(int i = user_data->nr_rows-1; i >= 0; --i)
		free(*(user_data->matrix+i));

	free(user_data->matrix);

	TEST_ASSERT(1);
}

void setUp(void)
{
	reference_id = initialise_service_stack(GCCL_ALLOC_DYNAMIC,GCCL_LINKED_LIST,10);
}

void tearDown(void)
{
	stop_service_stack(reference_id);

	reference_id = -1L;

	gccl_stop_manager();
}

void test_create_stack (void)
{
	int16_t index_stack = -1;

	index_stack = create_stack(sizeof(int), integer_comparator, NULL, NULL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(2, index_stack, "FAILURE: THE NEW STACK WAS NOT CREATED PROPERLY!");

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_stack(index_stack), "FAILURE: THE STACK IS NOT EMPTY!");

	TEST_ASSERT_FALSE_MESSAGE(isFull_stack(index_stack), "FAILURE: THE STACK IS FULL!");

	index_stack = create_stack(sizeof(int), integer_comparator, NULL, "i",reference_id);

	TEST_ASSERT_EQUAL_INT_MESSAGE(3, index_stack, "FAILURE: THE NEW STACK WAS NOT CREATED PROPERLY!");
}

void test_exists_element_in_stack(void)
{
	int16_t index_stack  = create_stack(sizeof(int), integer_comparator, NULL, NULL);

	for(int i = 9; i >=0; --i)
	{
		validation_dataset[i] = i+1;
		push_element_stack(validation_dataset+i, index_stack);
	}

	for(int i = 9; i >=0; --i)
	{
		TEST_ASSERT_TRUE_MESSAGE(exists_element_in_stack(validation_dataset+i, index_stack),"FAILURE: THE VALUE IS NOT IN THE STACK!");
	}

	int data = 0;

	TEST_ASSERT_FALSE_MESSAGE(exists_element_in_stack(&data, index_stack),"FAILURE: THE VALUE IS IN THE STACK!");
}

void test_free_func_stack(void)
{
	int16_t index_stack = create_stack(sizeof(user_free_data_t), user_data_comparator,
									   user_free_data, NULL);

	user_free_data_t user_data;

	user_data.id = 0;
	user_data.nr_rows = 2;
	user_data.nr_columns = 2;

	user_data.matrix = (int**)malloc(user_data.nr_rows * sizeof(int*));

	TEST_ASSERT_NOT_NULL_MESSAGE(user_data.matrix, "FAILURE: THIS POINTER TO POINTER COULD NOT BE NULL!");

	for(int i = 0; i < user_data.nr_rows; ++i)
		*(user_data.matrix+i) = (int*)malloc(user_data.nr_columns * sizeof(int));

	for(int i = 0; i < user_data.nr_rows; i++)
		for(int j = 0; j < user_data.nr_columns; j++)
		{
			user_data.matrix[i][j] = i+j;
		}

	push_element_stack(&user_data, index_stack);

	TEST_ASSERT_EQUAL_INT_MESSAGE(1,get_size_stack(index_stack),"FAILUE: SIZE MUST BE ONE (1)!");
}

void test_isEmpty_stack(void)
{

	int16_t index_stack = -1;

	index_stack = create_stack(sizeof(int), integer_comparator, NULL, NULL);

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_stack(index_stack), "FAILURE: THE STACK IS NOT EMPTY!");

	int data = 1;

	push_element_stack(&data, index_stack);

	TEST_ASSERT_FALSE_MESSAGE(isEmpty_stack(index_stack), "FAILURE: THE STACK IS EMPTY!");

	pop_element_stack(index_stack);

	TEST_ASSERT_TRUE_MESSAGE(isEmpty_stack(index_stack), "FAILURE: THE STACK IS NOT EMPTY!");
}

void test_isFull_stack(void)
{

	int16_t index_stack = -1;

	index_stack = create_stack(sizeof(int), integer_comparator, NULL, "s", 1);

	TEST_ASSERT_FALSE_MESSAGE(isFull_stack(index_stack), "FAILURE: THE STACK IS EMPTY!");

	int data = 1;

	push_element_stack(&data, index_stack);

	TEST_ASSERT_TRUE_MESSAGE(isFull_stack(index_stack), "FAILURE: THE STACK IS NOT FULL!");

	pop_element_stack(index_stack);

	TEST_ASSERT_FALSE_MESSAGE(isFull_stack(index_stack), "FAILURE: THE STACK IS FULL!");
}

void test_push_and_pop_element_stack(void)
{

	int16_t index_stack = -1;

	index_stack = create_stack(sizeof(int), integer_comparator, NULL, "s", 10);

	int stack_size = 0;

	for(int i = 0; i < 10; i++){

		validation_dataset[i] = i;
		stack_size = push_element_stack(&i, index_stack);
		TEST_ASSERT_EQUAL_INT_MESSAGE(i+1,stack_size,"FAILURE: THE SIZE OF THE STACK IS WRONG!");
	}

	TEST_ASSERT_EQUAL_INT_MESSAGE(-1,push_element_stack(validation_dataset, index_stack),"FAILURE: THE STACK IS NOT FULL!");

	int* element_ptr = NULL;

	for(int i = 9; i>=0; i--){

		element_ptr = (int*)pop_element_stack(index_stack);

		TEST_ASSERT_EQUAL_INT_MESSAGE(validation_dataset[i],*element_ptr, "FAILURE: THE RETRIEVED ELEMENT FROM THE STACK WAS NOT WHAT WAS EXPECTED!");
		TEST_ASSERT_EQUAL_INT_MESSAGE(i,get_size_stack(index_stack),"FAILURE: THE SIZE OF THE STACK IS WRONG!");
	}

	TEST_ASSERT_NULL_MESSAGE(pop_element_stack(index_stack),"FAILURE: THE STACK IS NOT EMPTY!");
}


int main(void)
{
	int nr_test_functions = 6;

	void* functionsPointer[] = {test_create_stack,\
								test_exists_element_in_stack,\
								test_free_func_stack,\
								test_isEmpty_stack,\
								test_isFull_stack,\
								test_push_and_pop_element_stack};

	char* functionsName[] = {"test_create_stack()",\
							 "test_exists_element_in_stack()",\
							 "test_free_func_stack()",\
							 "test_isEmpty_stack()",\
							 "test_isFull_stack()",\
							 "test_push_and_pop_element_stack()"};

	for(int i =0; i < nr_test_functions; ++i){

		UnityDefaultTestRun(functionsPointer[i],functionsName[i],i);
	}

	return 0;
}
