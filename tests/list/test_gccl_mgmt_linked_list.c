/*
*****************************************************************************************
*
*Copyright (c) 2013-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: test_mgmt_linked_list.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include <stdlib.h>
#include <string.h>

#include "unity.h"

#include "list/linked_list_mgmt.h"
#include "list/linked_list.h"

static gccl_alloc_t allocation = GCCL_ALLOC_DYNAMIC;

static gccl_uds_type_t element_str_type = {GCCL_LINKED_LIST};

typedef struct
{
	int index;

	void *metadata;

}user_free_data_t;

static int8_t user_data_comparator(const void* const arg1, const void* const arg2)
{
	const user_free_data_t param1 = *((const user_free_data_t*)arg1);

	const user_free_data_t param2 = *((const user_free_data_t*)arg2);

	return param1.index == param2.index ? 0 : param1.index < param2.index ? -1 : 1;
}

static void user_free_data(const void * const data)
{
	user_free_data_t *user_data = (user_free_data_t*)data;

	free(user_data->metadata);

	TEST_ASSERT(1);
}

void setUp(void)
{
	gccl_mgmt_initialise_LL_manager(allocation,element_str_type,10);
}

void tearDown(void)
{
	gccl_mgmt_stop_LL_manager();
}

void test_mgmt_check_reference_id(void)
{
	int16_t index_LL = -1;

	long reference_id = -1;

	TEST_ASSERT_FALSE_MESSAGE(gccl_mgmt_check_reference_id(reference_id),"FAILURE: THE MGMGT_CHECK_REFERENCE_ID() MUST RETURN FALSE FOR A NEGATIVE IDENTIFIER!");

	reference_id = gccl_mgmt_generate_reference_id();

	index_LL = create_LL(sizeof(int),integer_comparator,NULL,"i",reference_id);

	TEST_ASSERT_GREATER_OR_EQUAL_INT16_MESSAGE(2,index_LL,"FAILURE: THE INDEX MUST BE GREATER OR EQUAL TO 2!");

	TEST_ASSERT_TRUE_MESSAGE(gccl_mgmt_check_reference_id(reference_id),"FAILURE: THE PROVIDED REFERENCE ID MUST EXIST IN THE GCCL DATA STRUCTURE INDEX!");

	reference_id = gccl_mgmt_generate_reference_id();

	TEST_ASSERT_FALSE_MESSAGE(gccl_mgmt_check_reference_id(reference_id),"FAILURE: THE PROVIDED REFERENCE ID MUST NOT EXIST IN THE GCCL DATA STRUCTURE INDEX!");

	destroy_LL(index_LL);
}

void test_mgmt_create_LL(void)
{
	int16_t index_LL = -1;

	int max_size = -1;

	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type, NULL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(2,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	TEST_ASSERT_TRUE_MESSAGE(index_LL > 0,"FAILURE: THE NEW LINKED LIST WAS NOT CREATED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(INT32_MAX,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type, "");

	TEST_ASSERT_EQUAL_INT_MESSAGE(3,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(INT32_MAX,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"s",30);

	TEST_ASSERT_EQUAL_INT_MESSAGE(4,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(30, max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");

	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"sl",10,false);

	TEST_ASSERT_EQUAL_INT_MESSAGE(5,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(10,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"sl",7,true);

	TEST_ASSERT_EQUAL_INT_MESSAGE(6,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(7,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"l",false);

	TEST_ASSERT_EQUAL_INT_MESSAGE(7,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(INT32_MAX,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"l",true);

	TEST_ASSERT_EQUAL_INT_MESSAGE(8,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(INT32_MAX,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"u",true);

	TEST_ASSERT_EQUAL_INT_MESSAGE(9,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(INT32_MAX,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"u",false);

	TEST_ASSERT_EQUAL_INT_MESSAGE(10,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(INT32_MAX,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"sul",89,true,true);

	TEST_ASSERT_EQUAL_INT_MESSAGE(11,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(89,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");


	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"sliu", 33, 2358L,true);

	TEST_ASSERT_EQUAL_INT_MESSAGE(12,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

	max_size  = get_max_size_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(33,max_size,"FAILURE: THE MAX_SIZE VALUE OF THE CREATED LINKED LIST IS DIFFERENT FROM WHAT IS EXPECTED!");

	index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,"suli",56,true,true,746890L);

	TEST_ASSERT_EQUAL_INT_MESSAGE(13,index_LL,"FAILURE: THE LINKED LIST INDEX WAS NOT INCREMENTED PROPERLY!");

}

void test_mgmt_create_LL_get_rem_list(void)
{
	int16_t index_LL = gccl_mgmt_create_LL(sizeof(int16_t), integer_comparator, NULL, element_str_type,NULL);

	gccl_mgmt_free_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(2, gccl_mgmt_create_LL(sizeof(int), integer_comparator, NULL, element_str_type,NULL), "FAILURE: THE LINKED LIST OF PREVIOUSLY REMOVED LISTS WAS NOT RECYCLED PROPERLY!");
}

void test_mgmt_free_all_typed_LL(void)
{
	TEST_ASSERT_EQUAL_INT_MESSAGE(2,gccl_mgmt_get_total_typed_LL(element_str_type, GCCL_LIST),"FAILURE: THE NUMBER OF MANAGED LINKED LISTS IS DIFFERENT FROM WHAT IS EXPECTED!");

	gccl_mgmt_create_LL(sizeof(int), integer_comparator, NULL, element_str_type, NULL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(3,gccl_mgmt_get_total_typed_LL(element_str_type, GCCL_LIST),"FAILURE: THE NUMBER OF MANAGED LINKED LISTS IS DIFFERENT FROM WHAT IS EXPECTED!");

	gccl_mgmt_free_all_typed_LL(element_str_type, GCCL_LIST);

	TEST_ASSERT_EQUAL_INT_MESSAGE(2,gccl_mgmt_get_total_typed_LL(element_str_type, GCCL_LIST),"FAILURE: THE NUMBER OF MANAGED LINKED LISTS IS DIFFERENT FROM WHAT IS EXPECTED!");
}

void test_mgmt_free_func(void)
{
	user_free_data_t user_data = {-1, NULL};

	user_data.index = 0;

	user_data.metadata = malloc(sizeof(int));

	*((int*)(user_data.metadata)) = 1;

	int16_t index_LL = gccl_mgmt_create_LL(sizeof(user_free_data_t), user_data_comparator, user_free_data, element_str_type, NULL);

	insert_element_LL(&user_data, INS_LAST, index_LL);

	get_and_delete_head_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(0,get_size_LL(index_LL),"FAILUE: SIZE MUST BE ZERO (0)!");
}

void test_mgmt_free_func_reset(void)
{
	user_free_data_t user_data = {-1, NULL};

	int16_t index_LL = gccl_mgmt_create_LL(sizeof(user_free_data_t), user_data_comparator, user_free_data, element_str_type, NULL);

	for(int i = 0; i < 20; i++)
	{
		user_data.index = i;

		user_data.metadata = malloc(sizeof(int));

		*((int*)(user_data.metadata)) = i+1;

		insert_element_LL(&user_data, INS_LAST, index_LL);
	}

	TEST_ASSERT_EQUAL_INT_MESSAGE(20,get_size_LL(index_LL),"FAILUE: SIZE MUST BE TWENTY (20)!");
}

void test_mgmt_generate_reference_id(void)
{
	long generated_reference_id = gccl_mgmt_generate_reference_id();

	TEST_ASSERT_GREATER_OR_EQUAL_INT_MESSAGE(0,generated_reference_id,"FAILURE: THE GENERATED REFERENCE ID MUST BE GREATER THAN OR EQUAL TO 0!");
}

void test_mgmt_get_array_copy_LL(void)
{
	int* array_list = NULL;

	int16_t index_LL = gccl_mgmt_create_LL(sizeof(int), integer_comparator,NULL,element_str_type,"s", 30);

	int validation_dataset_array[30];

	array_list = (int *)gccl_mgmt_get_array_copy_LL(-1,integer_element_array_copy);

	TEST_ASSERT_NULL_MESSAGE(array_list,"FAILURE: THE POINTER FOR THE ARRAY COPY OF AN INVALID LINKED LIST INDEX MUST HOLD A NULL MEMORY ADDRESS REFERENCE!");

	array_list = (int *)gccl_mgmt_get_array_copy_LL(index_LL,integer_element_array_copy);

	TEST_ASSERT_NOT_NULL_MESSAGE(array_list,"FAILURE: THE POINTER FOR THE ARRAY COPY OF A GIVEN LINKED LIST MUST HOLD A VALID MEMORY ADDRESS REFERENCE, EVEN IF THE LINKED LIST IS EMPTY!");

	gccl_mgmt_free_array_copy_LL(array_list);

	for(int i = 0; i < 30; ++i)
	{
		validation_dataset_array[i] = i;
		insert_element_LL(validation_dataset_array+i, INS_LAST, index_LL);
	}

	array_list = (int *) gccl_mgmt_get_array_copy_LL(index_LL,integer_element_array_copy);

	for(int i = 29; i >= 0; --i)
		TEST_ASSERT_EQUAL_INT_MESSAGE(validation_dataset_array[i],array_list[i],"FAILURE: THE VALUE OBTAINED FROM THE SOURCE DATASET DIFFERS FROM THE VALUE RETRIEVED FROM THE PERFORMED LINKED LIST COPY!");

	gccl_mgmt_free_array_copy_LL(array_list);
}


void test_mgmt_get_LL_simple_Dynamic(void)
{
	for(int i = 0; i < 11; ++i)
		TEST_ASSERT_EQUAL_INT_MESSAGE(i+2,gccl_mgmt_create_LL(sizeof(int), integer_comparator,NULL,element_str_type,NULL),"FAILURE: WRONG LINKED LIST INDEX RETURNED!");

	for(int i = 0; i < 13; ++i)
		TEST_ASSERT_NOT_NULL_MESSAGE(gccl_mgmt_get_LL(i), "FAILURE: THE LINKED LIST HEADER FROM A PREVIOUSLY CREATED LINKED LIST WAS NOT RETURNED PROPERLY!");

	TEST_ASSERT_NULL_MESSAGE(gccl_mgmt_get_LL(13), "FAILURE: THE VALUE OF THE LINKED LIST HEADER RETURNED IS NOT NULL!");

	TEST_ASSERT_NULL_MESSAGE(gccl_mgmt_get_LL(-20), "FAILURE: THE VALUE OF THE LINKED LIST HEADER RETURNED IS NOT NULL!");
}

void test_mgmt_get_LL_simple_Static(void)
{
	int index_LL = -1;

	gccl_mgmt_initialise_LL_manager(GCCL_ALLOC_STATIC,element_str_type,0);

	for(int i = 0; i < 12; ++i)
	{
		if(i < 11)
		{
			index_LL = gccl_mgmt_create_LL(sizeof(int),integer_comparator,NULL,element_str_type,NULL);

			TEST_ASSERT_EQUAL_INT_MESSAGE(i+2,index_LL,"FAILURE: WRONG LINKED LIST INDEX RETURNED!");
		}

		else
			TEST_ASSERT_EQUAL_INT_MESSAGE(-2,gccl_mgmt_create_LL(sizeof(int), integer_comparator,NULL,element_str_type,NULL),"FAILURE: STATIC RULE DOES NOT ALLOW THE NUMBER OF LINKED LISTS GROWING DYNAMICALLY. THE VALUE RETURNED IS NOT WHAT WAS EXPECTED!");
	}

	for(int i = 0; i < 14; ++i)
	{
		if(i < 13)
		{
			TEST_ASSERT_NOT_NULL_MESSAGE(gccl_mgmt_get_LL(i), "FAILURE: THE LINKED LIST HEADER FROM A PREVIOUSLY CREATED LINKED LIST WAS NOT RETURNED PROPERLY!");
		}
		else
			TEST_ASSERT_NULL_MESSAGE(gccl_mgmt_get_LL(i), "FAILURE: THE VALUE OF THE LINKED LIST HEADER RETURNED IS NOT NULL!");
	}

	TEST_ASSERT_NULL_MESSAGE(gccl_mgmt_get_LL(13), "FAILURE: THE VALUE OF THE LINKED LIST HEADER RETURNED IS NOT NULL!");

	TEST_ASSERT_NULL_MESSAGE(gccl_mgmt_get_LL(-20), "FAILURE: THE VALUE OF THE LINKED LIST HEADER RETURNED IS NOT NULL!");
}


int main(void)
{
	int nr_test_functions = 10;

	void* functions_pointer[] = {test_mgmt_check_reference_id,\
								 test_mgmt_create_LL,\
								 test_mgmt_create_LL_get_rem_list,\
								 test_mgmt_free_all_typed_LL,\
								 test_mgmt_free_func,\
								 test_mgmt_free_func_reset,\
								 test_mgmt_generate_reference_id,\
								 test_mgmt_get_array_copy_LL,\
								 test_mgmt_get_LL_simple_Dynamic,\
								 test_mgmt_get_LL_simple_Static};


	char* functions_name[] = {"test_mgmt_check_reference_id()",\
							  "test_mgmt_create_LL()",\
							  "test_mgmt_create_LL_get_rem_list()",\
							  "test_mgmt_free_all_typed_LL()",\
							  "test_mgmt_free_func()",\
							  "test_mgmt_free_func_reset()",\
							  "test_mgmt_generate_reference_id()",\
							  "test_mgmt_get_array_copy_LL()",\
							  "test_mgmt_get_LL_simple_Dynamic()",\
						      "test_mgmt_get_LL_simple_Static()"};

	for(int i = 0; i < nr_test_functions; ++i)
		UnityDefaultTestRun(functions_pointer[i],functions_name[i],i);

	return 0;
}
