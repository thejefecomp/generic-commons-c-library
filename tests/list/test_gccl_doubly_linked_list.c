/*
*****************************************************************************************
*
*Copyright (c) 2022-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: test_doubly_linked_list.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include "unity.h"

#include "list/doubly_linked_list.h"
#include "list/linked_list_mgmt.h"


typedef struct
{
	int number1;
	float number2;

}test_data_type;

static long reference_id = -1L;

int8_t comparator_test_data_type (const void * const arg1, const void * const arg2)
{
	const test_data_type * param1 = (const test_data_type *)arg1;

	const test_data_type * param2 = (const test_data_type *)arg2;

	int8_t result = 0;

	result = param1->number1 > param2->number2 ? 1 :
			 param1->number1 < param2->number2 ? -1 :
			 0;

	return result;
}


void setUp(void)
{
	reference_id = initialise_doubly_LL_service(GCCL_ALLOC_DYNAMIC,10);
}

void tearDown(void)
{
	stop_doubly_LL_service();
	reference_id = -1L;
	gccl_stop_manager();
}


void test_create_doubly_LL(void)
{
	int16_t index_DLL = create_doubly_LL(sizeof(test_data_type), comparator_test_data_type, NULL, NULL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(2,index_DLL,"FAILURE: THE RETURNED INDEX OF THE NEW DOUBLY LIKED LIST CREATED IS DIFFERENT FROM WHAT HAS BEEN EXPECTED!");

	index_DLL = create_doubly_LL(sizeof(int), integer_comparator, NULL, NULL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(3,index_DLL,"FAILURE: THE RETURNED INDEX OF THE NEW DOUBLY LIKED LIST CREATED IS DIFFERENT FROM WHAT HAS BEEN EXPECTED!");

	index_DLL = create_doubly_LL(sizeof(int), integer_comparator, NULL, "i",reference_id);

	TEST_ASSERT_EQUAL_INT_MESSAGE(4,index_DLL,"FAILURE: THE RETURNED INDEX OF THE NEW DOUBLY LIKED LIST CREATED IS DIFFERENT FROM WHAT HAS BEEN EXPECTED!");

}

int main(void)
{
    int nr_test_functions = 1;

    void* functionsPointer[] = {test_create_doubly_LL};

    char* functionsName[] = {"test_create_doubly_LL()"};

    for(int i = 0; i < nr_test_functions; ++i)
        UnityDefaultTestRun(functionsPointer[i],functionsName[i],i);

    return 0;
}
