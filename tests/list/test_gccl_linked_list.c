/*
*****************************************************************************************
*
*Copyright (c) 2013-2025, Jeferson Souza (thejefecomp) - thejefecomp@neartword.com
*All rights reserved.
*Project: Generic_Commons_C_Library
*
*Filename: test_linked_list.c
*
*This file is part of Generic_Commons_C_Library project.
*
*Redistribution and use in source and binary forms, with or without
*modification, are permitted provided that the following conditions are met:
*
** Redistributions of source code must retain the above copyright notice, this
*  list of conditions and the following disclaimer.
*
** Redistributions in binary form must reproduce the above copyright notice,
*  this list of conditions and the following disclaimer in the documentation
*  and/or other materials provided with the distribution.
*
** Neither the name of my-courses nor the names of its
*  contributors may be used to endorse or promote products derived from
*  this software without specific prior written permission.

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
*FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
*DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
*OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************
*/

#include<time.h>

#include"unity.h"

#include "list/linked_list.h"

static long reference_id = -1L;

static int validation_dataset[10];

/*
 * Auxiliary functions
 */

bool custom_sequential_search(const void *const param, const void *const status,
				   const void *const data){

	const int value = *((const int *)param);

	if((*(const int *)data) == value){

		*((bool *)status) = true;
	}

	return true;
}

bool multiply_by_two(const void *const param, const void *const status,
					 const void *const data)
{
	*(int *)data *=2;

	return true;
}

bool count_elements(const void *const param, const void *const status,
					 const void *const data)
{
	bool *is_counter_started = (bool *)param;

	int *counter = (int *)status;

	if(!(*is_counter_started)){

		*is_counter_started = true;
		*counter = 1;
	}
	else
		(*counter)++;

	return true;
}




bool sum_of_elements(const void *const param, const void *const status,
					 	   const void *const data)
{
	int *sum = (int *)status;

	(*sum)+= *(int *)data;

	return true;
}


void executeInsertModeTest(int16_t index_LL, int8_t insert_mode){

	for(int i = 1; i < 11; i++)
	{
		insert_element_LL(&i, insert_mode, index_LL);
	}
}

void checkInsertModeInsertion(int16_t index_LL, int8_t insert_mode){

	executeInsertModeTest(index_LL, insert_mode);

	int *data = 0;

	for(int i = 0; i < 10; i++)
	{
		data = ((int *)get_element_at_position_LL(i,index_LL));

		switch(insert_mode)
		{
			case INS_LAST:
			case INS_ASC_ORDER:

				TEST_ASSERT_EQUAL_INT_MESSAGE(i+1,*data, "FAILURE: THE CORRECT VALUE WAS NOT STORED PROPERLY ON THE LINKED LIST!");
				break;

			case INS_FIRST:
			case INS_DESC_ORDER:

				TEST_ASSERT_EQUAL_INT_MESSAGE(10-i,*data, "FAILURE: THE CORRECT VALUE WAS NOT STORED PROPERLY ON THE LINKED LIST!");
				break;

			default: break;
		}
	}
}

int16_t predef_data_set_integer()
{

	int16_t index_LL = create_LL(sizeof(int), integer_comparator,NULL, "s", 10);

	for(int i = 0; i < 10; ++i)
	{
		validation_dataset[i] = i;
		insert_element_LL(validation_dataset+i, INS_LAST, index_LL);
	}

	return index_LL;
}

void print_test_benchmark(char *const operation_label,
						  char *const data_type_operation,
						  char *const operation_strategy,
						  int number_of_elements,
						  clock_t total_number_of_clock_ticks,
						  double total_number_of_seconds,
						  double operation_throughput)
{

	printf("Duration of %d %s %s (%s)\n",number_of_elements,data_type_operation,operation_label, operation_strategy);
	printf("Number of clock ticks (cpu-time): %lu\n",total_number_of_clock_ticks);
	printf("Number of seconds (s): %f\n",total_number_of_seconds);
	printf("%s throughput (%s/second): %f\n",operation_label,operation_label,operation_throughput);
}

/*
 * Setup and tear-down functions
 */
void setUp(void)
{
	reference_id = initialise_LL_service(GCCL_ALLOC_DYNAMIC, 10);
}

void tearDown(void)
{
	stop_LL_service();

	reference_id = -1L;

	gccl_stop_manager();
}

/*
 * Test functions
 */

void test_create_LL (void)
{
	int16_t index_LL = -1;

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, NULL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(2, index_LL, "FAILURE: THE LINKED LIST HEADER WAS NOT INITIALISED PROPERLY!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(0, get_size_LL(index_LL), "FAILURE: THE SIZE OF THE LINKED LIST WAS NOT INITIALISED PROPERLY!");

	TEST_ASSERT_NULL_MESSAGE(get_head_LL(index_LL), "FAILURE: THE HEAD OF THE LINKED LIST WAS NOT INITIALISED PROPERLY!");

	TEST_ASSERT_NULL_MESSAGE(get_tail_LL(index_LL), "FAILURE: THE TAIL OF THE LINKED LIST WAS NOT INITIALISED PROPERLY!");

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, "i",reference_id);

	TEST_ASSERT_EQUAL_INT_MESSAGE(3, index_LL, "FAILURE: THE LINKED LIST HEADER WAS NOT INITIALISED PROPERLY!");

}

void test_destroy_LL (void)
{

	int16_t index_LL = -1;

	index_LL = predef_data_set_integer();

	TEST_ASSERT_TRUE_MESSAGE(destroy_LL(index_LL), "FAILURE: THE CREATED LINKED LIST WAS NOT DESTROYED PROPERLY!");
}

void test_exists_element_LL(void)
{
	int16_t index_LL = -1;

	index_LL = predef_data_set_integer();

	int *data_on_list = NULL;

	int value_to_test = -30;

	for(int i = 9; i >= 0; --i)
	{
		data_on_list = (int *) get_element_at_position_LL(i, index_LL);

		TEST_ASSERT_EQUAL_MESSAGE(true,exists_element_in_LL(data_on_list, index_LL), "FAILURE: ELEMENT DOES NOT EXIST ON THE LINKED LIST!");
	}

	TEST_ASSERT_EQUAL_MESSAGE(false,exists_element_in_LL(&value_to_test, index_LL), "FAILURE: ELEMENT EXISTS ON THE LINKED LIST!");
}

void test_get_and_delete_element_LL(void)
{
	int16_t index_LL = -1;

	index_LL = predef_data_set_integer();

	int *data_on_list = NULL;

	int head_pos = 0;

	for(int i = 0; i < 10; ++i)
	{
		data_on_list = (int *) get_and_delete_element_LL(&head_pos, DEL_AT_POS,index_LL);

		TEST_ASSERT_EQUAL_INT_MESSAGE(validation_dataset[i], *data_on_list, "FAILURE: THE CORRECT VALUE WAS NOT STORED PROPERLY ON THE LINKED LIST. STORED != INSERTED!");

		TEST_ASSERT_EQUAL_INT_MESSAGE(9-i, get_size_LL(index_LL), "FAILURE: THE CORRECT VALUE WAS NOT STORED PROPERLY ON THE LINKED LIST. STORED != INSERTED!");
	}

	TEST_ASSERT_EQUAL_INT_MESSAGE(0, get_size_LL(index_LL), "FAILURE: LINKED LIST SIZE WAS NOT UPDATED PROPERLY!");

	TEST_ASSERT_NULL_MESSAGE(get_head_LL(index_LL), "FAILURE: ELEMENTS WERE NOT UPDATED/REMOVED PROPERLY FROM THE LINKED LIST. HEAD IS NOT POINTING TO NULL!");

	TEST_ASSERT_NULL_MESSAGE(get_tail_LL(index_LL), "FAILURE: ELEMENTS WERE NOT UPDATED/REMOVED PROPERLY FROM THE LINKED LIST. TAIL IS NOT POINTING TO NULL!");
}

void test_get_and_delete_head_LL(void)
{
	int16_t index_LL = -1;

	index_LL = predef_data_set_integer();

	int size = get_size_LL(index_LL);

	int *head_on_list = (int *) get_head_LL(index_LL);;

	int *head_deleted = NULL;

	head_deleted = (int *) get_and_delete_head_LL(index_LL);

	TEST_ASSERT_EQUAL_INT_MESSAGE(size-1,get_size_LL(index_LL),"FAILURE: THE LINKED LIST HEAD WAS NOT REMOVED PROPERLY FROM THE LINKED LIST, OR THERE WAS A PROBLEM UPDATING ITS SIZE. ACTUAL_SIZE != EXPECTED_SIZE!");

	TEST_ASSERT_EQUAL_PTR_MESSAGE(head_on_list, head_deleted, "FAILURE: THE LINKED LIST HEAD WAS NOT RETRIEVED PROPERLY FROM THE LINKED LIST. RETRIEVED != DELETED!");
}

void test_insert_element_LL(void)
{
	int16_t index_LL = -1;

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, NULL);

	int8_t insert_modes[] = {INS_LAST,INS_FIRST,INS_ASC_ORDER, INS_DESC_ORDER};

	for (int i = 0; i < 4; ++i)
	{
		switch(insert_modes[i])
		{
			case INS_LAST:

				printf("Check INS_LAST mode\n");
				break;

			case INS_FIRST:

				printf("Check INS_FIRST mode\n");
				break;

			case INS_ASC_ORDER:

				printf("Check INS_ASC_ORDER mode\n");
				break;

			case INS_DESC_ORDER:

				printf("Check INS_DESC_ORDER mode\n");
				break;

			default: break;
		}

		checkInsertModeInsertion(index_LL, insert_modes[i]);
	}

}

void test_isEmpty_LL(void)
{
	int16_t index_LL = -1;

	int data = 8;

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, "s", 0);

	TEST_ASSERT_EQUAL_MESSAGE(true,isEmpty_LL(index_LL),"FAILURE: THE LINKED LIST HEADER WAS NOT INITIALISED PROPERLY. CHECK SIZE AND MAX_SIZE FIELDS!");

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, NULL);

	TEST_ASSERT_EQUAL_MESSAGE(true,isEmpty_LL(index_LL),"FAILURE: THE LINKED LIST HEADER WAS NOT INITIALISED PROPERLY. CHECK SIZE AND MAX_SIZE FIELDS!");

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, "s", 10);

	insert_element_LL(&data, INS_LAST, index_LL);

	TEST_ASSERT_EQUAL_MESSAGE(false,isEmpty_LL(index_LL),"FAILURE: SIZE IS NOT CORRECT AFTER EXECUTING THE INSERT OPERATION!");
}

void test_isFull_LL(void)
{
	int16_t index_LL = -1;

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, "s", 0);

	TEST_ASSERT_EQUAL_MESSAGE(true,isFull_LL(index_LL),"FAILURE: THE LINKED LIST HEADER WAS NOT INITIALISED PROPERLY. CHECK SIZE AND MAX_SIZE FIELDS!");

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, NULL);

	TEST_ASSERT_EQUAL_MESSAGE(false,isFull_LL(index_LL),"FAILURE: THE LINKED LIST HEADER WAS NOT INITIALISED PROPERLY. CHECK SIZE AND MAX_SIZE FIELDS!");

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, "s", 10);

	TEST_ASSERT_EQUAL_MESSAGE(false,isFull_LL(index_LL),"FAILURE: THE LINKED LIST HEADER WAS NOT INITIALISED PROPERLY. CHECK SIZE AND MAX_SIZE FIELDS!");
}

void test_iterate_LL(void)
{
	int counter = 0;

	int *data = NULL;

	int data_to_be_found = 2;

	int expected_value = -1;

	int16_t index_LL = -1;

	long sum = 0;

	index_LL = predef_data_set_integer();

	TEST_ASSERT_TRUE_MESSAGE(iterate_LL(index_LL, multiply_by_two, 0, 9, true, NULL, NULL),"FAILURE: THERE IS A PROBLEM WHILE APPLYING THE PROVIDED FUNCTION TO THE DATA!");

	for(int i = 0; i < 10; i++)
	{
		data = (int *) get_and_delete_head_LL(index_LL);

		TEST_ASSERT_EQUAL_INT_MESSAGE(validation_dataset[i]*2,*data,"FAILURE: THERE IS A PROBLEM WHILE APPLYING THE PROVIDED FUNCTION TO THE DATA. VALUE IS DIFFERENT FROM WHAT IS EXPECTED!");
	}

	index_LL = predef_data_set_integer();

	TEST_ASSERT_TRUE_MESSAGE(iterate_LL(index_LL, multiply_by_two, 3, 4, true, NULL, NULL),"FAILURE: THERE IS A PROBLEM WHILE APPLYING THE PROVIDED FUNCTION TO THE DATA!");

	for(int i = 0; i < 10; i++)
	{
		data = (int *) get_and_delete_head_LL(index_LL);

		expected_value = -1;

		if(i == 3 || i == 4)
			expected_value = validation_dataset[i]*2;
		else
			expected_value = validation_dataset[i];

		TEST_ASSERT_EQUAL_INT_MESSAGE(expected_value,*data,"FAILURE: THERE IS A PROBLEM WHILE APPLYING THE PROVIDED FUNCTION TO THE DATA. VALUE IS DIFFERENT FROM WHAT IS EXPECTED!");
	}

	index_LL = predef_data_set_integer();

	TEST_ASSERT_TRUE_MESSAGE(iterate_LL(index_LL, sum_of_elements, 0, get_size_LL(index_LL)-1, true, NULL, &sum),"FAILURE: THERE IS A PROBLEM WHILE APPLYING THE PROVIDED FUNCTION TO THE DATA!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(45,sum,"Failure: THE NUMBER OBTAINED BY THIS SUMMATION IS NOT WHAT WAS EXPECTED!");

	TEST_ASSERT_FALSE_MESSAGE(iterate_LL(index_LL, sum_of_elements, -1, 3, true, NULL, &sum),"FAILURE: THE ITERATE_LL() FUNCTION MUST RETURN FALSE WITH NEGATIVE STARTING POSITIONS!");

	TEST_ASSERT_FALSE_MESSAGE(iterate_LL(index_LL, sum_of_elements, 0, -5, true, NULL, &sum),"FAILURE: THE ITERATE_LL() FUNCTION MUST RETURN FALSE WITH NEGATIVE ENDING POSITIONS!");

	TEST_ASSERT_FALSE_MESSAGE(iterate_LL(index_LL, sum_of_elements, 0, get_size_LL(index_LL), true, NULL, &sum),"FAILURE: THE ITERATE_LL() FUNCTION MUST RETURN FALSE WITH ENDING POSITIONS GREATER THAN OR EQUAL TO THE SIZE OF THE LINKED LIST!");

	TEST_ASSERT_FALSE_MESSAGE(iterate_LL(index_LL, sum_of_elements, 5, 2, true, NULL, &sum),"FAILURE: THE ITERATE_LL() FUNCTION MUST RETURN FALSE WITH STARTING POSITIONS GREATER THAN ENDING POSITIONS!");

	sum = 0;

	TEST_ASSERT_TRUE_MESSAGE(iterate_LL(index_LL, sum_of_elements, 2, 2, true, NULL, &sum),"FAILURE: THE ITERATE_LL() FUNCTION MUST EXECUTE AND RETURN TRUE WITH STARTING POSITIONS EQUAL TO ENDING POSITIONS!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(2,sum,"Failure: THE NUMBER OBTAINED BY THIS SUMMATION IS NOT WHAT WAS EXPECTED!");

	bool is_counting_elements = false;

	TEST_ASSERT_TRUE_MESSAGE(iterate_LL(index_LL, count_elements, 0, 9, true, &is_counting_elements, &counter),"FAILURE: THERE IS A PROBLEM WHILE APPLYING THE PROVIDED FUNCTION TO THE DATA!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(10,counter,"FAILURE: THE VALUE OF COUNTER IS NOT WHAT WAS EXPECTED!");

	bool is_present = false;

	TEST_ASSERT_TRUE_MESSAGE(iterate_LL(index_LL, custom_sequential_search, 0, 9, true, &data_to_be_found, &is_present),"FAILURE: THERE IS A PROBLEM WHILE APPLYING THE PROVIDED FUNCTION TO THE DATA!");

	TEST_ASSERT_TRUE_MESSAGE(is_present,"FAILURE: TRUE VALUE WAS EXPECTED!");
}

void test_reset_LL(void)
{
	int16_t index_LL = -1;

	index_LL = predef_data_set_integer();

	TEST_ASSERT_TRUE_MESSAGE(reset_LL(index_LL), "FAILURE: THE CREATED LINKED LIST WAS NOT RESETED PROPERLY!");

	TEST_ASSERT_EQUAL_INT_MESSAGE(0, get_size_LL(index_LL), "FAILURE: THE SIZE OF THE LINKED LIST WAS NOT RESETED PROPERLY!");

	TEST_ASSERT_NULL_MESSAGE(get_head_LL(index_LL), "FAILURE: THE HEAD OF THE LINKED LIST WAS NOT RESETED PROPERLY!");

	TEST_ASSERT_NULL_MESSAGE(get_tail_LL(index_LL), "FAILURE: THE TAIL OF THE LINKED LIST WAS NOT RESETED PROPERLY!");
}

void test_benchmark_LL(void)
{
	int *data = NULL;

	clock_t end_time = -1L;

	int16_t index_LL = -1;

	int number_of_elements = 10000000;

	double operation_throughput = -1.0;

	clock_t start_time = -1L;

	clock_t total_number_of_clock_ticks = -1L;

	double total_number_of_seconds = -1.0;

	index_LL = create_LL(sizeof(int), integer_comparator, NULL, "s",number_of_elements);

	//INSERT FIRST
	start_time = clock();

	for(int i = number_of_elements; i > 0; --i)
		insert_element_LL(&i, INS_FIRST, index_LL);

	end_time = clock();

	total_number_of_clock_ticks = (end_time-start_time)+1;

	total_number_of_seconds = total_number_of_clock_ticks/(double)CLOCKS_PER_SEC;

	operation_throughput = 1/(total_number_of_seconds/number_of_elements);

	print_test_benchmark("Insertions",
						 "32-bit Integer",
						 "FIRST POSITION",
						 number_of_elements,
						 total_number_of_clock_ticks,
						 total_number_of_seconds,
						 operation_throughput);

	TEST_ASSERT_EQUAL_INT32_MESSAGE(number_of_elements,get_size_LL(index_LL),"FAILURE: THE SIZE OF THE LINKED LIST IS DIFFERENT FROM WHAT HAS BEEN EXPECTED!");

	//GET HEAD
	start_time = clock();

	for(int i = number_of_elements; i > 0; --i)
		get_head_LL(index_LL);

	end_time = clock();

	total_number_of_clock_ticks = (end_time-start_time)+1;

	total_number_of_seconds = total_number_of_clock_ticks/(double)CLOCKS_PER_SEC;

	operation_throughput = 1/(total_number_of_seconds/number_of_elements);

	print_test_benchmark("Gets",
						 "32-bit Integer",
						 "HEAD",
						 number_of_elements,
						 total_number_of_clock_ticks,
						 total_number_of_seconds,
						 operation_throughput);

	data = (int*)get_and_delete_head_LL(index_LL);

	 TEST_ASSERT_NOT_NULL_MESSAGE(data,"FAILURE: THE REFERENCE TO THE ELEMENT RETRIVED FROM THE LIMKED LIST IS NULL!");

	//DELETE AT HEAD
	start_time = clock();

	for(int i = number_of_elements-1; i > 0; --i)
	{
		get_and_delete_head_LL(index_LL);

		if(i % 500 == 0)
			gccl_bin_off_indication();
	}

	end_time = clock();

	total_number_of_clock_ticks = (end_time-start_time)+1;

	total_number_of_seconds = total_number_of_clock_ticks/(double)CLOCKS_PER_SEC;

	operation_throughput = 1/(total_number_of_seconds/number_of_elements);

	print_test_benchmark("Deletes",
						 "32-bit Integer",
						 "FIRST POSITION",
						 number_of_elements-1,
						 total_number_of_clock_ticks,
						 total_number_of_seconds,
						 operation_throughput);

	TEST_ASSERT_EQUAL_INT32_MESSAGE(0,get_size_LL(index_LL),"FAILURE: THE SIZE OF THE LINKED LIST IS DIFFERENT FROM WHAT HAS BEEN EXPECTED!");
}

int main(void)
{
	int nr_test_functions = 11;

	void* functionsPointer[] = {test_create_LL,\
								test_destroy_LL,\
								test_exists_element_LL,\
								test_get_and_delete_element_LL,\
								test_get_and_delete_head_LL,\
								test_insert_element_LL,\
								test_isEmpty_LL,\
								test_isFull_LL,\
								test_iterate_LL,\
						  	    test_reset_LL,\
								test_benchmark_LL};

	char* functionsName[] = {"test_create_LL()",\
		                     "test_destroy_LL()",\
							 "test_exists_element_LL()",\
							 "test_get_and_delete_element_LL()",\
							 "test_get_and_delete_head_LL()",\
							 "test_insert_element_LL()",\
							 "test_isEmpty_LL()",\
							 "test_isFull_LL()",\
							 "test_iterate_LL()",\
							 "test_reset_LL()",\
							 "test_benchmark_LL()"};

	for(int i = 0; i < nr_test_functions; ++i)
		UnityDefaultTestRun(functionsPointer[i],functionsName[i],i);

	return 0;
}
