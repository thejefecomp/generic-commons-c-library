# Generic Commons C Library (GCCL) #

The **Generic Commons C Library (GCCL)** is a project designed and developed by **Jeferson Souza (thejefecomp)** that aims to offer distinguished types of data structures to be used in the development of applications using the C programming language, **with a unique characteristic of keeping data privacy at its highest level**. Tasks related to any manipulation of the data kept in memory are left in the hand of the developer itself, which is the user of the library. It means that the **GCCL** does not need any knowledge about the data. 

Internally, the **GCCL** performs two types of actions: (a) management actions, which among other responsibilities support either allocating or freeing memory required to store a given type of data structure; and (b) data actions to enable the user (i.e., the developer) to perform create, retrieve, update, and delete (CRUD) operations on the data. 

The **GCCL** works as an executor-only when talking about operations performed on data, which implies that data actions such as comparisons of stored values are executed by invoking a function defined by the user that follows a given signature specified according to GCCL rules. As a consequence, the user is the only one that knows exactly which data type has been stored in a given data structure, and how that data type can be manipulated. In that sense, data is stored using a generic pointer (i.e., a void pointer), requiring the user to perform the suitable type casting to manipulate the data.

The library is intended to be offered in two distinguished flavours: static-linked; and dynamic-shared. The static-linked flavour is included (at compile-time) directly in the binary of the application under development, while the dynamic-shared flavour enables applications to keep the GCCL functions in a different binary, which is available to be used at run-time.

## Compiling the source code ##

Before thinking about compiling the source code, we need to clone the **GCCL** git repository, available on **Bitbucket** ([https://bitbucket.org](https://bitbucket.org)), using the following command:

    git clone https://bitbucket.org/thejefecomp/generic-commons-c-library.git

A new **"generic-commons-c-library"** directory will be created if the clone operation has been performed successfully. The **"generic-commons-c-library"** is the root directory of the project.

The **GCCL** uses **Clang** ([https://clang.llvm.org](https://clang.llvm.org)) as the default compiler of the project, which is part of the **LLVM Compiler Infrastructure** ([ https://llvm.org]( https://llvm.org)). If the "/usr/bin/cc" executable is neither a copy of or point to clang binary, please install **Clang** on your operating system to experience a smooth compilation task.


**Compiling the static-linked version of the GCCL**

The easy way to compile the static version of the **GCCL** is using the "compile" target defined in the Makefile, which is present in the root directory of the project. Let us just go to the **"generic-commons-c-library"** directory to execute the following command:


    make compile
    

**Compiling the dynamic-shared version of the GCCL**

Compiling the **GCCL** shared library is as ease as doing the static compilation. We just need to set the variable **"LIB_MODE=shared"** when executing the "compile" target:

    make compile LIB_MODE=shared


**Executing the unit tests against the static-linked version of the GCCL**

There is a "test" target in the Makefile to help executing the unit tests. Executing the unit tests against the static-linked version of the **GCCL** can be done as follows:

    make test
    

**Executing the unit tests against the dynamic-shared version of the GCCL**

In a similar way if compared to the task of compiling the source code, we just need to set the variable **"LIB_MODE=shared"** when executing the "test" target:

    make test LIB_MODE=shared

    
**Adding target CPU to both compilation and testing stages of the GCCL**

We can also add the specific Central Processor Unit (CPU) as the target CPU to be used in both compilation and testing stages of the **GCCL**. For doing that, we just need to set the variable **"CPU=target_cpu"** with the CPU type of our choice, which must be supported by both our current hardware configuration and clang itself. The following examples present the use of **M1 processor manufactured by "Apple, Inc."** as a target CPU.

*1*. Compiling and testing (Static version)

    make compile CPU=apple-m1
    
    make test CPU=apple-m1

*2*. Compiling and testing (Shared version)

    make compile CPU=apple-m1 LIB_MODE=shared
    
    make test CPU=apple-m1 LIB_MODE=shared

If we have no idea about what target CPUs are supported by our current hardware configuration, **Clang** can help us. We just need to run the following command:
    
    clang --print-supported-cpus
    
and **Clang** provides a list of the supported CPUs for our current hardware configuration.